USE Test;
select a.AGENT_NAME, a.WORKING_AREA,o.ORD_AMOUNT
from agents a
INNER JOIN orders o ON o.AGENT_CODE = a.AGENT_CODE
WHERE (a.WORKING_AREA, o.ORD_AMOUNT) = ANY (select WORKING_AREA,MAX(ORD_AMOUNT) 
from orders group by a.WORKING_AREA);


SELECT a.AGENT_NAME, a.PHONE_NO, dat.max_outstanding, cust.WORKING_AREA
FROM agents a 
inner JOIN (SELECT AGENT_CODE, WORKING_AREA from customer) cust
 ON a.AGENT_CODE = cust.AGENT_CODE
join (select AGENT_CODE, MAX(outstanding_amt) as max_outstanding from customer  GROUP BY AGENT_CODE)
dat on a.AGENT_CODE = dat.AGENT_CODE

where (
GROUP BY cust.WORKING_AREA
ORDER BY 4 ;


SELECT cust_name, c.working_area, outstanding_amt
FROM customer c
INNER JOIN (SELECT working_area, max(outstanding_amt) max_outstanding 
FROM customer GROUP BY working_area ) outstanding_data 
ON outstanding_data.max_outstanding = c.outstanding_amt 
AND outstanding_data.working_area = c.working_area
order by 1;