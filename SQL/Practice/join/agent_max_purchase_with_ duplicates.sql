USE Test;
SELECT   a.AGENT_NAME, om.order_max, ome.ORD_NUM, ome.ORD_DATE
from agents a 
LEFT JOIN (
SELECT  AGENT_CODE, MAX(ORD_AMOUNT) as order_max
from orders
group by AGENT_CODE  

 ) om
 ON a.AGENT_CODE = om.AGENT_CODE
 
 
 LEFT JOIN (
 SELECT AGENT_CODE, ORD_NUM, ORD_DATE, CUST_CODE, ORD_AMOUNT
 from orders) ome
 ON om.AGENT_CODE = ome.AGENT_CODE 
 AND ome.ORD_AMOUNT = om.order_max 
 
 #where   ome.ORD_AMOUNT = om.order_max 


 ORDER BY a.AGENT_NAME

 
;