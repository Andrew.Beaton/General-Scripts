USE Test;
select   a.AGENT_NAME, MIN(o.ORD_AMOUNT) 
from orders o

INNER JOIN agents  a ON o.AGENT_CODE = a.AGENT_CODE 

WHERE o.ORD_AMOUNT  IN ( SELECT  MIN(ORD_AMOUNT)
 from orders group by AGENT_CODE)
group by a.AGENT_CODE
ORDER BY AGENT_NAME;


