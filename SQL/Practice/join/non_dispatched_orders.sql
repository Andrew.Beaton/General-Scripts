USE Test ;
SELECT c.CUST_NAME, c.CUST_CITY, ord.ORD_AMOUNT,
ord.ORD_DATE, dso.ORD_DATE as despatch_date , a.PHONE_NO

from customer c
LEFT JOIN orders ord ON c.CUST_CODE = ord.CUST_CODE
LEFT JOIN daysorder dso ON c.CUST_CODE = ord.CUST_CODE 
AND ord.ORD_DATE  = dso.ORD_DATE
LEFT JOIN agents a ON c.AGENT_CODE = a.AGENT_CODE
WHERE dso.ORD_DATE is NULL 