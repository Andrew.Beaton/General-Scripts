#!/bin/bash
#SBATCH --nodes=60
#SBATCH --ntasks-per-node=24
#SBATCH --time=0:30:00
#SBATCH -J NC_02mj
#SBATCH --qos=debug
 
BIN=/global/project/projectdirs/txsupp/vp9users/edison/internal-gcc6.3/vorpal-r31842/bin
RUNDIRNAME=20181230_E210RB_300WL_increasedRes_NChannel02mj

srun ${BIN}/txpp.py ${RUNDIRNAME}.pre
srun ${BIN}/vorpal -i ${RUNDIRNAME}.in -r 71