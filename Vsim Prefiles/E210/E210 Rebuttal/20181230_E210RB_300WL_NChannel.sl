#!/bin/bash
#SBATCH --nodes=20
#SBATCH --ntasks-per-node=24
#SBATCH --time=00:30:00
#SBATCH -J V2_Test

#SBATCH --qos=debug 
 
BIN=/global/project/projectdirs/txsupp/vp9users/edison/internal-gcc6.3/vorpal-r31842/bin
RUNDIRNAME=20181230_E210RB_300WL_increasedRes_NChannel

srun ${BIN}/txpp.py ${RUNDIRNAME}.pre
srun ${BIN}/vorpal -i ${RUNDIRNAME}.in -r 45 