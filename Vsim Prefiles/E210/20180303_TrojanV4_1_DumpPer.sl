#!/bin/bash
#SBATCH --nodes=40
#SBATCH --ntasks-per-node=24
#SBATCH --time=03:00:00
#SBATCH -J TH05mJ02ps
#SBATCH --partition=regular
#SBATCH --qos=premium

BIN=/global/project/projectdirs/txsupp/vp9users/edison/internal-gcc-4.9/vorpal-r30961/bin
RUNDIRNAME=20180303_TrojanV4_1_DumpPer_05mJ_02ps

srun ${BIN}/txpp.py ${RUNDIRNAME}.pre
srun ${BIN}/vorpal -i ${RUNDIRNAME}.in -r 199
