# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 20:55:26 2018

@author: Andrew
"""
import h5py
import numpy as np 
import matplotlib.pyplot as plt 

RunName="TorchV8_5_0_8ps1mj"
SpeciesName="HeElectrons"
DumpN=19
def loadh5 (MainFilename,Species,DN):
	
    FileN = '%s_%s_%i.h5' %(RunName,Species,DN)
    print "Start loading %s" %(FileN)
    f = h5py.File(FileN)
    ParticleData = np.array(f[Species], dtype=np.float32)
    numPtclsInMacro= int(round(f[Species].attrs["numPtclsInMacro"]))
    #numPtclsInMacro = f[Species].attrs["numPtclsInMacro"]
    #vsLowerBounds   = f[Species][1].attrs["vsLowerBounds"]
    f.close()

    return ParticleData 

ParticleData=loadh5(RunName,SpeciesName,DumpN)


def TotalChargeCal(OGData,RunName,Species,DN):
    FileN = '%s_%s_%i.h5' %(RunName,Species,DN)
    f = h5py.File(FileN)
    numPtclsInMacro= int(round(f[Species].attrs["numPtclsInMacro"]))
    TotalCharge=0
    for b in range (0,OGData.shape[0]):
        TotalCharge = TotalCharge + (OGData[b,7] * numPtclsInMacro *1.60217662e-19)
    return TotalCharge

#TotalCharge=TotalChargeCal(ParticleData,RunName,SpeciesName,DumpN)

#print "Total Charge ="
#print TotalCharge

#ParticleData=np.transpose(ParticleData)
print ParticleData.shape
#print ParticleData[0,1]


def Slice (OGData,CutVL,CutVU,Dim):
    SliceD=OGData
    outside=[]
    for x in range (0,OGData.shape[0]):
        if not CutVL <= OGData[x,Dim] <= CutVU :
            outside.append(x)
    SliceD=np.delete(SliceD,outside,axis=0)
    return SliceD

#SlicedData=Slice(ParticleData,2e-6)
#print SlicedData.shape
def RemoveWZeros(OGData):
    SliceD=OGData
    Zeros=[]
    for x in range (0,OGData.shape[0]):
        #print OGData[x,7]
        if OGData[x,7] == 0:
            Zeros.append(x)
    #print Zeros
    SliceD=np.delete(SliceD,Zeros,axis=0)
    return SliceD


def AverageDens(OGData,RunName,SpeciesName,DumpN):
    SliceD=Slice(OGData,-50e-6,50e-6,2)
    #print np.average(OGData[:,0], axis=0)+35e-6
    SliceD=Slice(SliceD,np.average(OGData[:,0], axis=0)-35e-6,np.average(OGData[:,0], axis=0)+35e-6,0)
    SliceD=Slice(SliceD,np.average(OGData[:,1], axis=0)-35e-6,np.average(OGData[:,1], axis=0)+35e-6,1)
    print SliceD.shape
    NonZero=RemoveWZeros(SliceD)
    print "removed zero weight macros "
    print NonZero.shape
    TotalCharge=TotalChargeCal(NonZero,RunName,SpeciesName,DumpN)
    print "Total charge remaining =" 
    print TotalCharge
    xmax = max(NonZero[:,0])
    ymax = max(NonZero[:,1])
    zmax = max(NonZero[:,2])
    
    xmin = min(NonZero[:,0])
    ymin = min(NonZero[:,1])
    zmin = min(NonZero[:,2])
    print zmin,zmax
    
    XSize= xmax-xmin
    YSize= ymax-ymin
    ZSize= zmax-zmin
    print XSize,YSize,ZSize
    
    AverageDens=TotalCharge/(XSize*YSize*ZSize)
    print AverageDens
    return NonZero


DatatoPlot=AverageDens(ParticleData,RunName,SpeciesName,DumpN)

print DatatoPlot.shape

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(DatatoPlot[:,0], DatatoPlot[:,1], DatatoPlot[:,2])
plt.show