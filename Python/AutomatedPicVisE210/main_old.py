# -*- coding: utf-8 -*-
"""
Created on Wed Feb 03 18:23:10 2016

@author: P. Scherkl, O. Karger
"""

#####################
# Uncomment for cluster use and adapt path
import imp, sys    
#sys.path.append('//global//cscratch1//sd//hidding//AndrewB//Python//PIcvisBase//')

from dataManager import *
from dumpSummarizer import *
from Coordinator import *



from dump import *
from elecDump import *
from fieldDump import *


"""
#--------------------------------------------------------
#       Define .h5 files to be post-processed: specify path, prefix, dumpNumbers, and witness species
#--------------------------------------------------------
"""
#pathToData     ="/scratch2/scratchdirs/hidding/e210/V2-He++/Trojan/TrojanV2_40umD20um5mj/TrojanV3_40umD20um5mj1ps/"
pathToData     = "/scratch2/scratchdirs/hidding/e210/V2-He++/Torch/TestingAixlensProfile/TorchV7_1He++_1ps/"
prefix         = "TorchV7_1He++_-1psTagging5mj"
#prefix         = "TrojanV3_40umD20um5mj0_0ps"

#pathToData     ="/scratch2/scratchdirs/hidding/e210/V2-He++/TestV6-ReducedSpecies38umWaist/"
#prefix ="TorchV6He++_-1psTagging5mj38um"

#particleDumpList = []
particleDumpList = ["HeElectrons","ChannelElectrons","HePlusElec"]#,"HeWakeElectrons"]
#particleDumpList = ["HeElectrons", "HElectrons","HePlusElec"]

#fieldDumpList     = ["ElecMultiField"]
fieldDumpList    = ["lasersPlusPlasma"]#"lasers"
#fieldDumpList    = ["ElecMultiField", "lasersPlusPlasma", "SumRhoJ", "smRhoJ", "MagMultiField","auxRhoJ"]

#load and plot following dump numbers:
#dumpNumbers    = np.arange(1, 50 + 1)
#dumpNumbers    = np.arange(430, 440 + 1, 10) # NOTE: if you only want every n-th dump loaded use this line
#dumpNumbers  = [240]#[sys.argv[1]] # use this for single dumps
#
dumpNumbers  = [20]
Comp2D       = 2 #1:xy, 2:xz 

coordinator = Coordinator(pathToData, dumpNumbers, prefix, particleDumpList, fieldDumpList)
#coordinator._Manager.setOffsetX(-120.38200692) # moves xAxis in all plots by given number [um]


coordinator.plotSummarizedDumps = 0 





"""
#--------------------------------------------------------
#       Plot switches for driver electron beams
#--------------------------------------------------------
"""
coordinator.getParticleDump("driver").plot2D    = 1
coordinator.getParticleDump("driver").plotHist  = 0 #"1" current, "2" line charge density
coordinator.getParticleDump("driver").comp2D    = Comp2D #1 == y, 2 == z component

coordinator.getParticleDump("driver").setPlotParticleRatio(0.5)
coordinator.getParticleDump("driver").setTransparency(0.1)
coordinator.getParticleDump("driver").setPlotMarkerSize(0.2)
coordinator.getParticleDump("driver").setPlotMarker("k.")


#coordinator.getParticleDump("driver").setHistBinSize(0.01)
#coordinator.getParticleDump("driver").setHistSmooth(1)
#coordinator.cutPhaseSpace("driver","E",0,1000)
#coordinator.getParticleDump("driver").setColorCodeComp("E")
#coordinator.getParticleDump("driver").setColorMap("gray")
#coordinator.getParticleDump("driver").setTransparencyVec("x")
#coordinator.plotPhaseSpaces("driver","x")
"""
#--------------------------------------------------------
#       Plot switches for witness electron beams
#--------------------------------------------------------
"""

cmap                = "inferno"
ratio               = 1.0
transparency        =0.1
#transparency        = 0.1
#msize               = 0.5
msize               = 0.5


coordinator.getParticleDump(0).plot2D    = 1


coordinator.getParticleDump(0).setPlotMarkerSize(msize)
coordinator.getParticleDump(0).setColorMap(cmap)
coordinator.getParticleDump(0).setPlotParticleRatio(ratio)
coordinator.getParticleDump(0).comp2D    = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(0).setColorCodeComp("E")
#coordinator.getParticleDump(0).setPlotMarker("blue.")

#coordinator.getParticleDump(0).plotHist  = 0  #"1" current, "2" line charge density
#coordinator.getParticleDump(0).setTransparency(transparency)
#coordinator.getParticleDump(0).setHistBinSize(0.01)
#coordinator.getParticleDump(0).setHistSmooth(1)
#coordinator.getParticleDump(0).setTransparencyVec("E")
#coordinator.getParticleDump(0).setTransparencyVec("E")

##

#coordinator.cutPhaseSpace(0,"E", 45,2e4)
#coordinator.cutPhaseSpace(0,"x", 160 , 300)
#coordinator.cutPhaseSpace(0,"y", -15, 15)
#coordinator.cutPhaseSpace(0,"z", -15 ,15)


#coordinator.plotPhaseSpaces(0,"x")
#coordinator.plotPhaseSpaces(0,"y")
#coordinator.plotPhaseSpaces(0,"z")
#coordinator.plotPhaseSpaces(0,"t")
#

coordinator.getParticleDump(1).plot2D    = 1
coordinator.getParticleDump(1).plotHist  = 0  #"1" current, "2" line charge density


coordinator.getParticleDump(1).setPlotMarkerSize(msize)
#coordinator.getParticleDump(1).setColorMap(cmap)
coordinator.getParticleDump(1).setPlotParticleRatio(ratio)
coordinator.getParticleDump(1).comp2D     = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(1).setColorCodeComp("E")
coordinator.getParticleDump(1).setPlotMarker("m.")
#coordinator.getParticleDump(0).setPlotMarker("blue.")
coordinator.getParticleDump(1).setTransparency(0.5)
#coordinator.getParticleDump(1).setHistBinSize(0.001)
#coordinator.getParticleDump(1).setHistSmooth(1)
#coordinator.getParticleDump(1).setTransparencyVec("e",  norm = (580,600), reverse = False)

##

#coordinator.cutPhaseSpace(1,"E",5,2e10)
#coordinator.cutPhaseSpace(1,"x",-60, 0)
#coordinator.cutPhaseSpace(1,"y", -15 , 15)
#coordinator.cutPhaseSpace(1,"z", -15 , 15)
#####coordinator.cutPhaseSpace(1,"yp",-100, 100)
###
#coordinator.plotPhaseSpaces(1,"x")
#coordinator.plotPhaseSpaces(1,"y")
#coordinator.plotPhaseSpaces(1,"z")
##coordinator.plotPhaseSpaces(1,"y")
##coordinator.plotPhaseSpaces(1,"t")
#coordinator.plotPhaseSpaces((0,1),"x")

#
coordinator.getParticleDump(2).plot2D    =1
coordinator.getParticleDump(2).plotHist  = 0  #"1" current, "2" line charge density


coordinator.getParticleDump(2).setPlotMarkerSize(msize)
#coordinator.getParticleDump(2).setColorMap(cmap)
coordinator.getParticleDump(2).setPlotParticleRatio(ratio)
coordinator.getParticleDump(2).comp2D     = Comp2D #1 == y, 2 == z component
#coordinator.getParticleDump(2).setColorCodeComp("E")
#coordinator.getParticleDump(1).setPlotMarker("m.")
coordinator.getParticleDump(0).setPlotMarker("orange.")

#coordinator.getParticleDump(1).setTransparency(transparency)
#coordinator.getParticleDump(1).setHistBinSize(0.001)
#coordinator.getParticleDump(1).setHistSmooth(1)
#coordinator.getParticleDump(1).setTransparencyVec("e",  norm = (580,600), reverse = False)


##
#coordinator.cutPhaseSpace(2,"E",45,350)
#coordinator.cutPhaseSpace(2,"x", 160 , 300)
#
#coordinator.plotPhaseSpaces(2,"x")
#coordinator.plotPhaseSpaces((0,2),"x")
#coordinator.plotPhaseSpaces((0,1),"y")
#coordinator.plotPhaseSpaces((0,1),"z")


#coordinator.cutPhaseSpace(0,"y", -2.5 , 2.5)
coordinator.cutPhaseSpace(1,"y", -2.5 , 2.5)
#coordinator.cutPhaseSpace(2,"y", -2.5 , 2.5)

#coordinator.getParticleDump(0).plotParticleTrajectories([2454307,2068763,1770779,2230043,1944347,1824027,2320163,2184475,1945371,1314075,1826075,1639707,1580827,1951003,1634075,2209051])
#ve=np.arange(0,2500000)
#coordinator.getParticleDump(0).plotParticleTrajectories(ve) 


################
"""
coordinator.cutPhaseSpace(0,"E", 5,2e4)
coordinator.cutPhaseSpace(0,"x", 210 , 300)

coordinator.plotPhaseSpaces(0,"x")


coordinator.cutPhaseSpace(1,"x", 210 , 300)
coordinator.cutPhaseSpace(1,"E", 5,2e4)
coordinator.plotPhaseSpaces(1,"x")


coordinator.cutPhaseSpace(2,"E",5,350)
coordinator.cutPhaseSpace(2,"x", 210 , 300)
#
coordinator.plotPhaseSpaces(2,"x")
################
"""

##############

"""
#--------------------------------------------------------
#       Plot switches for field dumps
#--------------------------------------------------------
"""
#######  2D switches   ###############################





coordinator.getFieldDump(1).plot2D           = 0
coordinator.getFieldDump(0).comp2D           = -1 # 0,1,2 for x,yz and -1 for geometric sum
#coordinator.getFieldDump(0).setColorbar2D("RdBu_r")
coordinator.getFieldDump(0).setPot2DColorMap("RdBu_r")
#coordinator.getFieldDump(0).setTransparency(0.99)
#coordinator.getFieldDump(0).setTransparencyVec("e",  norm = (580,600), reverse = False)
#coordinator.getFieldDump(0).fieldLOComp      = 0 
#coordinator.getFieldDump(0).plotFieldLO      = 0
#coordinator.getFieldDump(0).plotPotLO        = 0 
#coordinator.getFieldDump(0).plotFieldGradLO  = 0 






coordinator.getElecMultiField().plot2D          = 0
coordinator.getElecMultiField().comp2D          = 0 # 0,1,2 for x,y, z and -1 for geometric sum
#coordinator.getElecMultiField().plane2D         = ["x", "y",0]
coordinator.getElecMultiField().setColorbar2D("RdBu_r")

if Comp2D == 1:
    coordinator.getElecMultiField().plane2D         = ["x", "y",0]
elif Comp2D == 2:
    coordinator.getElecMultiField().plane2D         = ["x", 0, "z"]

#coordinator.getElecMultiField().setColorbar2D("gray")
#coordinator.getElecMultiField().plotFieldLO     = 1
coordinator.getElecMultiField().fieldLOComp     = 0
#coordinator.getElecMultiField().fieldLORadius   = 2
coordinator.getElecMultiField().fieldLOShift    = [0, 0] #[um]
coordinator.getElecMultiField().plotFieldGradLO = 0
#coordinator.getElecMultiField().plotPotLO       = 1
coordinator.getElecMultiField().plotPot2D       = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getElecMultiField().setPot2DColorMap("coolwarm")
coordinator.getElecMultiField().setPotLOColor("r")



coordinator.getSumRhoJ().plot2D                 = 0
coordinator.getSumRhoJ().comp2D                 = 0 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getSumRhoJ().setColorbar2D("gray")
coordinator.getSumRhoJ().plotFieldLO            = 0
coordinator.getSumRhoJ().fieldLOComp            = 0
coordinator.getSumRhoJ().fieldLORadius          = 0
coordinator.getSumRhoJ().fieldLOShift           = [0, 0] #[um]
coordinator.getSumRhoJ().plotFieldGradLO        = 0
coordinator.getSumRhoJ().plotPotLO              = 0
coordinator.getSumRhoJ().plotPot2D              = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getSumRhoJ().setPot2DColorMap("gray")


coordinator.getLasersPlusPlasma().plot2D           = 1
coordinator.getLasersPlusPlasma().comp2D           = Comp2D # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getLasersPlusPlasma().setColorbar2D("RdBu_r")
#coordinator.getLasersPlusPlasma().plane2D         = ["z", 0, "y"]
#coordinator.getLasersPlusPlasma().plotFieldLO      = 1#1
coordinator.getLasersPlusPlasma().fieldLOComp      = 0
coordinator.getLasersPlusPlasma().fieldLORadius    = 2
coordinator.getLasersPlusPlasma().fieldLOShift     = [0, 0] #[um]
coordinator.getLasersPlusPlasma().plotFieldGradLO  = 0
#coordinator.getLasersPlusPlasma().plotPotLO        = 1#1
coordinator.getLasersPlusPlasma().plotPot2D        = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getLasersPlusPlasma().setPot2DColorMap("RdBu_r")
coordinator.getLasersPlusPlasma().setPotLOColor("fuchsia")
#coordinator.getLasersPlusPlasma().setTransparency(0.2)

"""
#--------------------------------------------------------
#       2D plotting options
#--------------------------------------------------------
"""

coordinator._Plotter2D.saveFigures            = 1
coordinator._Plotter2D.export                 = 0 
coordinator._Plotter2D.setDumpNote(0)

coordinator._Plotter2D.setPlotPropLen(1)
coordinator._Plotter2D.setPropLenPosX(0.66)
coordinator._Plotter2D.setPropLenPosY(0.9)
coordinator._Plotter2D.setPropLenColor("k")
coordinator._Plotter2D.setDPI(350)           

coordinator._Plotter2D.setPlotParticleColorbar(1)
coordinator._Plotter2D.setColbarLabelColor("k")
coordinator._Plotter2D.setColbarTickLeftColor("w")
coordinator._Plotter2D.setColbarTickRightColor("k")
coordinator._Plotter2D.setColbarZero(0)


coordinator._Plotter2D.setPlotParticleColorbar(1)


#######  Figure limits   ###############################
#coordinator._Plotter2D.setXLim(180-180,450-180)
coordinator._Plotter2D.setXLim(190,450)

coordinator._Plotter2D.setYLim(-65,65)
coordinator._Plotter2D.setYLimFieldLO(-70,60)
coordinator._Plotter2D.setYLimFieldGradLO(-20,20)
coordinator._Plotter2D.setYLimPotLO(-3.5,3)
coordinator._Plotter2D.setYLimParticleLO(0,20)
coordinator._Plotter2D.setFigSize(8*1.5,3.8*1.5)
#coordinator._Manager.setOffsetX(-180)									 

 
#######  2D field style options ########################
col2                = "c"
coordinator._Plotter2D.setFieldLOAxisColor(col2)
coordinator.getLasersPlusPlasma().setFieldLOColor(col2)	
coordinator.getLasersPlusPlasma().setFieldLOWidth(1)
											
coordinator._Plotter2D.setShowColBar(1)

#coordinator._Plotter2D.setPotLOAxisColor("Orange.")													
coordinator._Plotter2D.plotLotov        = 0
coordinator._Plotter2D.lotovRange       = [50, 175]
coordinator._Plotter2D.lotovDens        = 6.0e22
coordinator._Plotter2D.lotovSlopeFactor = 3.5 
coordinator._Plotter2D.plotTzoufras     = 0

#######  Potential style options #######################
col                 = "fuchsia"
coordinator._Plotter2D.setPotLOAxisColor(col)
coordinator._Plotter2D.plotPotLOTrappingArea(1)
coordinator._Plotter2D.setPotLOAxisColor(col)
coordinator.getElecMultiField().setPotLOColor(col)											 
coordinator._Plotter2D.setPotLOTrappingAreaColor(col)
coordinator._Plotter2D.setPot2DMax(-1)
coordinator.getElecMultiField().setPotLOWidth(1)




"""
#--------------------------------------------------------
#       Phase  space plotting style options
#--------------------------------------------------------
"""


coordinator._PlotterPhaseSpace.saveFigures = 0
coordinator._PlotterPhaseSpace.export      = 0  
coordinator._PlotterPhaseSpace.setDumpNote(1)
coordinator._PlotterPhaseSpace.setDPI(500)

coordinator._PlotterPhaseSpace.setFigSize(7, 6)
coordinator._PlotterPhaseSpace.setPlotPropLen(1)

#coordinator._PlotterPhaseSpace.setXLim(0,80)
#coordinator._PlotterPhaseSpace.setYLim(-15,15)
#coordinator._PlotterPhaseSpace.setZLim(-30,30)
#coordinator._PlotterPhaseSpace.setLabelX("adasd")

coordinator._PlotterPhaseSpace.setPlotParticleColorbar(1)






"""
#--------------------------------------------------------
#       define parameters that should be analyzed/calculated (print to terminal and saved in summarizer output files!)
#--------------------------------------------------------
"""


coordinator.getParticleDump(0).analyzeDumps(0)
coordinator.getParticleDump(0).saveCharge                  = 1
coordinator.getParticleDump(0).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(0).saveEnergyMean              = 1
coordinator.getParticleDump(0).saveEnergyMax               = 0
coordinator.getParticleDump(0).saveGammaMean               = 0
coordinator.getParticleDump(0).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(0).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(0).saveEnergyDevRMS            = 0
coordinator.getParticleDump(0).saveEnergySpreadRMS         = 0
coordinator.getParticleDump(0).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(0).savePositionMean            = 1
coordinator.getParticleDump(0).savePositionMeanWeighted    = 1
coordinator.getParticleDump(0).savePositionMeanLabframe    = 1
coordinator.getParticleDump(0).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(0).saveBunchLengthRMS          = 0
coordinator.getParticleDump(0).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(0).saveBunchLengthMax          = 0
coordinator.getParticleDump(0).saveWidthRMS                = 0
coordinator.getParticleDump(0).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(0).saveWidthMax                = 0
coordinator.getParticleDump(0).saveDivergenceRMS           = 1
coordinator.getParticleDump(0).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(0).saveEmittance               = 1
coordinator.getParticleDump(0).saveAccField                = 0
coordinator.getParticleDump(0).saveZeroCrossing            = 0
coordinator.getParticleDump(0).saveCurrentMax              = 0
coordinator.getParticleDump(0).saveBrightness5D            = 0
coordinator.getParticleDump(0).saveAccFieldGradient        = 0
                       
                       
coordinator.getParticleDump(1).analyzeDumps(0)
coordinator.getParticleDump(1).saveCharge                  = 1
coordinator.getParticleDump(1).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(1).saveEnergyMean              = 0
coordinator.getParticleDump(1).saveEnergyMax               = 0
coordinator.getParticleDump(1).saveGammaMean               = 0
coordinator.getParticleDump(1).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(1).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(1).saveEnergyDevRMS            = 0
coordinator.getParticleDump(1).saveEnergySpreadRMS         = 0
coordinator.getParticleDump(1).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(1).savePositionMean            = 0
coordinator.getParticleDump(1).savePositionMeanWeighted    = 0
coordinator.getParticleDump(1).savePositionMeanLabframe    = 0
coordinator.getParticleDump(1).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(1).saveBunchLengthRMS          = 0
coordinator.getParticleDump(1).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(1).saveBunchLengthMax          = 0
coordinator.getParticleDump(1).saveWidthRMS                = 0
coordinator.getParticleDump(1).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(1).saveWidthMax                = 0
coordinator.getParticleDump(1).saveDivergenceRMS           = 0
coordinator.getParticleDump(1).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(1).saveEmittance               = 1
coordinator.getParticleDump(1).saveEmittanceDispersion     = 1
coordinator.getParticleDump(1).saveAccField                = 0
coordinator.getParticleDump(1).saveZeroCrossing            = 0
coordinator.getParticleDump(1).saveCurrentMax              = 0
coordinator.getParticleDump(1).saveBrightness5D            = 0




coordinator.getParticleDump(2).analyzeDumps(0)
coordinator.getParticleDump(2).saveCharge                  = 1
coordinator.getParticleDump(2).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(2).saveEnergyMean              = 1
coordinator.getParticleDump(2).saveEnergyMax               = 1
coordinator.getParticleDump(2).saveGammaMean               = 0
coordinator.getParticleDump(2).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(2).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(2).saveEnergyDevRMS            = 0
coordinator.getParticleDump(2).saveEnergySpreadRMS         = 0
coordinator.getParticleDump(2).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(2).savePositionMean            = 1
coordinator.getParticleDump(2).savePositionMeanWeighted    = 0
coordinator.getParticleDump(2).savePositionMeanLabframe    = 1
coordinator.getParticleDump(2).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(2).saveBunchLengthRMS          = 0
coordinator.getParticleDump(2).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(2).saveBunchLengthMax          = 1
coordinator.getParticleDump(2).saveWidthRMS                = 1
coordinator.getParticleDump(2).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(2).saveWidthMax                = 0
coordinator.getParticleDump(2).saveDivergenceRMS           = 0
coordinator.getParticleDump(2).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(2).saveEmittance               = 1
coordinator.getParticleDump(2).saveAccField                = 0
coordinator.getParticleDump(2).saveZeroCrossing            = 0
coordinator.getParticleDump(2).saveCurrentMax              = 0
coordinator.getParticleDump(2).saveBrightness5D            = 0


coordinator.getParticleDump("driver").analyzeDumps(0) 
coordinator.getParticleDump("driver").saveCharge                  = 1
coordinator.getParticleDump("driver").saveEnergyMeanWeighted      = 0
coordinator.getParticleDump("driver").saveEnergyMean              = 0
coordinator.getParticleDump("driver").saveEnergyMax               = 0
coordinator.getParticleDump("driver").saveGammaMean               = 0
coordinator.getParticleDump("driver").saveGammaMeanWeighted       = 0
coordinator.getParticleDump("driver").saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump("driver").saveEnergyDevRMS            = 0
coordinator.getParticleDump("driver").saveEnergySpreadRMS         = 0
coordinator.getParticleDump("driver").saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump("driver").savePositionMean            = 1
coordinator.getParticleDump("driver").savePositionMeanWeighted    = 0
coordinator.getParticleDump("driver").savePositionMeanLabframe    = 1
coordinator.getParticleDump("driver").savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump("driver").saveBunchLengthRMS          = 0
coordinator.getParticleDump("driver").saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump("driver").saveBunchLengthMax          = 0
coordinator.getParticleDump("driver").saveWidthRMS                = 0
coordinator.getParticleDump("driver").saveWidthRMSWeighted        = 0
coordinator.getParticleDump("driver").saveWidthMax                = 0
coordinator.getParticleDump("driver").saveDivergenceRMS           = 0
coordinator.getParticleDump("driver").saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump("driver").saveEmittance               = 0
coordinator.getParticleDump("driver").saveAccField                = 0
coordinator.getParticleDump("driver").saveZeroCrossing            = 0
coordinator.getParticleDump("driver").saveCurrentMax              = 0
coordinator.getParticleDump("driver").saveBrightness5D            = 0
coordinator.getParticleDump("driver").saveAccFieldGradient        = 0
  
                       
coordinator.getElecMultiField().analyzeDumps(0)
coordinator.getElecMultiField().saveZeroCrossing            = 1
coordinator.getElecMultiField().saveMinField                = 1
coordinator.getElecMultiField().saveMaxField                = 1
coordinator.getElecMultiField().saveTransformerRatio        = 1

coordinator.getLasersPlusPlasma().analyzeDumps(0)
coordinator.getLasersPlusPlasma().saveZeroCrossing            = 1
coordinator.getLasersPlusPlasma().saveMinField                = 1
coordinator.getLasersPlusPlasma().saveMaxField                = 1
coordinator.getLasersPlusPlasma().saveTransformerRatio        = 1


coordinator.getFieldDump(1).analyzeDumps(0)
#coordinator.getFieldDump(2).saveZeroCrossing            = 1
#coordinator.getFieldDump(2).saveMinField                = 1
coordinator.getFieldDump(1).saveMaxField                = 1
#coordinator.getFieldDump(2).saveTransformerRatio        = 1
#

"""
#--------------------------------------------------------
#       execute code
#--------------------------------------------------------
"""

coordinator.dumpLoop()
     
"""   
#--------------------------------------------------------
#       plot summarized data
#--------------------------------------------------------
"""
#if coordinator.plotSummarizedDumps:

    
#    coordinator._Summarizer.plotData("x",("driverCharge","HeElectronscharge",))
#    coordinator._Summarizer.plotData("x",("getLasersPlusPlasma")) # note: putting a number into the string gives the corresponding witness beam (ofc, you can also use its name)
#    coordinator._Summarizer.plotData("x",("driverCharge", "EnergyMaxdriver"))
#    coordinator._Summarizer.exportDataToFile("getLasersPlusPlasma")

del coordinator


