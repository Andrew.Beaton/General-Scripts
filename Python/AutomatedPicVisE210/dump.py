# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 11:15:21 2016

@author: Paul Scherkl"""

from plotter import *
import math


class dump(object):
    """ generalized object that maintains dumps in general. can easily be extended by 
    variables or methods that are relevant for all types of dumps (e.g. elecDump, fieldDump, etc)"""
    
    
    def __init__(self, name, dumpType):
        self.name      = name
        self.type      = dumpType
        self.loaded       = 0
        self.windowStart  = 0
        self.windowEnd    = 0
        self.runTime      = 0
        
        self.analyze      = 0
        self.plot2D       = 0
        self.plot3D       = 0
        self.transparency = 1.0
        
        
        
        
    def analyzeDumps(self, switch):
        """ turns plotting to terminal and summarizing on/off"""
        self.analyze = switch 
        
    def getType(self):
        """ returns type of dump object"""
        return self.type
        
    def getName(self):
        """ returns type of dump object"""
        return self.name
    
    def getWindowStart(self):
        """ returns back position of moving window in lab coordinates"""
        return self.windowStart

    def getWindowEnd(self):
        """ returns front position of moving window in lab coordinates"""
        return self.windowEnd

    def setTransparency(self, alpha):
        """ sets transparency of dump object"""
        self.transparency = alpha
        
        
    def getTransparency(self, alpha):
        """ returns transparency of dump object"""
        return self.transparency         
        
        
    def smooth(self, y, box_pts):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth
        

        