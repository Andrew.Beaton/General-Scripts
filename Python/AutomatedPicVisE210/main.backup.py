# -*- coding: utf-8 -*-
"""
Created on Wed Feb 03 18:23:10 2016

@author: P. Scherkl, O. Karger
"""

#####################
# Uncomment for cluster use and adapt path
#import imp, sys    
#sys.path.append('//lustre//project//k1191//picViz//')  # uncomment for SHAHEEN
#sys.path.append('//scratch2//scratchdirs//hidding//picViz//') # uncomment for NERSC
#sys.path.append('//work//hhh36//hhh362//picViz//') # uncomment for JURECA




from Coordinator import Coordinator
import numpy as np




"""
#--------------------------------------------------------
#       Define .h5 files to be post-processed: specify path, prefix, dumpNumbers, and witness species
#--------------------------------------------------------
"""
#pathToData     = "//global//cscratch1//sd//hidding//AndrewB//run2_SlowmoOnlywitness//"
pathToData     = "/scratch2/scratchdirs/hidding/e210/AxilensReallaser/20um05mj40umD/0.375ps/"
#pathToData     = "E:\\Paul\\My Files & Folders\\Science\Data\\\\170405_impactionbinary_less\\"
#pathToData     = "E:\\Paul\\My Files & Folders\\Science\Data\\panos\\"
#pathToData     = "E:\\Paul\\My Files & Folders\\Science\Data\\ICSPaper\\witness4Escort12\\"
#pathToData     = "I:\\MultiColorICSData\\multiColor01\\"
#pathToData     = "I://Science//Physics-Trojans//Group//MultiColorICSData\\multiColor01\\"
#pathToData     = u"D:\\Strathcloud\\Persönliche Ordner\\Science\\Data\\ICSPaper\\res42Li05\\"
#pathToData     = u"D:\\Strathcloud\\Persönliche Ordner\\Science\\Data\\plasmaGlowPaper\\plasmaGlowPT01\\"
prefix         = "AxilensRealLaser40umD20um05mj0_375ps"

#particleDumpList = ["BeamElectrons", "LiPlusElec"]
#particleDumpList = ["ChannelElectrons", "TORCHElectrons"]
particleDumpList = ["ChannelElectrons","HeElectrons"]
#particleDumpList = ["NePlusElec", "H", "HeElectrons", "TORCHElectrons","TORCHIons"]

#particleDumpList    = ["TORCHElectrons", "TORCHIons"]
fieldDumpList    = ["lasersPlusPlasma"]
#fieldDumpList    = ["ElecMultiField", "lasersPlusPlasma", "SumRhoJ", "smRhoJ", "MagMultiField","auxRhoJ"]

#load and plot following dump numbers:
#dumpNumbers    = np.arange(20, 30 + 1)
#dumpNumbers    = np.arange(430, 440 + 1, 10) # NOTE: if you only want every n-th dump loaded use this line
dumpNumbers  = [212] # use this for single dumps
#


coordinator = Coordinator(pathToData, dumpNumbers, prefix, particleDumpList, fieldDumpList)
#coordinator._Manager.setOffsetX(-120.38200692) # moves xAxis in all plots by given number [um]


coordinator.plotSummarizedDumps = 0





"""
#--------------------------------------------------------
#       Plot switches for driver electron beams
0#--------------------------------------------------------
"""
coordinator.getParticleDump("driver").plot2D    = 1
coordinator.getParticleDump("driver").plotHist  = 0 #"1" current, "2" line charge density
coordinator.getParticleDump("driver").plot3D    = 0
coordinator.getParticleDump("driver").comp2D    = 2 #1 == y, 2 == z component

#coordinator.getParticleDump("driver").setPlotParticleRatio(0.05)
coordinator.getParticleDump("driver").setPlotParticleRatio(1)
#coordinator.getParticleDump("driver").setColorCodeComp("pz")
#coordinator.getParticleDump("driver").setColorMap("gray")
coordinator.getParticleDump("driver").setTransparency(0.1)
coordinator.getParticleDump("driver").setHistBinSize(0.01)
coordinator.getParticleDump("driver").setHistSmooth(1)
coordinator.getParticleDump("driver").setPlotMarker("k.")
#coordinator.cutPhaseSpace("driver","E",0,1000)
coordinator.getParticleDump("driver").setPlotMarkerSize(0.2)

#coordinator.plotPhaseSpaces("driver","x")
"""
#--------------------------------------------------------
#       Plot switches for witness electron beams
#--------------------------------------------------------
"""
cmap                = "inferno"
ratio               = 1.0
transparency        =0.1
#transparency        = 0.1
#msize               = 0.5
msize               = 0.5

coordinator.getParticleDump(0).plot2D    = 1
coordinator.getParticleDump(0).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(0).plot3D    = 0

coordinator.getParticleDump(0).setPlotMarkerSize(msize)

coordinator.getParticleDump(0).setColorCodeComp("E")
coordinator.getParticleDump(0).setColorMap(cmap)
#coordinator.getParticleDump(0).setHistBinSize(0.01)
coordinator.getParticleDump(0).setPlotParticleRatio(ratio)
#coordinator.getParticleDump(0).setHistSmooth(1)
coordinator.getParticleDump(0).comp2D    = 2 #1 == y, 2 == z component
#coordinator.getParticleDump(0).setTransparencyVec("E")
#coordinator.getParticleDump(0).setTransparencyVec("e")
##
#coordinator.cutPhaseSpace(0,"x", 79 , 82.5)
#coordinator.cutPhaseSpace(0,"y", 1.8, 2.2)
##coordinator.cutPhaseSpace(0,"z", -2 ,2)
#coordinator.cutPhaseSpace(0,"E", 240 ,1000)

#coordinator.plotPhaseSpaces(0,"x")
#coordinator.plotPhaseSpaces(0,"y")
#coordinator.plotPhaseSpaces(0,"y")
#coordinator.plotPhaseSpaces(0,"t")
#

coordinator.getParticleDump(1).plot2D    = 1
coordinator.getParticleDump(1).plotHist  = 0 #"1" current, "2" line charge density
coordinator.getParticleDump(1).plot3D    = 0
#coordinator.getParticleDump(1).setPlotMarker("r.")
coordinator.getParticleDump(1).setPlotMarkerSize(msize)
#coordinator.getParticleDump(1).setTransparency(0.15)
coordinator.getParticleDump(1).setColorCodeComp("E")
coordinator.getParticleDump(1).setColorMap(cmap)
#coordinator.getParticleDump(1).setHistBinSize(0.001)
coordinator.getParticleDump(1).setPlotParticleRatio(ratio)
#coordinator.getParticleDump(1).setHistSmooth(1)
coordinator.getParticleDump(1).comp2D     = 2 #1 == y, 2 == z component
#coordinator.getParticleDump(1).setTransparencyVec("e", norm = (575, 600))




#coordinator.cutPhaseSpace(1,"E",240,1000)
#coordinator.cutPhaseSpace(1,"x", 82.5 , 86)
#####coordinator.cutPhaseSpace(1,"yp",-100, 100)
###
#coordinator.plotPhaseSpaces(1,"x")
#coordinator.plotPhaseSpaces(1,"y")
##coordinator.plotPhaseSpaces(1,"y")
##coordinator.plotPhaseSpaces(1,"t")
#coordinator.plotPhaseSpaces((0,1),"x")

#
coordinator.getParticleDump(2).plot2D    = 0
coordinator.getParticleDump(2).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(2).plot3D    = 0
coordinator.getParticleDump(2).setPlotMarker("b.")
coordinator.getParticleDump(2).setPlotMarkerSize(2)
coordinator.getParticleDump(2).setTransparency(0.05)
#coordinator.getParticleDump(2).setColorCodeComp("e")
coordinator.getParticleDump(2).setColorMap("jet")
coordinator.getParticleDump(2).setHistBinSize(0.001)
coordinator.getParticleDump(2).setPlotParticleRatio(0.02)
coordinator.getParticleDump(2).setHistSmooth(1)
coordinator.getParticleDump(2).comp2D     = 1 #1 == y, 2 == z component
#
#coordinator.cutPhaseSpace(2,"E",0,240)
#coordinator.cutPhaseSpace(2,"x", 70 , 82)
#
#coordinator.plotPhaseSpaces((0,1,2),"x")
#coordinator.plotPhaseSpaces((0),"y")
#coordinator.getParticleDump(1).plotParticleTrajectories([1,50,100,200,250,300,330])
coordinator.cutPhaseSpace(1,"y", -2.5 , 2.5)
"""
#--------------------------------------------------------
#       Plot switches for field dumps
#--------------------------------------------------------
"""
#######  2D switches   ###############################

coordinator.getElecMultiField().plot2D          = 0
coordinator.getElecMultiField().comp2D          = -1 # 0,1,2 for x,y, z and -1 for geometric sum
coordinator.getElecMultiField().plane2D         = ["x", "y",0]
coordinator.getElecMultiField().setColorbar2D("ch_blue")
coordinator.getElecMultiField().plotFieldLO     = 1
coordinator.getElecMultiField().fieldLOComp     = 0
coordinator.getElecMultiField().fieldLORadius   = 4
coordinator.getElecMultiField().fieldLOShift    = [0, 0] #[um]
coordinator.getElecMultiField().plotFieldGradLO = 0
coordinator.getElecMultiField().plotPotLO       = 0
coordinator.getElecMultiField().plotPot2D       = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
#coordinator.getElecMultiField().setPot2DColorMap("magma")
coordinator.getElecMultiField().setPotLOColor("b")
coordinator.getElecMultiField().plot3D          = 0
coordinator.getElecMultiField().colorBarMin = 1.
#coordinator.getElecMultiField().colorBarMax = 100.


coordinator.getSumRhoJ().plot2D                 = 0
coordinator.getSumRhoJ().comp2D                 = 0 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getSumRhoJ().setColorbar2D("gray")
coordinator.getSumRhoJ().plotFieldLO            = 0
coordinator.getSumRhoJ().fieldLOComp            = 0
coordinator.getSumRhoJ().fieldLORadius          = 1
coordinator.getSumRhoJ().fieldLOShift           = [0, 0] #[um]
coordinator.getSumRhoJ().plotFieldGradLO        = 0
coordinator.getSumRhoJ().plotPotLO              = 0
coordinator.getSumRhoJ().plotPot2D              = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getSumRhoJ().setPot2DColorMap("gray")


coordinator.getLasersPlusPlasma().plot2D           = 1
coordinator.getLasersPlusPlasma().comp2D           = 2 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getLasersPlusPlasma().setColorbar2D("gray")
coordinator.getLasersPlusPlasma().plotFieldLO      = 0
coordinator.getLasersPlusPlasma().fieldLOComp      = 0
coordinator.getLasersPlusPlasma().fieldLORadius    = 1
coordinator.getLasersPlusPlasma().fieldLOShift     = [0, 0] #[um]
coordinator.getLasersPlusPlasma().plotFieldGradLO  = 0
coordinator.getLasersPlusPlasma().plotPotLO        = 0
coordinator.getLasersPlusPlasma().plotPot2D        = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getLasersPlusPlasma().setPot2DColorMap("gray")

coordinator.getLasersPlusPlasma().plot3D           = 0



coordinator.getFieldDump(3).plot2D           = 0
coordinator.getFieldDump(3).comp2D           = 0 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getFieldDump(3).fieldLOComp      = 0 
coordinator.getFieldDump(3).plotFieldLO      = 0 
coordinator.getFieldDump(3).plotPotLO        = 0
coordinator.getFieldDump(3).plotFieldGradLO  = 0 



"""
#--------------------------------------------------------
#       2D plotting options
#--------------------------------------------------------
"""

coordinator._Plotter2D.saveFigures            = 1
coordinator._Plotter2D.export                 = 0  
coordinator._Plotter2D.setDumpNote(0)

coordinator._Plotter2D.setPlotPropLen(1)
coordinator._Plotter2D.setPropLenPosX(0.66)
coordinator._Plotter2D.setPropLenPosY(0.9)
coordinator._Plotter2D.setPropLenColor("k")
coordinator._Plotter2D.setDPI(350)           

coordinator._Plotter2D.setColbarLabelColor("k")
coordinator._Plotter2D.setColbarTickLeftColor("w")
coordinator._Plotter2D.setColbarTickRightColor("k")
coordinator._Plotter2D.setColbarZero(1)


coordinator._Plotter2D.setPlotParticleColorbar(1)
coordinator._Plotter2D.setShowColBar(1)


#######  Figure limits   ###############################
coordinator._Plotter2D.setXLim(-60,450)
coordinator._Plotter2D.setYLim(-90,90)
#coordinator._Plotter2D.setYLimFieldLO(-100,100)
coordinator._Plotter2D.setYLimFieldGradLO(-20,20)
coordinator._Plotter2D.setYLimPotLO(-4,4)
coordinator._Plotter2D.setYLimParticleLO(0,20)
coordinator._Plotter2D.setFigSize(8*1.5,3.8*1.5)

 
#######  2D field style options ########################

coordinator._Plotter2D.setFieldLOAxisColor("black")

coordinator._Plotter2D.plotLotov        = 0
coordinator._Plotter2D.lotovRange       = [50, 175] 
coordinator._Plotter2D.lotovDens        = 6.0e22
coordinator._Plotter2D.lotovSlopeFactor = 3.5 
coordinator._Plotter2D.plotTzoufras     = 0

#######  Potential style options #######################

coordinator._Plotter2D.setPotLOAxisColor("blue")
coordinator._Plotter2D.plotPotLOTrappingArea(1)
coordinator._Plotter2D.setPotLOTrappingAreaColor("blue")
coordinator._Plotter2D.setPot2DMax(-1)







"""
#--------------------------------------------------------
#       Phase  space plotting style options
#--------------------------------------------------------
"""


coordinator._PlotterPhaseSpace.saveFigures = 0
coordinator._PlotterPhaseSpace.export      = 0  
coordinator._PlotterPhaseSpace.setDumpNote(1)
coordinator._PlotterPhaseSpace.setDPI(500)

coordinator._PlotterPhaseSpace.setFigSize(7, 6)
coordinator._PlotterPhaseSpace.setPlotPropLen(1)

#coordinator._PlotterPhaseSpace.setXLim(0,20)
#coordinator._PlotterPhaseSpace.setYLim(0,20)
#coordinator._PlotterPhaseSpace.setLabelX("adasd")

coordinator._PlotterPhaseSpace.setPlotParticleColorbar(1)




"""
#--------------------------------------------
#          3D plotting style options
#--------------------------------------------
"""
coordinator._Plotter3D.saveFigures = 1
coordinator._Plotter3D.setDPI(500)
coordinator._Plotter3D.setFigSize(9,9)
coordinator._Plotter3D.setRotation(30, -50)

#coordinator._Plotter3D.setXLim(0,280)
#coordinator._Plotter3D.setYLim(-70,70)
#coordinator._Plotter3D.setZLim(-70,70)

coordinator._Plotter3D.setShowColBar(0)
coordinator._Plotter3D.setShowGrid(0)


"""
#--------------------------------------------------------
#       define parameters that should be analyzed/calculated (print to terminal and saved in summarizer output files!)
#--------------------------------------------------------
"""


coordinator.getParticleDump(0).analyzeDumps(0)
coordinator.getParticleDump(0).saveCharge                  = 1
coordinator.getParticleDump(0).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(0).saveEnergyMean              = 1
coordinator.getParticleDump(0).saveEnergyMax               = 1
coordinator.getParticleDump(0).saveGammaMean               = 1
coordinator.getParticleDump(0).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(0).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(0).saveEnergyDevRMS            = 1
coordinator.getParticleDump(0).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(0).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(0).savePositionMean            = 1
coordinator.getParticleDump(0).savePositionMeanWeighted    = 0
coordinator.getParticleDump(0).savePositionMeanLabframe    = 1
coordinator.getParticleDump(0).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(0).saveBunchLengthRMS          = 1
coordinator.getParticleDump(0).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(0).saveBunchLengthMax          = 1
coordinator.getParticleDump(0).saveWidthRMS                = 1
coordinator.getParticleDump(0).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(0).saveWidthMax                = 1
coordinator.getParticleDump(0).saveDivergenceRMS           = 1
coordinator.getParticleDump(0).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(0).saveEmittance               = 1
coordinator.getParticleDump(0).saveAccField                = 1
coordinator.getParticleDump(0).saveZeroCrossing            = 0
coordinator.getParticleDump(0).saveCurrentMax              = 1
coordinator.getParticleDump(0).saveBrightness5D            = 1
coordinator.getParticleDump(0).saveAccFieldGradient        = 1
coordinator.getParticleDump(0).saveEmittanceDispersion        = 1
                       
                       
coordinator.getParticleDump(1).analyzeDumps(0)
coordinator.getParticleDump(1).saveCharge                  = 1
coordinator.getParticleDump(1).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(1).saveEnergyMean              = 1
coordinator.getParticleDump(1).saveEnergyMax               = 0
coordinator.getParticleDump(1).saveGammaMean               = 0
coordinator.getParticleDump(1).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(1).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(1).saveEnergyDevRMS            = 1
coordinator.getParticleDump(1).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(1).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(1).savePositionMean            = 1
coordinator.getParticleDump(1).savePositionMeanWeighted    = 0
coordinator.getParticleDump(1).savePositionMeanLabframe    = 0
coordinator.getParticleDump(1).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(1).saveBunchLengthRMS          = 0
coordinator.getParticleDump(1).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(1).saveBunchLengthMax          = 1
coordinator.getParticleDump(1).saveWidthRMS                = 1
coordinator.getParticleDump(1).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(1).saveWidthMax                = 0
coordinator.getParticleDump(1).saveDivergenceRMS           = 1
coordinator.getParticleDump(1).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(1).saveEmittance               = 1
coordinator.getParticleDump(1).saveAccField                = 1
coordinator.getParticleDump(1).saveZeroCrossing            = 1
coordinator.getParticleDump(1).saveCurrentMax              = 1
coordinator.getParticleDump(1).saveBrightness5D            = 1
coordinator.getParticleDump(1).saveEmittanceDispersion        = 1



coordinator.getParticleDump(2).analyzeDumps(0)
coordinator.getParticleDump(2).saveCharge                  = 1
coordinator.getParticleDump(2).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(2).saveEnergyMean              = 1
coordinator.getParticleDump(2).saveEnergyMax               = 0
coordinator.getParticleDump(2).saveGammaMean               = 0
coordinator.getParticleDump(2).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(2).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(2).saveEnergyDevRMS            = 1
coordinator.getParticleDump(2).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(2).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(2).savePositionMean            = 1
coordinator.getParticleDump(2).savePositionMeanWeighted    = 0
coordinator.getParticleDump(2).savePositionMeanLabframe    = 0
coordinator.getParticleDump(2).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(2).saveBunchLengthRMS          = 0
coordinator.getParticleDump(2).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(2).saveBunchLengthMax          = 1
coordinator.getParticleDump(2).saveWidthRMS                = 1
coordinator.getParticleDump(2).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(2).saveWidthMax                = 0
coordinator.getParticleDump(2).saveDivergenceRMS           = 1
coordinator.getParticleDump(2).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(2).saveEmittance               = 1
coordinator.getParticleDump(2).saveAccField                = 1
coordinator.getParticleDump(2).saveZeroCrossing            = 1
coordinator.getParticleDump(2).saveCurrentMax              = 1
coordinator.getParticleDump(2).saveBrightness5D            = 1


coordinator.getParticleDump("driver").analyzeDumps(0) 
coordinator.getParticleDump("driver").saveCharge                  = 1
coordinator.getParticleDump("driver").saveEnergyMeanWeighted      = 0
coordinator.getParticleDump("driver").saveEnergyMean              = 1
coordinator.getParticleDump("driver").saveEnergyMax               = 0
coordinator.getParticleDump("driver").saveGammaMean               = 1
coordinator.getParticleDump("driver").saveGammaMeanWeighted       = 0
coordinator.getParticleDump("driver").saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump("driver").saveEnergyDevRMS            = 1
coordinator.getParticleDump("driver").saveEnergySpreadRMS         = 1
coordinator.getParticleDump("driver").saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump("driver").savePositionMean            = 1
coordinator.getParticleDump("driver").savePositionMeanWeighted    = 0
coordinator.getParticleDump("driver").savePositionMeanLabframe    = 1
coordinator.getParticleDump("driver").savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump("driver").saveBunchLengthRMS          = 1
coordinator.getParticleDump("driver").saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump("driver").saveBunchLengthMax          = 1
coordinator.getParticleDump("driver").saveWidthRMS                = 1
coordinator.getParticleDump("driver").saveWidthRMSWeighted        = 0
coordinator.getParticleDump("driver").saveWidthMax                = 0
coordinator.getParticleDump("driver").saveDivergenceRMS           = 1
coordinator.getParticleDump("driver").saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump("driver").saveEmittance               = 1
coordinator.getParticleDump("driver").saveAccField                = 0
coordinator.getParticleDump("driver").saveZeroCrossing            = 0
coordinator.getParticleDump("driver").saveCurrentMax              = 1
coordinator.getParticleDump("driver").saveBrightness5D            = 0
coordinator.getParticleDump("driver").saveAccFieldGradient        = 1
  
                       
coordinator.getElecMultiField().analyzeDumps(0)
coordinator.getElecMultiField().saveZeroCrossing            = 1
coordinator.getElecMultiField().saveMinField                = 1
coordinator.getElecMultiField().saveMaxField                = 1
coordinator.getElecMultiField().saveTransformerRatio        = 1
coordinator.getElecMultiField().saveMinPotential            = 1

coordinator.getLasersPlusPlasma().analyzeDumps(0)
coordinator.getLasersPlusPlasma().saveZeroCrossing            = 1
coordinator.getLasersPlusPlasma().saveMinField                = 1
coordinator.getLasersPlusPlasma().saveMaxField                = 1
coordinator.getLasersPlusPlasma().saveTransformerRatio        = 1


coordinator.getFieldDump(3).analyzeDumps(0)
coordinator.getFieldDump(3).saveZeroCrossing            = 1
coordinator.getFieldDump(3).saveMinField                = 1
coordinator.getFieldDump(3).saveMaxField                = 1
coordinator.getFieldDump(3).saveTransformerRatio        = 1


"""
#--------------------------------------------------------
#       execute code
#--------------------------------------------------------
"""

coordinator.dumpLoop()
     
"""   
#--------------------------------------------------------
#       plot summarized data
#--------------------------------------------------------
"""
    
if coordinator.plotSummarizedDumps:
#    coordinator._Summarizer.plotData("x",("driverCharge","HeElectronscharge",))
    coordinator._Summarizer.plotData("x",("driverCharge","0charge")) # note: putting a number into the string gives the corresponding witness beam (ofc, you can also use its name)
#    coordinator._Summarizer.plotData("x",("driverCharge", "EnergyMaxdriver"))
    coordinator._Summarizer.exportDataToFile()

del coordinator


