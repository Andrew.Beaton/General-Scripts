# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 13:33:23 2016

@author: Paul Scherkl
"""

from dataManager import *
import matplotlib
matplotlib.use('Agg') # for use on cluster: uncomment this dude!

from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import matplotlib.gridspec as gridspec
import scipy.ndimage as ndimage
from scipy import signal
import matplotlib.colors as colors
import colormaps as customColMaps
import matplotlib.cm as cm



class plotter(object):
    """definition of class maintaining all plotting and output routines """   
    
    
    def __init__(self, manager):
        self._M = manager
        self.saveFigures        = 1    
        self.outFileFormat      = "png" 
        
        self.figure            = 0
        self.figSize           = []
        self.DPI               = 300
       
        self.plotBeamElectrons = 0
        self.plotWitnessBeams  = 0
             
        self.plotPropLen        = 0   
        self.propLenColor       = "k"
        self.propLenPosX        = 0.65
        self.propLenPosY        = 0.9
        
        self.dumpNote           = 0

        self.export             = 0


    def setDumpNote(self, switch):
        """turns on/off terminal output for dumped txt files"""
        self.dumpNote = switch



        
#--------------------------------------------------------
#       General helper functions
#--------------------------------------------------------

        
        
    def setFigSize(self, xSize, ySize):
        """sets size of output long. plot"""
        self.figSize = [xSize, ySize]
        
        
    def setDPI(self, dpi):
        """defines DPI of saved  figures (for all outFileFormats)"""
        if isinstance(dpi, (int,float)):
            self.DPI = dpi
        else:
            raise Exception("Wrong input for DPI entered. Please use a number (e.g. 400)")
        
        
    def exportData(self, data, name):
        """exports arbitrary data to file (at pathToData)"""
        outfile = self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber)+"_" + str(name) +".txt"
        np.savetxt(outfile, data, fmt='%.9f', delimiter='    ', newline='\n', header= '', footer='', comments='')
        
        
        
    def setOutFileFormat(self, fileFormat):
        """Set format for output file to either pdf, png, jpg"""
        if isinstance(fileFormat, basestring):
            if fileFormat.lower() == "pdf":
                self.outFileFormat = "pdf"
            elif fileFormat.lower() == "png" or fileFormat.lower() == "jpg":
                self.outFileFormat = fileFormat
            elif fileFormat.lower() == "svg":
                self.outFileFormat = "svg"
            elif fileFormat.lower() == "eps":
                self.outFileFormat = "eps"
            else:
                raise Exception("Wrong OutFileFormat requested. Please choose from pdf, jpg, png, eps, or svg")
        else:
                raise Exception("Wrong OutFileFormat requested. Please choose from pdf, jpg, png, eps, or svg")
        
        
        
    def setPlotPropLen(self, switch):
        "switch on propagation coordinate in plots.. 0 means x in mm, 1 means t in ps"
        self.plotPropLen = switch
        
    def setPropLenColor(self, color):
        "changes color of propagation length string in main plot"
        self.propLenColor = color
        
    def setPropLenPosX(self, ratio):
        "changes x Position of propagaton length string in percent of the window width"
        self.propLenPosX           = ratio

    def setPropLenPosY(self, ratio):
        "changes y Position of propagaton length string in percent of the window width"
        self.propLenPosY          = ratio

       
        
class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))        
        
        
        
        
        
        
        
        
        
        
        
        