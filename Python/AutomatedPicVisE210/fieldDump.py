# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 11:27:03 2016

@author: Paul Scherkl
"""

from dump import *
from matplotlib import pyplot as plt
import numpy as np
import math

class fieldDump(dump):
    """class definition for field Dump objects saving field matrix and grid information. """
    
    
    def __init__(self, name):
        dump.__init__(self, name, "field")
        
        self.fieldType = ""
    
    
        self.fieldMatrix = 0
        self.yAxis       = 0
        self.xAxis       = 0
        self.zAxis       = 0

        
        self.numCellX  = 0
        self.numCellY  = 0
        self.numCellZ  = 0
        self.cellSizeX = 0
        self.cellSizeY = 0
        self.cellSizeZ = 0
        self.centerCellTransY      = 0
        self.centerCellTransZ      = 0

        self.plane2D         = ["x", "y", 0]
        self.comp2D          = 0
        self.colorbar2D      = plt.cm.get_cmap('YlGnBu_r')
        self.colorbar2D.set_under('blue')
        self.colorbar2D.set_over('red')
        self.colorBarMin = ""
        self.colorBarMax = ""
        self.showColorbar = 1

        self.plotFieldLO          = 0
        self.fieldLO              = 0
        self.fieldLORadius        = 6
        self.fieldLOComp          = 0
        self.fieldLOShift         = [0,0]
        self.fieldLOZeroCrossing  = 0
        self.fieldLOColor  = "k"
        self.fieldLOWidth  = 1
 
        self.plotFieldGradLO          = 0
        self.fieldGradLO              = 0
        self.fieldGradLORadius        = 0
        self.fieldGradLOComp          = 0
        self.fieldGradLOShift         = 0
        self.fieldGradLOZeroCrossing  = 0
        self.fieldGradLOColor         = "g"
        self.fieldGradLOWidth         = 1

       
        self.plotPotLO            = 0
        self.potLO                = 0
        self.potLOWidth           = 1
        self.potLOColor           = "b"
        self.pot2D                = 0
        self.plotPot2D            = 0
        self.pot2DColorMap        = plt.cm.get_cmap("hot")

        self.colorbar3D           = plt.cm.get_cmap('YlGnBu_r')
        self.saveZeroCrossing            = 0
        self.saveMinField                = 0
        self.saveMaxField                = 0
        self.saveTransformerRatio        = 0
        self.saveMinPotential            = 0

        self.cutValue = ""
        self.cutZoomFactor = 1
        self.cutDirection  = "<"
        
        
        
    def reset(self):
        """deletes all individual properties"""
        self.loaded      = 0
        self.fieldMatrix = 0
        self.yAxis       = 0
        self.xAxis       = 0
        self.zAxis       = 0
        self.windowStart = 0
        self.windowEnd   = 0
        self.numCellX    = 0
        self.numCellY    = 0
        self.numCellZ    = 0
        self.cellSizeX   = 0
        self.cellSizeY   = 0
        self.cellSizeZ   = 0
        self.centerCellTransY     = 0
        self.centerCellTransZ     = 0
        self.fieldLO              = 0
        self.fieldGradLO          = 0
        self.fieldLOZeroCrossing  = 0
        self.potLO                = 0
        self.pot2D                = 0




    def cutValues(self, cutVal, **kwargs):
        """ cuts field values 
        
        @cutVal: threshold value in field units (GV/m for electrics, .. ). 
        
        
        **kwargs:
        @cutZoomFactor (int, default 1); increases the cell density for the given field        
        @cutDirection: (string, default "<" (deltes values smaller than @cutVal)): can be "<" or ">". 
        """
        if isinstance(cutVal, (int, float)):
            self.cutValue = cutVal
        else:
            print " (!)  Wrong input for cutValue, use int or float value in field units. Command will be ignored"
        
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                if key.lower() == "zoomfactor":
                    if isinstance(value, int) and value > 1:
                        self.cutZoomFactor = value
                    else:
                        print " (!)  Wrong input for zoomfactor, use int value > 1. Command will be ignored"
                        
                if key.lower() == "cutdirection":
                    if value == "<" or value == ">":
                        self.cutDirection = value
                    else:
                        print " (!)  Wrong input for cutdirection, use \"<\" or \">\". Command will be ignored"
                




    def setColorbar3D(self, cmap):
        self.colorbar3D = plt.cm.get_cmap(cmap)

        
    def setPot2DColorMap(self, cmap):
        self.pot2DColorMap = plt.cm.get_cmap(cmap)
        
    def setFieldLOAxisColor(self, color):
        """changes color of fieldLO y-axis"""
        self.fieldLOAxisColor = color

    def setFieldLOAxisShiftFactor(self, shiftFactor):
        """changes color of fieldLO y-axis"""
        self.fieldLOAxisShiftFactor = shiftFactor



    def setFieldGradLOAxisColor(self, color):
        """changes color of fieldGradLO y-axis"""
        self.fieldLOAxisColor = color

    def setFieldGradLOAxisShiftFactor(self, shiftFactor):
        """changes color of fieldGradLO y-axis"""
        self.fieldLOAxisShiftFactor = shiftFactor


    def setPotLOAxisColor(self, color):
        """changes color of potLO y-axis"""
        self.potLOAxisColor = color

    def setPotLOAxisShiftFactor(self, shiftFactor):
        """changes color of potLO y-axis"""
        self.potLOAxisShiftFactor = shiftFactor

              
    def setColorbar2D(self, cbarName):
        """changes colorbar of 2D field plot"""
        self.colorbar2D = plt.cm.get_cmap(cbarName)   
            
        
    def setFieldLOColor(self, color):
        """changes color of field lineout plot"""
        self.fieldLOColor = color        
        
    def setFieldGradLOColor(self, color):
        """changes color of fieldGrad lineout plot"""
        self.fieldGradLOColor = color        
        
    def setPotLOColor(self, color):
        """changes color of longitudinal potential"""
        self.potLOColor = color
        
       
    def setFieldLOWidth(self, width):
        """changes linewidth of field lineout"""
        self.fieldLOWidth = width
        
    def setFieldGradLOWidth(self, width):
        """changes linewidth of fieldGrad lineout"""
        self.fieldGradLOWidth = width

    def setPotLOWidth(self, width):
        """changes linewidth of potential lineout"""
        self.potLOWidth = width
    

  
    def get2DPlane(self):
        """function evaluates field in plane specified by user and saves it for plotting"""
        if (len([x for x in self.plane2D if isinstance(x,basestring)]) is not 2 and len([x for x in self.plane2D if isinstance(x, (float, int))]) is not 1) or ((self.plane2D[0] == self.plane2D[1]) or (self.plane2D[0] == self.plane2D[2]) or (self.plane2D[1] == self.plane2D[2])):
            raise Exception("2D plane for "+self.name+" not properly defined. Please specify exactly TWO (differing) coordinates with strings x, y, or z, and the other one with a number. E.g. use [\"x\", \"y\", 0] to select x-y-plane in for z = 0")
        
        if isinstance(self.plane2D[0], basestring) and isinstance(self.plane2D[1], basestring):
            if self.plane2D[0].lower() == "x" and self.plane2D[1] == "y":
                zComp = self.centerCellTransZ + int(self.plane2D[2]/self.cellSizeZ)
                if abs(self.plane2D[2]) >= self.numCellZ*self.cellSizeZ/2*0.99:
                    raise Exception("   (!) Requested z-component " + str(self.plane2D[2]) + " um for " + self.name + " out of range (+-)"+str((self.numCellZ*self.cellSizeZ/2)) +" um" )
                return self.fieldMatrix[:, :, zComp , self.comp2D]
                
        elif isinstance(self.plane2D[0], basestring) and isinstance(self.plane2D[2], basestring):
            if self.plane2D[0].lower() == "x" and self.plane2D[2].lower() == "z":
                yComp = self.centerCellTransY + int(self.plane2D[1]/self.cellSizeY)
                if abs(self.plane2D[1]) >= self.numCellY*self.cellSizeY/2*0.99:
                    raise Exception("   (!) Requested y-component " + str(self.plane2D[1]) + " um for " + self.name + " out of range (+-)"+str((self.numCellY*self.cellSizeY/2)) +" um" )
                return self.fieldMatrix[:, yComp, : , self.comp2D]
        elif isinstance(self.plane2D[1], basestring) and isinstance(self.plane2D[2], basestring):
            if self.plane2D[1].lower() == "y" and self.plane2D[2].lower() == "z":
                xComp = self.centerCellTransY + int(self.plane2D[0]/self.cellSizeX)
                if abs(self.plane2D[0]) >= self.numCellX*self.cellSizeX/2*0.99:
                    raise Exception("   (!) Requested x-component " + str(self.plane2D[0]) + " um for " + self.name + " out of range (+-)"+str((self.numCellX*self.cellSizeX/2)) +" um" )
                return self.fieldMatrix[xComp, :, : , self.comp2D]
        else:
            raise Exception("   (!) Please don't try to use two identical vectors to define a plane..")
            
         
         
    def get2DPlaneFieldSum(self):
        """function evaluates field in plane specified by user and returns fieldsum!"""
        if (len([x for x in self.plane2D if isinstance(x,basestring)]) is not 2 and len([x for x in self.plane2D if isinstance(x, (float, int))]) is not 1) or ((self.plane2D[0] == self.plane2D[1]) or (self.plane2D[0] == self.plane2D[2]) or (self.plane2D[1] == self.plane2D[2])):
            raise Exception("   (!) 2D plane for "+self.name+" not properly defined. Please specify exactly TWO (differing) coordinates with strings x, y, or z, and the other one with a number. E.g. use [\"x\", \"y\", 0] to select x-y-plane in for z = 0")
        
        if isinstance(self.plane2D[0], basestring) and isinstance(self.plane2D[1], basestring):
            if self.plane2D[0].lower() == "x" and self.plane2D[1] == "y":
                zComp = self.centerCellTransZ + int(self.plane2D[2]/self.cellSizeZ)
                if abs(self.plane2D[2]) >= self.numCellZ*self.cellSizeZ/2*0.99:
                    raise Exception("   (!) Requested z-component " + str(self.plane2D[2]) + " um for " + self.name + " out of range (+-)"+str((self.numCellZ*self.cellSizeZ/2)) +" um" )
                if self.comp2D == -1:
                    return np.sqrt(self.fieldMatrix[:, :, zComp , 0]**2 + self.fieldMatrix[:, :, zComp , 1]**2 + self.fieldMatrix[:, :, zComp , 2]**2)
                elif self.comp2D == -2:
                    return np.sqrt( self.fieldMatrix[:, :, zComp , 1]**2 + self.fieldMatrix[:, :, zComp , 2]**2)
                
        elif isinstance(self.plane2D[0], basestring) and isinstance(self.plane2D[2], basestring):
            if self.plane2D[0].lower() == "x" and self.plane2D[2].lower() == "z":
                yComp = self.centerCellTransY + int(self.plane2D[1]/self.cellSizeY)
                if abs(self.plane2D[1]) >= self.numCellY*self.cellSizeY/2*0.99:
                    raise Exception("   (!) Requested y-component " + str(self.plane2D[1]) + " um for " + self.name + " out of range (+-)"+str((self.numCellY*self.cellSizeY/2)) +" um" )
                if self.comp2D == -1:
                    return np.sqrt(self.fieldMatrix[:, yComp, : , 0]**2+self.fieldMatrix[:, yComp, : , 1]**2+self.fieldMatrix[:, yComp, : , 2]**2)
                elif self.comp2D == -2:
                    return np.sqrt(self.fieldMatrix[:, yComp, : , 1]**2+self.fieldMatrix[:, yComp, : , 2]**2)
        elif isinstance(self.plane2D[1], basestring) and isinstance(self.plane2D[2], basestring):
            if self.plane2D[1].lower() == "y" and self.plane2D[2].lower() == "z":
                xComp = self.centerCellTransY + int(self.plane2D[0]/self.cellSizeX)
                if abs(self.plane2D[0]) >= self.numCellX*self.cellSizeX/2*0.99:
                    raise Exception("   (!) Requested x-component " + str(self.plane2D[0]) + " um for " + self.name + " out of range (+-)"+str((self.numCellX*self.cellSizeX/2)) +" um" )
                if self.comp2D == -1:
                    return np.sqrt(self.fieldMatrix[xComp, :, : , 0]**2+self.fieldMatrix[xComp, :, : , 1]**2+self.fieldMatrix[xComp, :, : , 2]**2)
                elif self.comp2D == -2:
                    return np.sqrt(self.fieldMatrix[xComp, :, : , 1]**2+self.fieldMatrix[xComp, :, : , 2]**2)
        else:
            raise Exception("   (!) Please don't try to use two identical vectors to define a plane..")         
         
         
         
         
         
         
         
        
        
    def getFieldLO(self):     
        """Function calculates lineout in longitudinal direction for each cell-step.
        Hereby, fields (field direction given by @fieldComp!) within circles of given @radius in y-z-plane are averaged 
        (summary: longitudinal tube in center of simulation box is transformed into lineout)
        Note: assumes symmetric transverse grid"""
        if self.fieldLO is not 0:
            return self.fieldLO
        else:
            fieldComp        = self.fieldLOComp
            shiftY           = int(self.fieldLOShift[0]/self.cellSizeY)
            shiftZ           = int(self.fieldLOShift[1]/self.cellSizeZ)
            averageRadius    = self.fieldLORadius
            
            if np.shape(self.fieldMatrix)[3] < fieldComp:
                print "   (!) Requested field component " + str(fieldComp) + " for field of type " + self.name + " is not available. Selected field has " + str(np.shape(self.fieldMatrix)[3]) + " components."
                return np.zeros(self.numCellX)
            
            numCellsInRadius = int(averageRadius/self.cellSizeY)
            
            
            yRangeStart = self.centerCellTransY + shiftY - numCellsInRadius
            yRangeStop  = self.centerCellTransY + shiftY + numCellsInRadius   
            if yRangeStart > yRangeStop:
                dummy = yRangeStart
                yRangeStart = yRangeStop
                yRangeStop  = dummy
            if yRangeStart <  self.centerCellTransY - self.numCellY/2:
                yRangeStart = self.centerCellTransY - int(self.numCellY/2)
            elif yRangeStart > self.centerCellTransY + self.numCellY/2:
                raise Exception("   (!) Given lineout y-shift of " + str(shiftY) + " um is not within simulation box (must be smaller than "+str(self.cellSizeY*self.numCellY/2)+" um)")
    
            if yRangeStop >  self.centerCellTransY + self.numCellY/2:
                yRangeStop = yRangeStop + int(self.numCellY/2)
            
            FieldOnAxis = np.zeros(self.numCellX)
            for x in range(self.numCellX):
                sliceField = 0
                counter    = 0
                for y in range(yRangeStart, yRangeStop ):
                    zRangeStart = self.centerCellTransZ - int(math.sqrt(numCellsInRadius**2-(y-self.centerCellTransY)**2)) + shiftZ
                    zRangeStop  = self.centerCellTransZ + int(math.sqrt(numCellsInRadius**2-(y-self.centerCellTransY)**2)) + shiftZ
                    if zRangeStart > zRangeStop:
                        dummy = zRangeStart
                        zRangeStart = zRangeStop
                        zRangeStop  = dummy
                    if zRangeStart <  self.centerCellTransZ - self.numCellZ/2:
                        zRangeStart = self.centerCellTransZ - int(self.numCellZ/2)
                    elif zRangeStart > self.centerCellTransZ + self.numCellZ/2:
                        raise Exception("   (!) Given lineout z-shift of " + str(shiftZ) + " um is not within simulation box (must be smaller than "+str(self.cellSizeZ*self.numCellZ/2)+" um)")
                    if zRangeStop >  self.centerCellTransZ + self.numCellZ/2:
                        zRangeStop = zRangeStop + int(self.numCellZ/2)
                    for z in range(zRangeStart, zRangeStop):
                        sliceField = sliceField +  self.fieldMatrix[x][y][z][fieldComp]
                        counter = counter +1
                if counter > 0 :
                    FieldOnAxis[x] = (sliceField/counter)
            self.fieldLO = FieldOnAxis
            if self.fieldLOComp ==2 :
                print self.fieldLO
            
            return self.fieldLO

  
            
            
            
    def getFieldZeroCrossing(self):
        return self.getLOZeroCrossing(self.getFieldLO())        
  
    
    def getLOZeroCrossing(self, lineOut):
        """ obtains longitudinal zero-crossing position of any lineout WITHIN blowout. """
        fieldMin = np.argmin(lineOut)
        for x in range (fieldMin, self.numCellX):
            if lineOut[x-1] < 0 and lineOut[x] >= 0:
                return x*self.cellSizeX
                    
    def getFieldGradLO(self):
        """calculates gradient for the electric field lineout (field, fieldcomponent, etc)"""
        if isinstance(self.fieldGradLO, (int, float)):
            self.fieldGradLO = np.gradient(self.getFieldLO())
        return self.fieldGradLO
        
    
    def getPotLO(self):
        """calculates potential for the electric field lineout (field, fieldcomponent, etc)"""
        if isinstance(self.potLO, (int, float)):
            fieldLineOut = 0
            if self.fieldLO is not 0:
                fieldLineOut = self.fieldLO
            else:
                fieldLineOut = self.getFieldLO()
            self.potLO = self.getLinePotential(fieldLineOut, 0)
        return self.potLO



    def getPotLOMin(self):
        return np.min(self.getPotLO())




        
    def getLinePotential(self, line, switch):
        """calculates electric trapping potential for a line in x-direction"""
        
        potential = np.zeros(len(line))
        a = np.linspace(len(line)-2,0, len(line)-2)
        for i in a:
                i = np.int(i)
                potential[i] = potential[i + 1] - np.float32(line[i])*1e9
        potential = potential/511706.6619*self.cellSizeX*1e-6 #normalize to mc^2/e
        if switch == 0:
            potential = potential - np.max(potential)
        return potential
        
        
    def getPot2D(self):
        """calculates 2D trapping potential"""
        if isinstance(self.pot2D, (int, float)):
            plane = self.get2DPlane()
            potentialPlane = plane * 0
            for i in range(self.numCellY):
                curentLine = plane[:,i]
                potentialPlane[:,i] =  self.getLinePotential(curentLine, 1)
                potentialPlane[:,i] = potentialPlane[:,i] - np.max(potentialPlane[:,i])
#            potentialPlane = potentialPlane - np.max(potentialPlane)
            self.pot2D = potentialPlane
        return self.pot2D
            
            
    def getField(self, x):
        """returns field value at given position from lineout"""
        if isinstance(self.fieldLO, (int, float)):
            self.getFieldLO()
        lineOut = self.fieldLO    
        return lineOut[int(np.abs((x-np.min(self.xAxis))/self.cellSizeX))]     
        
    def getFieldGradient(self, x):
        """returns gradient value at given position from lineout"""
        gradient = self.getFieldGradLO()
        return gradient[int(np.abs((x-np.min(self.xAxis))/self.cellSizeX))]
                       
    def getFieldLOMax(self):
        """return the max of the field lineout close to the driver (for transformer ratio)"""
        lineout = self.getFieldLO()
        return np.max(lineout[(len(lineout)/2):])
        
    def getFieldLOMin(self):
        """return the max of the field lineout close to the driver (for transformer ratio)"""
        lineout = self.getFieldLO()
        return np.min(lineout)
        
    def getFieldTransformerRatio(self):
        """returns the transformer ratio"""
        return np.abs(self.getFieldLOMin()/self.getFieldLOMax())
                       
                       