# -*- coding: utf-8 -*-


import functions
from matplotlib import pyplot as plt

import numpy as np
import pandas
import scipy.constants as const 

Path= "G:"
RunName="20180303_TrojanV4_1_DumpPer0_05ps05mj"
Species="BeamElectrons"
Dumpnumber=200
#SimData=pandas.read_excel("G:\\Results_all.xls")

LocName=Path+RunName
print LocName
BeamData=np.array(functions.loadh5(LocName,Species,Dumpnumber))

#Loads the data into specific arrays for each sheet

Data_05mj=pandas.read_excel("G:\\Results_all.xls",sheet_name="0.5mj")
Data_1mj=pandas.read_excel("G:\\Results_all.xls",sheet_name="1mj")
Data_5mj=pandas.read_excel("G:\\Results_all.xls",sheet_name="5mj")

averageX=np.average(BeamData[:,0])
print averageX


#  Corrilate timing 

DriveBeamXAverage_T=averageX/const.c
DriveBeamX_T=BeamData[:,0]/const.c
#Changes the x location to time
BeamPanda=pandas.DataFrame(DriveBeamX_T)



#Syncs to the middle of the drive beam 
print BeamPanda.describe()
BeamQcut=pandas.qcut(BeamPanda[0],q=100,precision=16)
#print BeamQcut.value_counts()

DriveBeamX_T_syncd=DriveBeamX_T -DriveBeamXAverage_T#1.330521e-09





T_zero_1=functions.E210BeamZero(40e-6)



T_zero_2=(BeamPanda[0].max() - BeamPanda[0].min())







T_zero_Delta=T_zero_1-T_zero_2

print T_zero_Delta
Timing_zerod=(Data_05mj["Timing"]*-1e-12)-T_zero_Delta
#Plotting


grid = plt.GridSpec(4,4, hspace=.5, wspace=0.2)

fig = plt.figure(figsize=(6, 6), dpi= 80)
#a1 = fig.add_axes([0,0,1,1])
a1 = fig.add_subplot(grid[:-1,:])
ax_bottom = fig.add_subplot(grid[3,:], xticklabels=[], yticklabels=[])
#ax_right = fig.add_subplot(grid[:-1,-1], xticklabels=[], yticklabels=[])
a1.scatter(DriveBeamX_T_syncd,BeamData[:,1],alpha=0.002)



a2=a1.twinx()
a2.scatter(Timing_zerod,Data_05mj["Emittance"],color="purple")
a2.set_ylim(0,100)
#a1.set_xlim(0.3985,0.3992)

xmin=np.min(DriveBeamX_T_syncd)-(0.8*np.max(DriveBeamX_T_syncd))
xmax=np.max(DriveBeamX_T_syncd)+(0.1*np.max(DriveBeamX_T_syncd))
Xrange=(xmin,xmax)

ymin=-200e-6
ymax=200e-6

Yrange=(ymin,ymax)
a1.set_xlim(xmin,xmax)
a1.set_ylim(ymin,ymax)

a1.axvline(x=0,color="red",linestyle="dashed",alpha=0.5)


ax_bottom.hist(DriveBeamX_T_syncd,60, histtype='stepfilled', orientation='vertical', color='navy',range=Xrange)
ax_bottom.invert_yaxis()
ax_bottom.axvline(x=0,color="red",linestyle="dashed",alpha=0.5)

ax_bottom.set_xlim(xmin,xmax)
#ax_bottom.set_ylim(ymin,ymax)

#ax_right.hist(BeamData[:,1],100, histtype='stepfilled', orientation='horizontal', color='deeppink',range=Yrange)

plt.show()