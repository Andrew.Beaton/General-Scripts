# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 13:28:58 2019

@author: Andrew & Fahim
"""

import math
import scipy.constants as c
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.integrate import quad
from matplotlib.ticker import MaxNLocator
import datetime
now = datetime.datetime.now()


def ChargePerUnitLength(nb,np,kp,transverseSize): #taken from W.Lu thesis around page 40 - 45
    Clambda = (nb/np)*(PlasmaWaveNumber(np)**2) * (transverseSize)**2 #A version only used in part of hte thesis


    return Clambda



def ChargePerUnitLength2(nb,np,kp,transverseSize): #taken from W.Lu thesis around page 40 - 45

    ChargePerUnitLength = nb*(transverseSize**2) #  for a bi gaussian electron beam
    #ChargePerUnitLength2=

    return ChargePerUnitLength

def Wavelength(np):
    Wl = 3.34e7/(np**0.5)
    return Wl


def PlasmaWaveNumber(np):
    k = 2*math.pi / Wavelength(np)

    return k

def EstimateEmax(CPUL,APF):
    # Page 43 equation 43
    logva = 1/((CPUL/10)**0.5)
    Emax = ((c.m_e*c.c*APF)/c.e)*(1.3*CPUL*math.log(logva,math.e))


    return Emax

def PlasmaAngularFrequency(n0):
    wp=((n0*c.e**2)/(c.m_e*c.epsilon_0))**0.5

    return wp

def PlasmaFrequency(n0):
    wp=PlasmaAngularFrequency(n0)
    return (wp/(2*math.pi))

def RadiusMax(cpul):
    #approx radius of blowout pg32
    rm = ((cpul)**0.5)*2.58
    return rm

def Peak_EbeamDensity(q,sigr,sigz):
	Peak = (q/c.elementary_charge) / (((2*math.pi)**3/2) * (sigr**2)*sigz)
	return Peak

def DriverBeam(x=None,r=None, mu=None, sig=None):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) * np.exp(-np.power(r, 2.) / (2 * np.power(sig, 2.)))

def WitnessBeam(x=None,r=None, mu=None, sig=None):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) * np.exp(-np.power(r, 2.) / (2 * np.power(sig, 2.)))

##################################
# Beam density calculations
##################################
sigr1 = 10e-6
sigz1 = 20e-6
q1    = 2e-9
BeamD = Peak_EbeamDensity(q1,sigr1,sigz1) #Peak electron beam deisnty

def UltraRelativisticCheck(Data):

    if np.max(Data)<2 or np.max(Data)>100 :
        print" WARNING  : You are not in the Ultra Relativistic regime, the data may not be reliable"

    else :
        print " In the Ultra Relativistic regime"

##################################
    #Fahims model functions - Ultra Relativistic
##################################




def DriverBeam(x=None,r=None, mu=None, sig=None):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) * np.exp(-np.power(r, 2.) / (2 * np.power(sig, 2.)))

def WitnessBeam(x=None,r=None, mu=None, sig=None):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.))) * np.exp(-np.power(r, 2.) / (2 * np.power(sig, 2.)))

# Simple Wakefield Model See W.Lu paper
def modelWLu(Rb,xi,mu,sig,nb,r):
    rb,drb =Rb

    Lambda =nb*DriverBeam(xi,r,mu, sig)#No witness beam only drive beam  #+2.0*nb*DriverBeam(xi,0.05,mu+2.0, sig*0.3) # A driver with gaussian distribution
    drbdxi = [drb, (4*Lambda * rb**(-3) -  2 * rb**(-1) * (drb)**2 -  rb**(-1)) ] # System of first order ODEs
    return drbdxi


##################################
    #Cylinder Approximations
##################################


def EcylinderVD (q,r,Nr,count):#Charge, range of r values , density array, position in the array

    #Chapter 5:Introduction to beam-generated Forces --Charged particle beams Stanley Humphries

    # Rmax=r[c]
    Rmin=r[0]

    E= (q/(2* math.pi* c.epsilon_0 * r[count]))
    Integrate = quad(lambda r: 2*math.pi *r,Rmin,r[count])

    #print Integrate
    EF= E*Integrate[0]*Nr[count]
    return EF

#Basic Gaussian function

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))

  return Gauss

########################


#Super Gaussian beam
def SuperGaussian(r,rl,n):#delta0,r,N):
    #SGauss= (1/((2*spc.constants.pi)**0.5)*delta0)*np.exp((-(r**N))/(2*delta0**N))
    SGauss =np.exp(-(r/rl)**(2*n))
    return SGauss


########################


##################################
    #User Input - Intermediate regieme
##################################


n0=1.2395e22#300 um  1.2395e22 E210 1.3e23
nb =7.93e22 # beam density
Br=15e-6

###
# Array setup
##
n01 = 1.08e23
n02 = 2.71e22
# 100um=1.08e23 200um= 2.71e22 300um= 1.21e22 400um= 6.80e21 500um=4.35e21
#^^plasma evelengths and their densities
nb1 =1e22
nb2 =10e22


array_size =100
n0_array = np.linspace(n01,n02,num = array_size)

nb_array = np.linspace(nb1,nb2, num = array_size)


PWN_loop=np.zeros([array_size,array_size])
cpul_loop=np.zeros([array_size,array_size])
APF_loop=np.zeros([array_size,array_size])
EFieldMax_GeV_loop =np.zeros([array_size,array_size])
Blowout_Radius_loop =np.zeros([array_size,array_size])
Blowout_Radius_loop_N =np.zeros([array_size,array_size])#Radius normalised to units in W.Lu thesis
###
# Array setup End
##




##################################
    #User Input - Ultra Relativistic
##################################

PlasmaDensity = 1.21e22
Plasma_Wavelength =Wavelength(PlasmaDensity)
Rb0    = 1.e-1 #initial conditions - Starting rb can not be 0
dRb0   = 0.0 #

xi_0   = 0.0 # initial conditions for RHS
xi_max = 5.8#6.699#max intrgration limits
dxi = 0.001 # size of integration steps
steps = np.int(np.abs(xi_max/dxi)) # number of steps needed to achieve dxi

initialvalue = [Rb0,dRb0]
#mu=0.6
sig=0.319 # Drive beam width as sigma
mu=1
nb=4.2#BeamD/PlasmaDensity#BeamD/n0_array#2.3 # Ratio of drive beam to backgroup plasma

r=0.000001
solWLu =[[],[]]
xi=np.linspace(xi_0, xi_max, steps) #(START,END,NUMBER OF STEPS BETWEEN LIMITS)
solWLu = odeint(modelWLu, initialvalue,xi,args=(mu,sig,nb,r)); #(func,initial,t)


####
# Model Checks
####

#Beta Check
rmax = np.max(solWLu[:,0])
DL =  1 # Used in the paper generall a good staring place
DS = 0.1 *rmax #  Sheath size
e_unknown =DS/rmax #approx value taken from the paper , still unsure

alpha = DL/rmax +e_unknown
beta = ((1+alpha)**2) * math.log((1+alpha)**2)/ ((1+alpha)**2 -1) -1
betaRmax = beta* rmax**2
print "Beta Rmax Check = " + str(betaRmax)
print "Beta value = " +str(beta)
print "Blowout radius max = " + str(rmax)
if np.max(solWLu[:,0]) < 2  or beta > 0.8 or betaRmax < 4:
    print "DANGER ! : Model conditions now met "

else:
    print "Regime conditions met"

if np.min(solWLu[:,0]) < 0:
    print "DANGER ! : Blowout radius is returning negative values"

xi_un=xi*(c.c/PlasmaAngularFrequency(n0))*1e6
Efield_UR = 0.5*solWLu[:,0]*solWLu[:,1]
figUR,(ur1,ur2)=plt.subplots(2,1)
plot1ur1=ur1.plot(xi_un,solWLu[:,0]*(c.c/PlasmaAngularFrequency(n0))*1e6, '--')
plot1ur1=ur1.plot(xi_un,-solWLu[:,0]*(c.c/PlasmaAngularFrequency(n0)*1e6), '--')



#PWN = PlasmaWaveNumber(PlasmaDensity)
APF =PlasmaAngularFrequency(PlasmaDensity)
#cpul =ChargePerUnitLength(BeamD,PlasmaDensity,PWN,10e-6)

EfieldNormalisation = c.m_e*c.c*APF/c.elementary_charge
plot2ur2=ur2.plot(xi_un,Efield_UR*EfieldNormalisation)
figUR.tight_layout()
figUR.savefig('ShapeUR+Efield.png',format = 'png', dpi=300,bbox_inches='tight')



########################
# Cylinder Input
########################
ShieldDensity = PlasmaDensity * 3
Cylinder_Length = Plasma_Wavelength * 2
Gaussian_Radius = 20e-6
Cylinder_End_Area =math.pi *Gaussian_Radius**2
Cylinder_Volume =Cylinder_End_Area *Cylinder_Length
Cylinder_E_count  = Cylinder_Volume * ShieldDensity
Cylinder_Charge = Cylinder_E_count * c.elementary_charge
Eval_Range= np.arange(0.1e-6,60e-6,0.5e-8)
SupG=SuperGaussian(Eval_Range,Gaussian_Radius,2)
Gauss= Gaussian(1,Eval_Range,0,Gaussian_Radius,2)

DensitySGProfile = SupG * ShieldDensity
print np.shape(DensitySGProfile)
DensityGProfile = Gauss * ShieldDensity


count=0
Q=Cylinder_Charge
print Q
EFSG=np.zeros([np.size(Eval_Range),1])
EFG=np.zeros([np.size(Eval_Range),1])
sr=np.size(Eval_Range)


while count < sr :
    #print c
    count=int(count)
    EFSG[count]= EcylinderVD(Q,Eval_Range,DensitySGProfile,count)
    EFG[count] = EcylinderVD(Q,Eval_Range,DensityGProfile,count)
    count+=1





########################

#Plotting Cylindric

########################

P2=plt.figure(2)
plt.plot(Eval_Range,EFSG,color ="blue")
plt.plot(Eval_Range,EFG,color ="green")
#plt.plot(r,E)
plt.autoscale(enable=True, axis='x', tight=True)

#EMax=EPeak(3e-9,30e-6,25e-6)

figCyl,(cylx1)=plt.subplots(1, 1)
cylx1.plot(Eval_Range,EFG)
cylx1.plot(Eval_Range,EFSG, '--',color ="green")
figCyl.savefig('Gauusian fields.png',format = 'png', dpi=300,bbox_inches='tight')




##
# Loop -Ultra Relativistic
##
"""
BlowOutRadius_max=np.zeros([array_size])
EzField_max = np.zeros([array_size])
UR_Loopsize = np.size(nb)
BlowOutRadius = np.zeros([array_size,steps])
for np_element in range(array_size):
    print np_element
    solWLu =[[],[]]
    solWLu = odeint(modelWLu, initialvalue,xi,args=(mu,sig,nb[np_element],r));
    #BlowOutRadius[np_element,:]= solWLu[:,0]
    BlowOutRadius_max[np_element]= np.max(solWLu[:,0])
    EzField_max[np_element]= np.max(0.5*solWLu[:,0]*solWLu[:,1])

figUR,(ur1,ur2,ur3)=plt.subplots(1,3)
plot1ur1=ur1.plot(xi,BlowOutRadius[0,:], '--')
plot1ur2=ur2.plot(xi,BlowOutRadius[1,:], '--')
plot1ur3=ur3.plot(xi,BlowOutRadius[2,:], '--')
figUR.savefig('ShapeUR.png',format = 'png', dpi=300,bbox_inches='tight')
"""

##
# Loop start - transition region
##
print array_size
for np_element in range(array_size): #here we loop over  the range of drive beam densities to use
	nb_current = nb_array[np_element]

	for np_element2 in range(array_size):

		PWN_loop[np_element2,np_element] = PlasmaWaveNumber(n0_array[np_element2])
		cpul_loop[np_element2,np_element] =  ChargePerUnitLength(nb_current,n0_array[np_element],PWN_loop[np_element2,np_element],Br)
		APF_loop[np_element2,np_element] =PlasmaAngularFrequency(n0_array[np_element2])
		EFieldMax_GeV_loop[np_element2,np_element]=EstimateEmax(cpul_loop[np_element2,np_element],APF_loop[np_element2,np_element])/1e9
		Blowout_Radius_loop[np_element2,np_element]=RadiusMax(cpul_loop[np_element2,np_element])*(c.c/APF_loop[np_element2,np_element])/1e-6
        Blowout_Radius_loop_N[np_element2,np_element]=RadiusMax(cpul_loop[np_element2,np_element])

##
# Loop end
##

EFM_working = EFieldMax_GeV_loop[:-1,:-1]

levels = MaxNLocator(nbins=100).tick_values(EFM_working.min(),EFM_working.max()) # sorts the tick values for the contour plot
figC,cp1=plt.subplots(1,1)
cf=cp1.contourf(n0_array,nb_array,EFieldMax_GeV_loop,levels=levels) # contour plot
figC.tight_layout()
cbar=figC.colorbar(cf, ax=cp1) # add the colour bar for the colour map
cbar.ax.set_ylabel("Estimated Efield in GV/m") #sets the colour map label
cp1.set_xlabel("Plasma Density")
cp1.set_ylabel("Drive Beam Density")
#figC.savefig(str(now)+'Contour.png',format = 'png', dpi=300,bbox_inches='tight')
figC.show()
####
# Plotting blowout Radius
####

Br_working =Blowout_Radius_loop_N# Blowout_Radius_loop_N[:-1,:-1]
print np.max(Br_working)
print np.min(Br_working)
levels = MaxNLocator(nbins=100).tick_values(Br_working.min(),Br_working.max())
figC2,cpR1=plt.subplots(1,1)
cf=cpR1.contourf(n0_array,nb_array,Blowout_Radius_loop_N,levels=levels)
figC2.tight_layout()
cbar=figC2.colorbar(cf, ax=cpR1)
cbar.ax.set_ylabel("Bean Radius in um ")
cpR1.set_xlabel("Plasma Density")
cpR1.set_ylabel("Drive Beam Density")
#figC2.savefig('ContourBeamRadius.png',format = 'png', dpi=300,bbox_inches='tight')
figC2.show()
##
# Loop plotting start
##

fig1,(ax1,ax2)=plt.subplots(2, 1)
ax1.plot(n0_array,Blowout_Radius_loop_N[50,:])
ax2.plot(nb_array,Blowout_Radius_loop_N[:,50])

ax1.set_xlabel("Plasma density")
ax2.set_xlabel("Drive Beam density")

ax1.set_ylabel("normalised beam radius")
ax2.set_ylabel("normalised beam radius")

fig1.tight_layout()
#fig1.savefig('PlasmaWL_and_ChargePerUnitL.png',format = 'png', dpi=300,bbox_inches='tight')
"""
fig2,(bx1,bx2)=plt.subplots(2,1)
bx1.plot(n0_array,EFieldMax_GeV_loop)
bx2.plot(nb_array,EFieldMax_GeV_loop)

bx1.set_xlabel("Plasma density")
bx2.set_xlabel("Electron Beam density")

bx1.set_ylabel("E Max Approx Gv/m")
bx2.set_ylabel("E Max Approx Gv/m")


fig2.tight_layout()
fig2.savefig('Efield_Max.png',format = 'png', dpi=300,bbox_inches='tight')


fig3,(cx1)=plt.subplots(1,1)
cx1.plot(n0_array,Blowout_Radius_loop)

cx1.set_xlabel("Plasma density")
cx1.set_ylabel("Blowout Radius")

fig3.tight_layout()
fig3.savefig('blowout radius.png',format = 'png', dpi=300,bbox_inches='tight')

##
# Loop plotting end
##



normalisedBr=Br*(c.c/PlasmaAngularFrequency(n0)) #normalised beam radius


WL = Wavelength(n0)
PWN = PlasmaWaveNumber(n0)

cpul =  ChargePerUnitLength(nb,n0,PWN,Br)
cpul2 = ChargePerUnitLength2(nb,n0,PWN,Br)

DistanceNormalisation=(c.c/PlasmaAngularFrequency(n0))
rmn = RadiusMax(cpul)
rm= rmn *(c.c/PlasmaAngularFrequency(n0))
APF =PlasmaAngularFrequency(n0)


#print "beam to plasma density ratio " + str((nb/n0))
#print "plasma angular frequency " +str(PlasmaAngularFrequency(n0)) +" rad/s"

#print "Estimates peak field is " + str(EstimateEmax(cpul,APF)/1e9) +" GV/m"
#print "Normalised Distance " +  str((c.c/PlasmaAngularFrequency(n0))) +" m"
#print "Plasma frequency is " + str(PlasmaFrequency(n0) ) + " hertz"
#print "Normalised blowout radius " +str(rmn)
#print "Estimated blowout radius " +str(rm*1e6)+" um"
"""
n0=1e21
PWN = PlasmaWaveNumber(n0)

cpul =  ChargePerUnitLength(nb,n0,PWN,Br)
cpul2 = ChargePerUnitLength2(nb,n0,PWN,Br)
print "Estimates peak field is " + str(EstimateEmax(cpul2,APF)/1e9) +" GV/m"