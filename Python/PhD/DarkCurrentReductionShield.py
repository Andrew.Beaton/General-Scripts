# -*- coding: utf-8 -*-
"""
Created on Tue Dec 04 14:26:21 2018

@author: Andrew
"""

import numpy as np
import scipy.constants as spc 
import matplotlib.pyplot as plt
import scipy.integrate as integrate

Q= 1.6e-19 #charge
r= np.arange(0.1e-6,60e-6,0.5e-8) #max Radius
N0=8e21#1e21
e0= spc.constants.epsilon_0 # electric permitivity


#Static transverse density cylinder
E_NoVar=((Q*N0)/(2*e0))*(r*r)**0.5
print np.max(E_NoVar)






#Basic Gaussian function

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))
 
  return Gauss

########################


#Super Gaussian beam 
def SuperGaussian(r,rl,n):#delta0,r,N):
    #SGauss= (1/((2*spc.constants.pi)**0.5)*delta0)*np.exp((-(r**N))/(2*delta0**N))
    SGauss =np.exp(-(r/rl)**(2*n))
    return SGauss


########################

SupG=SuperGaussian(r,10e-6,2)
#SupG = Gaussian(1,r,0,15e-6,2.17)
P1=plt.figure(1)
plt.plot(r,SupG,color ="blue")
DensitySGProfile= SupG*N0


Gauss= Gaussian(1,r,0e-6,10e-6,2)

DensityGProfile= Gauss*N0

plt.plot(r,Gauss,color ="green")
plt.autoscale(enable=True, axis='x', tight=True)




########################

#Varying Density 

########################
def EcylinderVD (q,r,Nr,c):#Charge, range of r values , density array, position in the array

    #Chapter 5:Introduction to beam-generated Forces --Charged particle beams Stanley Humphries
   
    # Rmax=r[c]
    Rmin=r[0]
    E= (q/(2* spc.constants.pi*e0*r[c]))
    Integrate = integrate.quad(lambda r: 2*spc.constants.pi *r,Rmin,r[c])
    
    #print Integrate
    EF= E*Integrate[0]*Nr[c]
    return EF



def EPeak(Q,sigz,sigr):
    #EPeak = 2*Q/(((2*spc.constants.pi)**(3/2)) *( spc.constants.epsilon_0 * sigz* sigr))
    EPeak = (Q/ (((2*spc.constants.pi)**(3/2)) *( 2*spc.constants.epsilon_0 * sigz * sigr))) 
    return EPeak


def ECldNRwaveBreak (N0):
    # Approx value for the base E fields of the blowout
    #More accuratly , the cold nonrelativistic wave breaking field 
    E=96*(N0*1e-6)**0.5
    return E


def PWaveL (N0):
    #Approx calculation of the plasma wavelength
    L=3.3e10/((N0*1e-6)**0.5)
    return L 


########################

#Radius loop 

########################

c=0

EFSG=np.zeros([np.size(r),1])
EFG=np.zeros([np.size(r),1])
sr=np.size(r)


while c < sr :
    #print c
    c=int(c)
    EFSG[c]= EcylinderVD(Q,r,DensitySGProfile,c)
    EFG[c] = EcylinderVD(Q,r,DensityGProfile,c)
    c+=1
    
    


########################

#Plotting 

########################

fig1= plt.figure()
ax1=  fig1.add_subplot(111)
ax1.plot(r,EFSG,color ="blue")
ax1.plot(r,EFG,color ="green")
ax1.set_xlim(0,40e-6)
#plt.plot(r,E)
#plt.autoscale(enable=True, axis='x', tight=True)
ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
EMax=EPeak(1e-9,5e-6,4e-6)





print "########--Results--########"

print "Cold wave breaking field " + str((ECldNRwaveBreak((N0))/1e9)) +  "  GV/m"
print np.sum(Gauss)
print str(EMax/1e9)+  "  GV/m"
print "Plasma wavelength in um " + str(PWaveL(N0))
