# -*- coding: utf-8 -*-
"""
Created on Mon Apr 08 14:52:28 2019

@author: Andrew
"""
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import scipy.constants as c 
class DataLoader: 
    # This guy loads data
    def __init__(self,MainFileName,FieldName,dump,dim):
        self.MainFileName=MainFileName
        self.FieldName=FieldName
        self.dump=dump
        self.xLim                = []
        self.xLimCheck =0
        self.dim =dim
    def loadField(self):
        DataFile = "%s_%s_%i.h5" %(self.MainFileName,self.FieldName,self.dump)
        print DataFile
        print " Loading %s_%i" %(self.FieldName,self.dump)
        try :
                f =h5py.File(DataFile)
                
                mesh = f[self.FieldName].attrs["vsMesh"]
                limits = f[self.FieldName].attrs["vsLimits"]
                self.dataField = np.array(f[self.FieldName],dtype=np.float64)
                
        except:
                print " something went wrong reading the data "
    
    
        self.dataField[:,:,:,0] = self.dataField[:,:,:,0]/1e9
        self.dataField[:,:,:,1] = self.dataField[:,:,:,2]/1e9
        self.dataField[:,:,:,2] = self.dataField[:,:,:,1]/1e9
        
        self.time=f["time"].attrs["vsTime"]
    
        self.WindowStart = f[mesh].attrs["vsLowerBounds"][0]*1e6
        self.WindowEnd = f[mesh].attrs["vsUpperBounds"][0]*1e6
        self.numCellX  = f[mesh].attrs["vsNumCells"][0] + 1
        self.numCellY  = f[mesh].attrs["vsNumCells"][1] + 1
        self.numCellZ  = f[mesh].attrs["vsNumCells"][2] + 1
        self.WindowUpperY = f[limits].attrs["vsUpperBounds"][1]
        self.WindowLowerY = f[limits].attrs["vsLowerBounds"][1]
        self.WindowUpperZ = f[limits].attrs["vsUpperBounds"][2]
        self.WindowLowerZ = f[limits].attrs["vsLowerBounds"][2]
        self.halfway = self.numCellY /2
        self.x_range = np.linspace(self.WindowStart,self.WindowEnd,num=self.numCellX)
        self.y_range = np.linspace(self.WindowLowerY,self.WindowUpperY,num=self.numCellY)*1e6
        self.z_range = np.linspace(self.WindowLowerZ,self.WindowUpperZ,num=self.numCellZ)*1e6
        self.cellSizeX = (f[limits].attrs["vsUpperBounds"][0]-f[limits].attrs["vsLowerBounds"][0])/self.numCellX*1e6
        self.x_range = self.x_range -(self.time*c.c*1e6)
        
        f.close
        print "Loading of %s complete" %(FieldName)


    def  lineoutAll(self,p1,p2,p3,dim):
        fig1 = plt.figure()
        ax1=  fig1.add_subplot(111)
        ax1.plot(self.dataField[:,p2,p3,dim])
        fig2 = plt.figure()
        ax2=  fig2.add_subplot(111)
        ax2.plot(self.dataField[p1,:,p3,dim])
        fig3 = plt.figure()
        ax3=  fig3.add_subplot(111)
        ax3.plot(self.dataField[p1,p2,:,dim])
        
    def xlimits(self,lowerx,upperx):
         self.xLim=[lowerx,upperx]
         self.xLimCheck =1
         
         
    def surfaceplt(self):
       
        
        self.fig2=plt.figure()
        self.ax2 =self.fig2.add_subplot(111)
        X,Y=np.meshgrid(self.y_range,self.x_range)
        
        #self.numCellZ,self.numCellX,self.numCellY
        FieldSlice = np.sqrt(self.dataField[:,:,self.halfway,1]**2 + self.dataField[:,:,self.halfway,2]**2)
        surface=self.ax2.pcolor(Y,X,FieldSlice,cmap=cm.coolwarm,)
        cbaxes = self.fig2.add_axes([1., 0.1, 0.03, 0.8]) 
        cbar = plt.colorbar(surface,cmap=cm.coolwarm,cax=cbaxes)
        cbar.set_label("Electric field")
        if self.xLimCheck ==1:
            self.ax2.set_xlim(int(self.xLim[0]),int(self.xLim[1]))
        plt.tight_layout()      
        
        
    def lineoutAddX(self):
            
        self.ax3 = self.ax2.twinx()
        self.ax3.set_ylabel('Efield (Gv/m)', color="r")
        self.ax3.tick_params(axis='y', labelcolor="r")
        self.ax3.plot(self.x_range,self.dataField[:,self.halfway,self.halfway,self.dim],"r--")
        plt.tight_layout()
        
        
    def lineoutAddY(self,posCM):
        gridPos =int(posCM / self.cellSizeX)
        print gridPos
        
        self.ax4 = self.ax2.twiny()
        self.ax4.set_xlabel('Efield (Gv/m)', color="y")
        self.ax4.tick_params(axis='x', labelcolor="y")
        self.ax4.plot(self.dataField[gridPos,:,self.halfway,self.dim],self.y_range,"y--")
        plt.tight_layout()
        
        
    def lineoutAddX_Sum(self):
            
        self.ax3 = self.ax2.twinx()
        self.ax3.set_ylabel('Efield (Gv/m)', color="r")
        self.ax3.tick_params(axis='y', labelcolor="r")
        
        FieldSum= self.dataField[:,self.halfway,self.halfway,0] + self.dataField[:,self.halfway,self.halfway,1] + self.dataField[:,self.halfway,self.halfway,2]
        self.ax3.plot(self.x_range,FieldSum,"r--")
        plt.tight_layout()
                    
        
    def lineoutAddY_Sum(self,posCM):
            
        self.ax3 = self.ax2.twiny()
        self.ax3.set_xlabel('Efield (Gv/m)', color="y")
        self.ax3.tick_params(axis='x', labelcolor="y")
        gridPos =int(posCM / self.cellSizeX)
        FieldSum= np.sqrt(self.dataField[gridPos,:,self.halfway,0]**2 + self.dataField[gridPos,:,self.halfway,1]**2 + self.dataField[gridPos,:,self.halfway,2]**2)
        self.ax3.plot(FieldSum,self.y_range,"y--")
        plt.tight_layout()
        
    def SaveFigure(self):
        self.fig2.savefig('FieldXY.png',format = 'png', dpi=500,bbox_inches='tight')
        
        
RunName = "20190325_DC_Shield_Matched_V4_MC"
#RunName ="20180912_EmittanceDampeingIonSlabV8_3"
FieldName = "EFieldIon"

dump=2
Efield =DataLoader(RunName,FieldName,dump,1)
Efield.loadField()
#Efield.printshape
print np.shape(Efield.dataField)
#Efield.lineoutAll(150,150,150,0)
#Efield.lineoutAll(150,150,150,1)
#Efield.lineoutAll(150,150,150,2)

#Efield.xlimits(100,200)
Efield.surfaceplt()
Efield.lineoutAddX()
#Efield.lineoutAddY(155)
#Efield.lineoutAddX_Sum()
Efield.lineoutAddY_Sum(155)
Efield.SaveFigure()

"""
fig1 = plt.figure()
ax1=  fig1.add_subplot(111)
ax1.plot(Efield.dataField[:,150,150,0])

fig2 = plt.figure()
ax2=  fig2.add_subplot(111)
ax2.plot(Efield.dataField[:,150,150,1])

fig3 = plt.figure()
ax3=  fig3.add_subplot(111)
ax3.plot(Efield.dataField[:,150,150,2])
"""