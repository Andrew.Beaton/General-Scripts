# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 18:04:55 2019

@author: Andrew
"""

import scipy.constants as c 

DE=10e9
DL=10e-6
gamma = DE/ 0.511e6
ConL = DL/gamma
ETime = ConL/c.constants.c
print ETime