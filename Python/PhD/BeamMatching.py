# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 17:46:04 2019

@author: Andrew
"""

import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import scipy.constants as c 
import scipy.integrate as integrate


# Calculated width and length is rms 
# Multiply by three for aprox max size 
#
def EPeak(Q,sigz,sigr):
#    EPeak= Q/( (2*c.constants.pi)**(3/2))/sigr/sigz/2*c.constants.epsilon_0
    EPeak = (Q/ (((2*c.constants.pi)**(3/2)) *( 2*c.constants.epsilon_0 * sigz * sigr))) 
    return EPeak


def EPeak_Radial(Q,sigz,sigr):
        EPeak = (Q/(c.epsilon_0*sigz*sigr)*0.025)
        return EPeak
    
 #Basic Gaussian function

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))
 
  return Gauss

########################


#Super Gaussian beam 
def SuperGaussian(r,rl,n):#delta0,r,N):
    #SGauss= (1/((2*spc.constants.pi)**0.5)*delta0)*np.exp((-(r**N))/(2*delta0**N))
    SGauss =np.exp(-(r/rl)**(2*n))
    return SGauss


########################
    
    
########################

#Varying Density 

########################
def EcylinderVD (q,r,Nr,count):#Charge, range of r values , density array, position in the array

    #Chapter 5:Introduction to beam-generated Forces --Charged particle beams Stanley Humphries
   
    # Rmax=r[c]
    Rmin=r[0]
    E= (q/(2* c.constants.pi*c.constants.epsilon_0 *r[count]))
    Integrate = integrate.quad(lambda r: 2*c.constants.pi *r,Rmin,r[count])
    
    #print Integrate
    EF= E*Integrate[0]*Nr[count]
    return EF

size= 200
Plasma_Wl= np.linspace(90e-6,600e-6,num=size)
Beam_Energy = np.linspace(1e9,9e9,num=size)
 
ComboMatching=1
Shield_Den=8e22

Emit =np.linspace(10e-6,90e-6, num = size)
if ComboMatching ==1:
     Plasma_Den =(3.34e7/Plasma_Wl) **2
     Plasma_Den =Plasma_Den + Shield_Den
     Plasma_Wl = 3.34e7/(Plasma_Den)**0.5

count =0
count2 =0
count3 =0
Plasma_Den=np.zeros([size])
Plasma_KP=np.zeros([size])
Length_Match=np.zeros([size])
Length_Match_um=np.zeros([size])
Gamma=np.zeros([size])
Radius_Match=np.zeros([size,size,size])
Radius_Match_um=np.zeros([size,size,size])

while count < size:
   # print "good news everyone " +str(count)
    Plasma_Den[count] =(3.34e7/Plasma_Wl[count]) **2
 
   
       
    Plasma_KP[count] = 2*math.pi/Plasma_Wl[count]
    Length_Match[count] =( 1/Plasma_KP[count])
    
    while count2 <size:
        #print count2

        Gamma[count2] = Beam_Energy[count2]/0.510998e6

        
        
        while count3<size:
            #print count3
            Radius_Match[count,count2,count3] =(Emit[count3]*((2)**0.5)/(Plasma_KP[count])/(Gamma[count2]**0.5))**0.5
            
            count3 +=1
        count3=0
        count2 +=1
    count2=0
    count +=1

print "beam length matched for initial conditons " +str(Length_Match[0])
#print Plasma_Den[0]
print "beam radius matched for initial conditons "+ str(Radius_Match[0,0,0])#

print " Beam max matched radius = %s"%(np.max(Radius_Match)) 
print " Beam max matched length = %s"%(np.max(Length_Match))
Length_Match_um=Length_Match/1e-6 *3 # the factor of three is to move from rms to max radius 
Radius_Match_um=Radius_Match/1e-6 *3   # the factor of three is to move from rms to max radius

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
X,Y=np.meshgrid(Gamma,Emit)
surface=ax.plot_surface(X,Y,Radius_Match_um[0,:,:],cmap=cm.coolwarm,)
ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
ax.set_xlabel("Gamma")
ax.set_ylabel("Emittance mrad")
ax.set_zlabel("Matched Radius (um)")
ax.view_init(30, 195)
plt.show




fig2 = plt.figure()
ax2= fig2.add_subplot(111)
plt.plot(Emit,Radius_Match_um[0,0,:])
ax2.set_xlabel("Emittance")
ax2.set_ylabel("Radius in um")
plt.show
fig3 = plt.figure()
ax3= fig3.add_subplot(111)
ax3.set_xlabel("Gamma")
ax3.set_ylabel("Matched Length")
ax3.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.plot(Gamma,Length_Match)

fig4 = plt.figure()
ax4=fig4.add_subplot(111)
plt.plot(Plasma_Wl,Radius_Match_um[:,0,0])

plt.show



fig5 = plt.figure()
ax5 = fig5.add_subplot(111, projection='3d')
X,Y=np.meshgrid(Gamma,Emit)
surface=ax5.plot_surface(X,Y,Radius_Match_um[99,:,:],cmap=cm.coolwarm,)
ax5.set_xlabel("Gamma")
ax5.set_ylabel("Emittance mrad")
ax5.set_zlabel("Matched Radius (um)")
ax5.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax5.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
ax5.view_init(30, 195)
plt.show


#~~~~~~~~~~~~~~~~~~
# Cylinder / shield
#~~~~~~~~~~~~~~~~~

########################

#Radius loop 

########################
Q= 1.6e-19 #charge
r= np.arange(0.1e-6,60e-6,0.5e-8) #max Radius
N0=Shield_Den#1e21

SupG=SuperGaussian(r,10e-6,2)
DensitySGProfile= SupG*N0
Gauss= Gaussian(1,r,0e-6,2.6e-6,2)
DensityGProfile= Gauss*1.13e25


EFSG=np.zeros([np.size(r),1])
EFG=np.zeros([np.size(r),1])
sr=np.size(r)
count=0

while count < sr :
    #print c
    count=int(count)
    EFSG[count]= EcylinderVD(Q,r,DensitySGProfile,count)
    EFG[count] = EcylinderVD(Q,r,DensityGProfile,count)
    count+=1
    
    

print" From Cylinder approx max field  =" + str(np.max(EFG)/1e9)+" Gv/m"
# Get the indices of maximum element in numpy array
Emax_G_pos = np.where(EFG == np.amax(EFG))

print "From Cylidnder approx max field after shield addition = " +str((np.max(EFG)-EFSG[Emax_G_pos[0]])/1e9)
print "Shield Efield = " +str(np.max(EFSG)/1e9)+" Gv/m"
########################

#Plotting - cylinder 

########################
fig0= plt.figure()
ax0= fig0.add_subplot(111)

ax0.plot(r,SupG,color ="blue")
ax0.plot(r,Gauss,color ="green")
ax0.autoscale(enable=True, axis='x', tight=True)

fig1= plt.figure()
ax1=  fig1.add_subplot(111)
ax1.plot(r,EFSG,color ="blue")
ax1.plot(r,EFG,color ="green")
ax1.set_xlabel("Radius")
ax1.set_ylabel("Efield in v/m")
ax1.set_xlim(0,40e-6)
#plt.plot(r,E)
#plt.autoscale(enable=True, axis='x', tight=True)
ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
#EMax=EPeak(3e-9,30e-6,25e-6)


    
    
Emax_GVm=Emax/1e9
Emax_combo = Emax +np.max(EFSG)
print "Max beam Efield is approx" +str(Emax_GVm[0,0,0])  +" Gv/m"
print "From EPeak approx  fields after shield  = " +str(Emax_GVm[0,0,0] - (np.max(EFSG)/1e9))
fig6 = plt.figure()
ax6=  fig6.add_subplot(111, projection='3d')
X,Y=np.meshgrid(Gamma,Emit)
surface=ax6.plot_surface(X,Y,Emax_GVm[0,:,:],cmap=cm.coolwarm,)
ax6.set_xlabel("Gamma")
ax6.set_ylabel("Emittance mrad")
ax6.set_zlabel("Efield Gv/m")
ax6.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax6.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
ax6.view_init(30, 300)
plt.show
#fig6.savefig('Efield Radial max 92um EQ.png',format = 'png', dpi=300,bbox_inches='tight')



##########################
#   ADK  # Caolionn O'Connel Thesis "Plasma Production via Field Ioniszation" 
##########################
Z_num = 1
#Emod = 10e9
#First ionisation potential
E_adk =Emax_combo
He_ground   =   24.5874
nadk        =   3.69*Z_num / He_ground**0.5
adk1        =   1.52e15 * 4**nadk * He_ground / ( nadk * math.gamma(2*nadk) )
adk2        =   ( 20.5 * ( ( He_ground )**1.5 / E_adk ) )**( 2 * nadk - 1 )
adk3        =   np.exp( -6.83 * ( ( He_ground**1.5 ) / E_adk ) )
adkf        =   adk1 * adk2 * adk3


Ionisation =  adkf *3e-18 #*2.6e25  4.66e-14

fig7 = plt.figure()
ax7=  fig7.add_subplot(111, projection='3d')
X,Y=np.meshgrid(Gamma,Emit)
surface=ax7.plot_surface(X,Y,Ionisation[0,:,:],cmap=cm.coolwarm,)
ax7.set_xlabel("Gamma")
ax7.set_ylabel("Emittance mrad")
ax7.set_zlabel("Ionisation Probability")
ax7.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax7.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
ax7.ticklabel_format(style='sci', axis='z', scilimits=(0,0))
ax7.view_init(30, 150)
fig7.tight_layout()
plt.show


#~~~~~~~~~~~~~~~~~~~~~~~~~
# Parameter Space checking
#~~~~~~~~~~~~~~~~~~~~~~~~~

adk_Rate = adkf[0,0,0]
print "Initial beam adk rate =" + str(adk_Rate)
print np.min(adkf)
adkf_min_pos = np.where(adkf == np.min(adkf))
adkf_min_pos_multi = np.where(adkf < 1.5575e13)
print np.shape(adkf_min_pos)
#print adkf[adkf_min_pos[2]]
print adkf[adkf_min_pos[0],adkf_min_pos[1],adkf_min_pos[2]]
