# -*- coding: utf-8 -*-
"""
Electron beam adk
Created on Thu Mar 21 13:14:06 2019

@author: Andrew
"""
import math 
import numpy as np 
import scipy.constants as c
from scipy.integrate import quad
import matplotlib.pyplot as plt

Atomic_Energy_Scale = 13.6#24 # enter in electron volts
Atomic_Energy_Scale_N = Atomic_Energy_Scale/27.25
E_crit_N = ((2**0.5) +1)*(Atomic_Energy_Scale_N**1.5)
E_crit = E_crit_N *5.14e11 /1e9
print "Barrier Supresion limit is " +str(E_crit) + "only valid for hrdrogen like atoms"

Beam_Q =1e-9 # drive beam charge
Beam_sigr= 9e-6
Beam_sigz= 15e-6

def EPeak(Q,sigz,sigr):
    #EPeak = 2*Q/(((2*spc.constants.pi)**(3/2)) *( spc.constants.epsilon_0 * sigz* sigr))
    EPeak = (Q/ (((2*c.constants.pi)**(3/2)) *( 2*c.constants.epsilon_0 * sigz * sigr))) 
    return EPeak


print "Peak Beam Field = " +str(EPeak(Beam_Q,Beam_sigr,Beam_sigz)/1e9 )
#############
# Cylinder approx
#############
#Basic Gaussian function

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))
 
  return Gauss

########################

########################

#Varying Density 

########################
def EcylinderVD (q,r,Nr,count):#Charge, range of r values , density array, position in the array

    #Chapter 5:Introduction to beam-generated Forces --Charged particle beams Stanley Humphries

    # Rmax=r[c]
    Rmin=r[0]

    E= (q/(2* math.pi* c.epsilon_0 * r[count]))
    Integrate = quad(lambda r: 2*math.pi *r,Rmin,r[count])

    #print Integrate
    EF= E*Integrate[0]*Nr[count]
    return EF
########################


########################

#Cylinder User Input
    
########################N0=1.2385e22
Eval_Range= np.arange(0.1e-6,60e-6,0.5e-8)
N0=1.2385e18
Gauss= Gaussian(1,Eval_Range,0,Beam_sigr,2)
DensityGProfile = Gauss * N0
EFG=np.zeros([np.size(Eval_Range),1])
sr=np.size(Eval_Range)

count = 0
while count < sr :
    #print c
    count=int(count)
    EFG[count] = EcylinderVD(Beam_Q,Eval_Range,DensityGProfile,count)/1e9
    count+=1


##########################
#   ADK
##########################
Z_num = 1
#Emod = 10e9
tau = 50e-15

#First ionisation potential
He_ground   =   24.5874
nadk        =   3.69*Z_num / He_ground**0.5
adk1        =   1.52e15 * 4**nadk * He_ground / ( nadk * math.gamma(2*nadk) )
adk2        =   ( 20.5 * ( ( He_ground )**1.5 / EFG ) )**( 2 * nadk - 1 )
adk3        =   np.exp( -6.83 * ( ( He_ground**1.5 ) / EFG ) )
adkf        =   adk1 * adk2 * adk3
plt.plot(EFG)
#plt.plot(adkf)