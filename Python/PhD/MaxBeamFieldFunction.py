
import scipy.constants as c 
def EPeak_Radial(Q,sigz,sigr):
        EPeak = (Q/(c.epsilon_0*sigz*sigr)*0.025)
        return EPeak
    


def Emax(Charge,Matched_Length,Matched_Radius):
#~~~~~~~~~~~~~~~~~~
# E field peak 
#~~~~~~~~~~~~~~~~~~
	Emax=EPeak_Radial(Charge,Matched_Length,Matched_Radius)
	return Emax
	