#point_charge.py-Iterativesolutionof2-DPDE,electrostatics
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as c
#Set dimensions of the problem
L = 1.0
N = 100
ds = L/N
#Define arrays used for plotting
x = np.linspace(-250e-6,250e-6,N)
y = np.copy(x)
X, Y = np.meshgrid(x,y)

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))

  return Gauss
Gaussian_Radius=10e-6
Gaussian_Length=45e-6

GaussT= Gaussian(1,x,0,Gaussian_Radius,2)
GaussL=Gaussian(1,x,0,Gaussian_Length,2)
#matplotlib.pyplot.plot(x,GaussT)
#matplotlib.pyplot.plot(x,GaussL)
#plt.show
count=0
count2=0
Dist=np.zeros([N,N])
while count < N:
    count2=0
    while count2 <N:
        Dist[count,count2] = (GaussT[count] * GaussL[count2]) **0.5
        
        count2 +=1
    
    count+=1
print np.max(Dist)

#CS = plt.contour(X,Y,Dist,100)#Makeacontourplot

#Makethechargedensitymatrix
rho0 = 1e18#21
rho = np.zeros((N,N))
rho= rho0 * Dist

E_Div = rho/c.epsilon_0
#CS = plt.contourf(X,Y,E_Div,30)#Makeacontourplot
#CB = plt.colorbar(CS, shrink=0.8, extend='both')
#plt.show

#Maketheinitialguessforsolutionmatrix
V = np.zeros((N,N))
#Solver
iterations = 0
eps = 1e-8
#Convergencethreshold
error = 1e4
#Largedummyerror
while iterations < 3e6 and error > eps:
    V_temp = np.copy(V)
    error = 0
#wemakethisaccumulateintheloop
    for j in range(2,N-1):
        for i in range(2,N-1):
             V[i,j] = 0.25*(V[i+1,j] + V[i-1,j] +V[i,j-1] + V[i,j+1] + rho[i,j]*ds**2)
             error += abs((V[i,j]-V_temp[i,j]))#*(V[i,j]-V_temp[i,j]))
        iterations += 1
    error /= float(N)
    print error
    print"iterations =",iterations




#Plotting
matplotlib.rcParams['xtick.direction'] = 'out'
matplotlib.rcParams['ytick.direction'] = 'out'
v_gvm=V/1e9
CS = plt.contourf(X,Y,v_gvm,30)#Makeacontourplot
#plt.clabel(CS, inline=1, fontsize=10)
plt.title('PDE solution of a point charge')
CB = plt.colorbar(CS, shrink=0.8, extend='both')
plt.show()
