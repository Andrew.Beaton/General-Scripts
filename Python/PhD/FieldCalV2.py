
import numpy as np 
import math
import scipy.constants as c 
def BeamFieldV2(Charge,Matched_Length,Matched_Radius,master_Res,div,range)
B_z=Matched_Length
B_r=Matched_Radius
r =range
COF = #Co-moving frame 
q=Charge

 for r2 in range (0,master_Res):
	#s4=range of field calculation
	E_b = q/ ((2*math.pi)**(3/2)*B_z*c.constants.epsilon_0*r[r2]) *(1- np.exp((r**2)/2*B_r**2) *np.exp(-((COF)**2)/2*B_z*2)