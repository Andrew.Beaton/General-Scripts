# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 10:08:52 2019

@author: Andrew
"""
import math
import numpy as np
import matplotlib.pyplot as plt
#xArg= 10e-3
yArg=100e-6
zArg = 50e-6
xR0=1e-3
w0=20e-6
rmsT=56e-15 #pulse duration
xRMS0=rmsT**2
tW=3e-12 # arrival time of the laser
LIGHTSPEED =2.997e8
def waistsq(xArg,w0,xR0):
    wsq=w0**2 * (1.+(xArg/xR0)**2)
    return wsq

def waistevol(xArg,xR0):
        we=1/math.sqrt(1+(xArg/xR0)**2)
        return we
    


def longenvelope(xArg,tArg,tW,xRMS0):
    pc=(tArg-tW) * LIGHTSPEED # laser pusle center ( drifting at c ) tw is time at which the center is reached
    r=np.sqrt(np.exp(-0.5*((xArg-pc)/xRMS0)**2))
    return r
     
     
     
     
     
xArg= np.arange(-30e-3,30e-3,10e-5)
tArg = xArg * LIGHTSPEED
Tshape = np.zeros((np.shape(xArg)))
WEvolve=np.zeros((np.shape(xArg)))
#longE=np.zeros((np.shape(xArg)))
print np.shape(Tshape)
count=0

for x in np.nditer(xArg):
    waistE=waistevol(x,xR0)
    env= math.exp(-(yArg**2+zArg**2)/waistsq(x,w0,xR0))
    longE= longenvelope(xArg,tArg,tW,xRMS0)
    Tshape[count] = env
    WEvolve[count] =waistE
    count+=1

f1=plt.figure()
f2=plt.figure()
ax1=f1.add_subplot(111)
ax1.plot(xArg,Tshape)

ax1.plot(xArg,WEvolve)

ax2=f2.add_subplot(111)
ax2.plot(xArg,longE)