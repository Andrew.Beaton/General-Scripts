# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 17:06:52 2019

@author: Andrew
"""
import numpy as np 
import math
import matplotlib.pyplot as plt    
import scipy.constants as c 
import scipy.integrate as integrate
import os 
import pandas as pd 
import MaxBeamFieldFunction 

def Gaussian (a,rg,b,c,n): # Standard Gaussian function . n=2 for regular  super gaussian N>2

  Gauss = a*np.exp(((-((rg-b)**n))/(2*c**2)))
 
  return Gauss





master_Div =10 # this divides each parameter into n sections
master_Res = 100 # This divides the transverse distance into n sections
BackgroundGasDensity = 5e19 # This ist the density of the background He gas 
#Super Gaussian beam 
def SuperGaussian(r,rl,n):#delta0,r,N):
    #SGauss= (1/((2*spc.constants.pi)**0.5)*delta0)*np.exp((-(r**N))/(2*delta0**N))
    SGauss =np.exp(-(r/rl)**(2*n))
    return SGauss

def ComboDens(Wavelengths,AdditionalDensity): # intakes the standard plasma wavelengths selected and adds the additional density from the shield
     Plasma_Den =(3.34e7/Wavelengths) **2 # approx relationship from pauls pre file 
     Plasma_Den =Plasma_Den + AdditionalDensity
     Plasma_Wl = 3.34e7/(Plasma_Den)**0.5
     return Plasma_Wl




  
class DBeam: # class containing all important information on the selected drive beam 
    # This guy loads data
    def __init__(self,div):
        self.div =div
        self.Charge         =np.zeros([1])
        self.Energy         =np.zeros([div,1])
        self.Emittance      =np.zeros([div,1])
        self.Radius_Matched =np.zeros([div,div,div])
        self.Length_Matched =np.zeros([div,1])
        
        
    def Assign_Charge(self,high): # sets charge of the drive beam 
        self.Charge =high
        
    def Assign_Energy(self,low,high): # assigns an energy range 
        self.Energy =np.linspace(low,high,num=self.div)
        self.EnergyG=self.Energy/0.510998e6 +1 # converts energy into gamma factor for use in radius matching 
        
        
    def Assign_Emittance(self,low,high):# sets emittance  of the drive beam 
        self.Emittance =np.linspace(low,high,num=self.div)
    
    def Matched_Length(self,Plasma_kp): # finds and sets the variable for the matched length of a drive beam 
        
        self.Length_Matched =( 1/Plasma_kp)


    def Matched_Radius(self,Plasma_kp): # finds and sets tehe variable for the mathced radius of a drive beam
        self.Radius_Matched = np.zeros([self.div,self.div,self.div],dtype=np.float64)    
        for s1 in range(0,self.div):
            #s1=emittance counter
            for s2 in range(0,self.div):
                #s2=Energy counter
                for s3 in range(0,self.div):
                    #s3=Plasma kp counter
                    
                    self.Radius_Matched[s1,s2,s3] =(self.Emittance[s1]*((2)**0.5)/(Plasma_kp[s3])/(self.EnergyG[s2]**0.5))**0.5 #taken from Pauls light source pre file
        
    def Calculate_Density (self,Length,Radius):#calculates the drive beam density for all sets ups of drive beam length and radius
        #From Paul pre file light source
        N_Total = self.Charge / c.elementary_charge # total number of electrons 
        self.Beam_Density_Gaussian = np.zeros([self.div,self.div,self.div,self.div])
        
        
        for s1 in range(0,self.div):
            #s1=emittance counter
             percentage = (((float(s1))/master_Div)*100) # completion percentage 
             print "Density calculations for Gaussian "+ str(percentage) + "% complete"
             
             
             for s2 in range(0,self.div):
                #s2=Energy counter
                
                for s3 in range(0,self.div):
                            #s3=Plasma kp counter 
                            # Here I just split things up like paul in the light source pre file 
                            Long_Norm = 1/ (math.sqrt(2*math.pi)*Length[s3])
                            Norm_R = 1/(math.sqrt(2*math.pi)*Radius[s1,s2,s3])
                            self.Beam_Density_Gaussian [s3,s1,s2,s3] = N_Total *Long_Norm*Norm_R*Norm_R
                            savelocation ="G:\May 2019\Shield\Beam Matching V2\Density Check"
                            filename = "Beam_Density"+"_"+str(s1)+"_"+str(s2)+"_"+str(s3)
                            np.save(os.path.join(savelocation,filename),self.Beam_Density_Gaussian [s3,s1,s2,s3])



class Shield: # sets up the shield class
    
    def __init__(self,div):
        
        self.div=div
        self.Density = np.zeros([div,1])
        self.Radius  = np.zeros([div,1])
        
        
    def Assign_Density(self,low,high): # sets up the density of the shield
        self.Density=np.linspace(low,high,num=self.div)
        self.Density_range = [low,high]
    
    def Assign_Radius(self,low,high): # Shield Radius 
        self.Radius = np.linspace(low,high,num=self.div)



class Plasma: # The plasma class is used for the background plasma used to create the wake 
    
    def __init__(self,div):
        
        self.div = div
        self.Density = np.zeros([div,1])
        self.KP = np.zeros([div,1])
        self.Wavelength =np.zeros([div,1])
        
     
    def Assign_Wavelength(self,low,high): #Takes in the initial wavelength requested by the user
        #self.Wavelength =np.linspace(low,high,num=self.div)
        self.Density_low= (3.34e7/low)**2
        self.Density_high =(3.34e7/high)**2
        
        #self.KP =  2*math.pi/self.Wavelength
        
        
    def Get_Combo_Wavelength(self,AdditionalDensity): # Works out the combined wavelength after applying the shield denisty
        #self.Plasma_Den_original =(3.34e7/self.Wavelength) **2
        self.Plasma_Den_Combo_high = self.Density_low + np.max(AdditionalDensity)
        self.Plasma_Den_Combo_low = self.Density_high + np.min(AdditionalDensity)
        self.Wavelength_Combo_high = 3.34e7/(self.Plasma_Den_Combo_high)**0.5
        print str(self.Wavelength_Combo_high) + " Is the high wavelength"
      
        self.Wavelength_Combo_low = 3.34e7/(self.Plasma_Den_Combo_low)**0.5
        print str(self.Wavelength_Combo_low) + " Is the low wavelength"
        self.Plasma_Den_Combo = np.linspace(self.Plasma_Den_Combo_low,self.Plasma_Den_Combo_high,num=self.div)
        print str(self.Plasma_Den_Combo[0]) + " Is dens 0 " +str(self.Plasma_Den_Combo[3]) + "is dens 9"
        self.Wavelength_Combo = np.linspace(self.Wavelength_Combo_low,self.Wavelength_Combo_high,num=self.div)
        print str(self.Wavelength_Combo[0]) + " Is dens 0 " +str(self.Wavelength_Combo[3]) + "is dens 9"
        self.KP_Combo =  2*math.pi/self.Wavelength_Combo
        
        
        
        
        
        
        
class Profile: # The profile class is used to hold things related to the shape of the beam and shield transverse profile 
    
    def __init__(self,div):
        self.div = div
        self.range =np.linspace(-20e-6,20e-6,num=master_Res)
        
    def Assign_Range(self,low,high):
        self.range = np.linspace(low,high,num=self.div)
        
    def Super_G(self,r,rl,n): ##SlabProfile.Super_G(SlabProfile.range,slab.Radius,2)
        self.distribution_SG= np.zeros([master_Res,master_Res])
        for s1 in range(0,self.div):
         self.distribution_SG[s1,:] =np.exp(-(r/rl[s1])**(2*n))
         
    def Gaussian_Beam(self,a,rg,b,c,n):
       self.distribution_G=np.zeros([self.div,self.div,self.div,master_Res])
       for s1 in range(0,self.div):
            #s1=emittance counter
            for s2 in range(0,self.div):
                #s2=Energy counter
                for s3 in range(0,self.div):
                    #s3=Plasma kp counter
                    self.distribution_G[s1,s2,s3,:] = a*np.exp(((-((rg-b)**n)) / (2*c[s1,s2,s3]**2)))
    """   
        
    
    def Gaussian_Beam_Density(self,Charge,Length,Radius):
        Ntotal = Charge / c.elementary_charge
        self.Gaussian_Beam_Density = np.zeros([self.div,self.div,self.div,self.div])
            
        for s1 in range(0,self.div):
                #s1=emittance counter 
                percentage = (((float(s1))/masterDiv)*100)
                print "Density calculations "+ str(percentage) + "% complete"
                for s2 in range(0,self.div):
                    #s2=Energy counter
                    for s3 in range(0,self.div):
                        #s3=plasmaKP
                           
                            
                            L_Norm  =1 / (math.sqrt(2*math.pi)*Length[s3])
                            R_Norm = 1 / (math.sqrt(2*math.pi)*Radius[s1,s2,s3])
                            self.Gaussian_Beam_Density[s3,s1,s2,s3] = Ntotal*L_Norm*R_Norm
        
       """ 
        
    def Find_Fields_Gaussian(self,q,Nr,dens,saveB,savelocation):#Charge, range of r values , density array, position in the array
    #Charge, range of r values , density array, position in the array
        
        Rmin=self.range[0]
        self.Efield_G = np.zeros([self.div,self.div,self.div,master_Res])
        for s1 in range(0,self.div):
                #s1=emittance counter
                
                print "Intergration "+ str((float(s1)/master_Div)*100) + "% complete"
                for s2 in range(0,self.div):
                    #s2=Energy counter
                    for s3 in range(0,self.div):
                        #s3=plasmaKP
                        DP = Nr[s1,s2,s3,:]*dens[s3,s1,s2,s3] #  nr varies with width of the shield, dens varies 
                        #DP is the gaussian profile with the density of the beam applied
                        print np.max(DP)
                        for r4 in range (0,master_Res):
                            #s4=range of field calculation
                            BeamMaxMethod =1
                            if BeamMaxMethod ==1 :
                                
                                MaxE=MaxBeamFieldFunction.Emax(driver.Charge,driver.Length_Matched[s3],driver.Radius_Matched[s1,s2,s3])
                                
                                self.Efield_G[s1,s2,s3,r4] =MaxE * self.distribution_G 
                                
                            else:
                                E= (q/(2* c.constants.pi*c.constants.epsilon_0 *self.range[r4])) 
                            
                            
                            
                                Integrate = integrate.quad(lambda r: 2*c.constants.pi *r,Rmin,self.range[r4]) # does the integration for the efield transversly
                                self.Efield_G[s1,s2,s3,r4]= E*Integrate[0]*DP[r4] # applies the shape of the charge distribution being trialed 
                        
                        if saveB ==1:# Gives the option to save these results as an np compatable format
                            filename = "BeamField"+"_"+str(s1)+"_"+str(s2)+"_"+str(s3)
                                 
                                 
                            np.save(os.path.join(savelocation,filename),self.Efield_G[s1,s2,s3,:])
    
    
    
    def Find_Fields_SG(self,q,Nr,dens):#Charge, range of r values , density array, position in the array
        # This is the same as the finding fields for gaussian but its for super gaussian aka the shield 
        Rmin  = self.range[0]
        self.Efield_SG = np.zeros([self.div,self.div,master_Res])
        for D1 in range (0,self.div):    # density change 
        
        
            
            for D2 in range(0,self.div): # s1 = different distrubution
                    
                        for r4 in range (0,master_Res): # loop for examination area 
                            DensityProfile = Nr[D2,:]*dens[D1] #  nr varies with width of the shield, dens varies 
                            #print "densShield=" +str(dens[D1])
                            Rmin=self.range[0]
                            E= (q/(2* c.constants.pi*c.constants.epsilon_0 *self.range[r4]))
    
                            Integrate = integrate.quad(lambda r: 2*c.constants.pi *r,Rmin,self.range[r4])
                            self.Efield_SG[D1,D2,r4]= E*Integrate[0]*DensityProfile[r4]
                            



class SumFields:# in this class the summated fields are stored and calculated 
    #also adk work and comparison of the adk ressults is done here
    
    def __init__(self,div):
         self.div = div
        
    def Assign_BeamField(self,Fields):
        self.BeamField = Fields
    
    def Assign_ShieldField(self,Fields):
        self.ShieldField = Fields
    
    def LoadSumFields(self,name,location):# loads the fields from the specified location assuming they are numpy files 
         self.Selected_Sum_Field = np.load(os.path.join(location,name)+".npy",)
         
    def LoadSumFields_return(self,name,location): # doesthe same as the previous but also returns the fields to be used in a function without making a class variable
        return np.load(os.path.join(location,name)+".npy",)
        
    def SumFields(self):
        #self.ComboField = np.zeros([self.div,self.div,self.div,])
        Combo = np.zeros([master_Div,master_Div,master_Res])
        for s1 in range(0,master_Div):
                #s1=emittance counter
                print "Summation "+ str((float(s1)/master_Div)*100) + "% complete"
                for s2 in range(0,master_Div):
                    #s2=Energy counter
                    for s3 in range(0,master_Div):
                        #s3=plasmaKP
                        
                        BeamFields_abs = abs(self.BeamField[s1,s2,s3,:])
                        #for D1 in range (0,master_Div):    # density change 
        
        
            
                        for D2 in range(0,master_Div): # s1 = different distrubution
                                 #print str(s1)+str(s2)+str(s3)
                                # print  np.nanmax(self.BeamField[s1,s2,s3,:])
                                # print  np.nanmin(self.ShieldField[s3,D2,:])
                                 Combo[s3,D2,:] = BeamFields_abs  - abs(self.ShieldField[s3,D2,:])#+ abs(self.ShieldField[s3,D2,:])
                                 #print str(np.nanmax(Combo[s3,D2,:])) +"\n"
                                 #self.ComboField[s1,s2,s3,D1,D2,:] = abs(self.BeamField[s1,s2,s3,:]) + self.ShieldField[D1,D2,:]
                        filename = "Combo"+"_"+str(s1)+"_"+str(s2)+"_"+str(s3)
                                 
                                 
                        np.save(os.path.join(DataLocation,filename),Combo)

    def ADK(self,z,field): #this the adk fucntion as set out by Bruhwiler et al for use in vsim aslo see note below for theseis reference
        
        ##########################
        #   ADK  # Caolionn O'Connel Thesis "Plasma Production via Field Ioniszation" 
        ##########################
        Z_num = z
        
        #First ionisation potential
        field_nanR =  np.where(field==0, 1e-6, field)
        E_adk =abs(field_nanR)/1e9
        #print " E adk " + str(np.nanmax(E_adk)

        He_ground   =   24.5874
        nadk        =   3.69*Z_num / He_ground**0.5
        # The formula has been split into a few parts to make things easier to read 
        adk1        =   1.52e15 *( 4**nadk * He_ground / ( nadk * math.gamma(2*nadk) ))
        adk2_1      = (He_ground**1.5)/ E_adk
        adk2_2=( 2 * nadk - 1 )
        adk2        =    20.5 * (adk2_1 ** adk2_2)
        adk3        =   np.exp( -6.83 *  ( math.pow( He_ground,1.5)  / E_adk ) )
        adkf        =   adk1 * adk2 * adk3
        self.ADK_rate  = adkf
    
    def ADK_All (self,z,prefix,location,BeamFieldB): # This function grabs in data from saved results and puts it through the adk function 
        Results_ADK = np.zeros([master_Div,master_Div,master_Res])
        
        for s1 in range(0,master_Div):
                #s1=emittance counter
                print "Mass AKD "+ str((float(s1)/master_Div)*100) + "% complete"
                for s2 in range(0,master_Div):
                    #s2=Energy counter
                    for s3 in range(0,master_Div):
                        #s3=plasmaKP
                         filename = prefix+"_"+str(s1)+"_"+str(s2)+"_"+str(s3) # sets the filename based ona  loop and prefix
                         field  = self.LoadSumFields_return(filename,location) # calls the external fucntion to load the file 
                         
                         
                         if np.any(np.isnan(field)) == True: # checks to see if there are any values of the field that are not numbers 
                             print "NAN detected in Field "+str(s1)+str(s2)+str(s3)
                             print np.isnan(field)
                             quit() # exits the program if there are nan's
                         if BeamFieldB ==1 :
                             self.ADK(z,field)
                             Results_ADK = self.ADK_rate 
                         else :
                             #for D1 in range(0,master_Div):
                                 for D2 in range (0,master_Div):
                                 
                                     self.ADK(z,field[s3,D2,:])
                                     Results_ADK[s3,D2,:] = self.ADK_rate
                                     
                                     if np.any(np.isnan(Results_ADK[s3,D2,:])) == True: # checks to see if there are any values of the field that are not numbers 
                                         print "NAN detected in Field "+str(s1)+str(s2)+str(s3)
                                         print np.isnan(field)
                                         quit()
                         DataLocationADK = location +"\\adk" # sets a location to a supplied directory with adk sub directory 
                         filename = "ADK"+"_"+str(s1)+"_"+str(s2)+"_"+str(s3) # creates the filename for the adk files 
                         np.save(os.path.join(DataLocationADK,filename),Results_ADK) #saves as .py format


    
    
    def Ionization_Fraction(self,FieldLocation,Fieldname,ExposureTime): # calculates the ionsiation fraction based on Alex knetsch's thesis work 
        #FieldLocation = FieldLocation + "\\adk"
        
        ADK_rate = self.LoadSumFields_return(Fieldname,FieldLocation) # loads the adk results 
        IonizationAmount = ADK_rate * ExposureTime  # here the adk rate is connected to the " exposure time" which indicated how long the gas should be exposed to the beam fields 
        #Fraction = IonizationAmount/GasDensity
        Fraction = 1- np.exp( - IonizationAmount)
        return Fraction # returns the fraction of the gas that is ionised 
        
    
    
    
    def exposureTime(self,s2,s3): 
        gamma = driver.Energy[s2]/ 0.511e6 +1
        ConL = driver.Length_Matched[s3]/gamma # relativisticly contracted matched driver length
        ETime = ConL/c.constants.c
        return ETime 
        
        
    def Compare_ADK(self,BeamFieldLocation,BeamFieldname,ComboFieldLocation,ComboFiendname,s1,s2,s3,BuildFO,SaveResults,PandaStart): # Here the beam ionisation and the beam+shield ionisation is compared
        BeamFieldLocation = BeamFieldLocation + "\\adk"
        ComboFieldLocation =ComboFieldLocation + "\\adk"
        Dindex = np.zeros([2]) # this is used to remove an extra array element later
        ExposureTime = self.exposureTime(s2,s3)#3e-18 # current guess of exposure time 
       
        ComboFraction_GMax = None 
        BeamFraction = self.Ionization_Fraction(BeamFieldLocation,BeamFieldname,ExposureTime) # loads the ionisation fraction of the beam 
        ComboFraction = self.Ionization_Fraction(ComboFieldLocation,ComboFiendname,ExposureTime)# loads the ionisation fraction of the beam+shield 
        #combo fraction is the ionisatio n fraction for all shield and beam combo fields
        BeamFraction_max = np.nanmax(BeamFraction)
        Midpoint =1  #(master_Div/2)-1 # this picks out the index value in the parameter space that is to be lcoked for the current parameter being examined
        RadiusMatchCheck =1
        #for D1 in range(0,master_Div):
        
            
        for D2 in range(0,master_Div): # this loops through all radius options for the current parameters
                if np.any(np.isnan(ComboFraction[s3,D2,:])) == True: # check if any of the values in the loaded and selecte data are nan and exits if they are 
                                         print "NAN detected in Field "+str(s1)+str(s2)+str(s3)
                                         print np.isnan(ComboFraction[s3,D2,:])
                                         quit()
               
                if SaveResults: # give the option to save results ina a .txt
                    if np.nanmax(ComboFraction[s3,D2,:]) < 0.9 :# this is where the max ionisation fraction is selected
                            # The second part of the if condition is related to the plasma kp to allow the limit of the plasma wavelength being examined
                            #and s3 < 5
                            # this can be changed by varing which "s" is used 
                            #print ComboFraction[D1,D2,:]
                            
                                
                            print "We have a Winner! - s3 = " +str(s3)+"  D2 = " +str(D2)
                            print " Matching case =" +str(BeamFieldname)
                            BeamFraction_max = np.nanmax(BeamFraction)
                            print "Beam Fraction max = " +str(BeamFraction_max)
                            CurrentIndex = np.array([s3,D2])
                            ComboFraction_Max = np.nanmax(ComboFraction[s3,D2,:])
                            print "Combo Fraction max = " +str(ComboFraction_Max)
                            FractionReduction= BeamFraction_max - ComboFraction_Max
                            print "Reduction in ionisation fraction = " +str(FractionReduction)
                            print "exposure Duration is = " +str(ExposureTime)
                            
                            f=open ("G:\\May 2019\\Shield\\Beam Matching V2\\Testing\\Results\\Results.txt",'a')
                            f.write(" Matching case =" +str(BeamFieldname)+"  Shield setting= "+str(CurrentIndex)+"\n")
                            f.write("Beam Fraction max = " +str(BeamFraction_max)+"\n")
                            f.write("Combo Fraction max = " +str(ComboFraction_Max)+"\n")
                            f.write("Reduction in ionisation fraction = " +str(FractionReduction)+"\n")
                            f.write("Drive Beam Energy = "+ str(driver.Energy[s2])+"\n")
                            f.write("Drive Beam Emittance = "+ str(driver.Emittance[s1])+"\n")
                            f.write("Drive Beam Length  = "+ str(driver.Length_Matched[s3])+"\n")
                            f.write("Drive Beam Radius = "+ str(driver.Radius_Matched[s1,s2,s3])+"\n")
                            f.write("Plasma Density = "+ str(LIT_Plasma.Plasma_Den_Combo[s3])+"\n")
                            f.write("Plasma wavelengthCombo = "+ str(LIT_Plasma.Wavelength_Combo[s3])+"\n")
                            f.write("Shield Density = "+str(slab.Density[s3])+"\n")
                            f.write("Shield Radius = " +str(slab.Radius[D2])+"\n"+"\n"+"\n"+"\n")
                            
                            f.close()
                            #the data above s written to a txt file 
                            
                            
                            fig9= plt.figure()
                            ax9= fig9.add_subplot(111)
    
                            ax9.plot(SlabProfile.range,BeamFraction,color ="blue")
                            ax9.plot(BeamProfile.range,ComboFraction[s3,D2,:],color ="green")
                            ax9.set_xlabel("Transverse location in meters")
                            ax9.set_ylabel("Gas Ionisation Fraction")
                            ax9.legend()
                            ax9.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
                            ax9.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
                            ax9.autoscale(enable=True, axis='x', tight=True)
                            fig9.savefig("G:\\May 2019\\Shield\\Beam Matching V2\\Testing\\Results\\Ionisation_Results_"+str(s1)+"_"+str(s2)+"_"+str(s3)+"Shield_"+str(s3)+"_"+str(D2))
                            #plt.show
                            # Here a figure is plotted for each iteration of D2 ( Radius of the shield ) that meets the if condition 
                            
                            
                if PandaStart : # here some data was added into some variables to start making it usable for the PANDA data structure. 
                    if s1 ==Midpoint and s2== Midpoint and D2 == Midpoint:
                        
                        if np.nanmax(ComboFraction[s3,D2,:]) < 0.91:
                                
                                 BeamFraction_max=np.nanmax(BeamFraction)
                                 ComboFraction_Max=np.nanmax(ComboFraction[s3,D2,:])
                                 FractionReduction= BeamFraction_max - ComboFraction_Max
                                 
                                 self.BeamFraction= np.append(self.BeamFraction,BeamFraction_max)
                                 self.ComboFraction_Max =np.append(self.ComboFraction_Max,ComboFraction_Max )
                                 self.FractionReduction =np.append(self.FractionReduction,FractionReduction )
                                 self.DriveBeamEnergy = np.append(self.DriveBeamEnergy,(driver.Energy[s2]) )
                                 self.DriveBeamEmittance =np.append(self.DriveBeamEmittance,(driver.Emittance[s1]) )
                                 self.DriveBeamLength =np.append(self.DriveBeamLength,(driver.Length_Matched[s3]))
                                 self.DriveBeamRadius =np.append(self.DriveBeamRadius,(driver.Radius_Matched[s1,s2,s3]))
                                 self.PlasmaWavelegnthCombo = np.append(self.PlasmaWavelegnthCombo,(LIT_Plasma.Wavelength_Combo[s3]))
                                 self.PlasmaDensityCombo=np.append(self.PlasmaDensityCombo,(LIT_Plasma.Plasma_Den_Combo[s3]))
                                 self.ShieldDensity = np.append(self.ShieldDensity,(slab.Density[s3]))
                                 self.ShieldRadius= np.append(self.ShieldRadius,(slab.Radius[D2]))
                                 
                # The bellow section is used for  looking into the scaling of the 
                if BuildFO :
                    
                   
                   #print s1
                   #print s2
                   #print ComboFraction_Max #+str(s1)+str(s2)
                   if s1 ==Midpoint and s2== Midpoint and D2 == RadiusMatchCheck:
                       #print s1 ,s2 ,D2
                       ComboFraction_Max = np.nanmax(ComboFraction[Midpoint,Midpoint,:])
                       ComboFraction_GMax = ComboFraction_Max
                   #if ComboFraction_Max < ComboFraction_GMax and s1 == Midpoint and s2 == Midpoint  :
                       
                       ComboFraction_GMax = np.max(ComboFraction[s3,D2,:])
                #print ComboFraction_GMax  +"inloop"     
                        
                        
                        
                        
                
                    
        #print ComboFraction_GMax                6
                if BuildFO :   
                  
                   if s1  == Midpoint and s2 == Midpoint and D2 == RadiusMatchCheck: # freezes the variables that are not being examined for scaling 
                       #Prints basic information
                       print "s numbers =" +str(s1)+str(s2)+str(s3)
                       print "combo Max =" +str(ComboFraction_GMax)
                       print "beam max =" +str(np.nanmax(BeamFraction))
                       print "exposure Time =" +str(ExposureTime)
                       print "beam Length =" + str(driver.Length_Matched[s3])
                       print "Plasma wavelengthCombo = "+ str(LIT_Plasma.Wavelength_Combo[s3])
                       print "Radius of shield is " + str(slab.Radius[D2])
                       print "Drive Beam Radius is " + str(driver.Radius_Matched[s1,s2,s3])+"\n"
                       
                       
                       
                       FractionReduction1 = np.nanmax(BeamFraction) -  np.nanmax(ComboFraction[s3,D2,:])
                       FractionReduction_local = np.array((s3,FractionReduction1))
                       self.FractionReductionF = np.vstack((self.FractionReductionF,FractionReduction_local))



                       #print " Matching case =" +str(BeamFieldname)+"\n"
                       #Creates a small array that stored the variable that is being selected and the ionsiation fraction related to it 
                       kpVarRC_local = np.array((s3,ComboFraction_GMax)) #combingin the plasma wavelength combo and ionsiation fraction
                       #print kpVarRC_local
                       self.KpVarRC=np.vstack((self.KpVarRC,kpVarRC_local)) # The small local aray is then stacked on the main array 
                       
                       # This is then repeated with the ionisation of the only the beam for comparison 
                       KpVarRB_local = np.array((s3,BeamFraction_max))
                       self.KpVarRB=np.vstack((self.KpVarRB,KpVarRB_local))
                       #Dindex = np.vstack((Dindex,CurrentIndex))
                       ScalingRunLog_local = np.array((s1,s2,s3))
                       self.ScalingRunLog= np.vstack((self.ScalingRunLog,ScalingRunLog_local))
        Dindex = np.delete(Dindex,0,0)
        
                
                    
        
        return Dindex
        
        
        
    def Mass_Compare_ADK(self,BeamADKLocation,ComboADKLocation,BuildFO,SaveResults,PandaStart): # Control function for the comparing of adk 
        self.GoodSetups = np.zeros([0,0,0,0,0])
        self.KpVarRC= np.zeros([2])#results from variying s3, the plasma kp for combo fields
        self.FractionReductionF =np.zeros([2])
        self.KpVarRB = np.zeros([2])#results from variying s3, the plasma kp for beam fields
        self.ScalingRunLog = np.zeros([3])
        f=open ("G:\\May 2019\\Shield\\Beam Matching V2\\Testing\\Results\\Results.txt",'w')
        f.close()
        if PandaStart : # sets up variables for using the pandas fromework 
            self.BeamFraction = np.zeros([1])
            self.ComboFraction_Max= np.zeros([1])
            self.FractionReduction = np.zeros([1])
            self.DriveBeamEnergy = np.zeros([1])
            self.DriveBeamEmittance =np.zeros([1])
            self.DriveBeamLength =np.zeros([1])
            self.DriveBeamRadius =np.zeros([1])
            self.PlasmaDensityCombo =np.zeros([1])
            self.PlasmaWavelegnthCombo =np.zeros([1])
            self.ShieldDensity =np.zeros([1])
            self.ShieldRadius =np.zeros([1])
        
        # these loops are then used to trigger the compare adk fuction to allow each varaible setting to be sent through the comparison function 
        for s1 in range(0,master_Div):
                #s1=emittance counter
                print "Mass AKD Comparison "+ str((float(s1)/master_Div)*100) + "% complete"
                for s2 in range(0,master_Div):
                    #s2=Energy counter
                    for s3 in range(0,master_Div):
                        #s3=plasmaKP
                        filename = "ADK"+"_"+str(s1)+"_"+str(s2)+"_"+str(s3)
                        CompareResults  = self.Compare_ADK(BeamADKLocation,filename,ComboADKLocation,filename,s1,s2,s3,BuildFO,SaveResults,PandaStart)
                         
                       
                        if np.size(CompareResults) >1:
                            print "Saving good results" 
                            #np.array([s1,s2,s3,])
                            #np.append(self.GoodSetups,[s1,s2,s3,CompareResults[0],CompareResults[1]])
        
        if PandaStart:  # Inside this isf is the experimental work with pandas its very much not finished or in a good way 
            #remove zeros from the start of the arrays
            self.BeamFraction = np.delete(self.BeamFraction,0,0)
            self.ComboFraction_Max= np.delete(self.ComboFraction_Max,0,0)
            self.FractionReduction = np.delete(self.FractionReduction,0,0)
            self.DriveBeamEnergy = np.delete(self.DriveBeamEnergy,0,0)
            self.DriveBeamEmittance = np.delete(self.DriveBeamEmittance,0,0)
            self.DriveBeamLength = np.delete(self.DriveBeamLength,0,0)
            self.DriveBeamRadius = np.delete(self.DriveBeamRadius,0,0)
            self.PlasmaDensityCombo = np.delete(self.PlasmaDensityCombo,0,0)
            self.PlasmaWavelegnthCombo = np.delete(self.PlasmaWavelegnthCombo,0,0)
            self.ShieldDensity = np.delete(self.ShieldDensity,0,0)
            self.ShieldRadius = np.delete(self.ShieldRadius,0,0)
            
            
            self.PlasmaWavelegnthCombo=np.flip(self.PlasmaWavelegnthCombo,0)
            
            #create pandas dataframe
            print np.shape(self.BeamFraction), np.shape(self.ComboFraction_Max) , np.shape(self.FractionReduction) , np.shape(self.DriveBeamEnergy) , np.shape(self.DriveBeamEmittance)  , np.shape(self.PlasmaWavelegnthCombo) 
            
            data1={'BeamFraction':self.BeamFraction[:],'ComboFraction':self.ComboFraction_Max[:],'FractionReduction':self.FractionReduction[:],
                   'DriveBeamEnergy':self.DriveBeamEnergy[:],'DriveBeamEmittance':self.DriveBeamEmittance[:], 'PlasmaWavelegnthCombo':self.PlasmaWavelegnthCombo[:] }
            
            
            database1 = pd.DataFrame(data=data1)
            
            #ax = plt.gca()
            #database1.plot(x='PlasmaWavelegnthCombo',y='ComboFraction',kind='scatter', xlim=(0,325e-6),ax=ax)#x='PlasmaWavelegnthCombo'
            #database1.plot(x='PlasmaWavelegnthCombo',y='BeamFraction',kind='scatter', xlim=(0,325e-6),color='red',ax=ax).legend(title='age', bbox_to_anchor=(1, 1))#x='PlasmaWavelegnthCombo'
            print database1
            #plt.show()
        
        self.FractionReductionF = np.delete(self.FractionReductionF,0,0)
        self.KpVarRC = np.delete(self.KpVarRC,0,0)
        self.KpVarRB = np.delete(self.KpVarRB,0,0)
        
        self.ScalingRunLog = np.delete(self.ScalingRunLog,0,0)
        
        if PandaStart:
            return database1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


driver= DBeam(master_Div) # creates the driver class
driver.Assign_Charge(1e-9) # sets up charge
driver.Assign_Emittance(10e-6,70e-6) # sets up the emittance range for the driver 
driver.Assign_Energy(1e9,10e9) # energy range for the driver 

print "DRiver emittance " +str(driver.Emittance[0])

print "DRiver Energy " +str(driver.Energy[0])

LIT_Plasma = Plasma(master_Div)# creates the plasma class
LIT_Plasma.Assign_Wavelength(80e-6,500e-6) # sets initial plasma wavelength range 

slab=Shield(master_Div) # sets up the slab shield class 
slab.Assign_Density(1e22,8e22) # densit y range for the shield
slab.Assign_Radius(5e-6,15e-6) # radius range for the shield

#print "Shield Radius" +str(slab.Radius[9])
LIT_Plasma.Get_Combo_Wavelength(slab.Density) #calculates the combo KP and Plasma wavelength 

SlabProfile = Profile(master_Div) # creats the class for the shield profile
SlabProfile.Super_G(SlabProfile.range,slab.Radius,2) # 2 refers to the mode of the super gaussian

driver.Matched_Radius(LIT_Plasma.KP_Combo) # finds the matched radius of the driver for each parameter space 
driver.Matched_Length(LIT_Plasma.KP_Combo) # finds the matched length of the driver for each parameter space 
  
driver.Calculate_Density(driver.Length_Matched,driver.Radius_Matched) #finds drive beam density 


#data printing
#print LIT_Plasma.Plasma_Den_Combo[0]
#print LIT_Plasma.Wavelength_Combo[0]
#print driver.Radius_Matched[0,0,0]
#print LIT_Plasma.Plasma_Den_Combo[5]
#print LIT_Plasma.Wavelength_Combo[5]
#print driver.Radius_Matched[0,0,5]
BeamProfile = Profile(master_Div)

print str(np.min(driver.Length_Matched))+"matched length min"
print str(np.max(driver.Length_Matched))+"matched length max"
#BeamProfile.Assign_Range(-20e-6,20e-6)

BeamProfile.Gaussian_Beam(1,BeamProfile.range,0,driver.Radius_Matched,2) # 2.6
#driver.Radius_Matched

alpha=BeamProfile.distribution_G


#BeamProfile.Gaussian_Beam_Density(2e-9,driver.Length_Matched,driver.Radius_Matched)
#print np.shape(BeamProfile.Gaussian_Beam_Density)
#print BeamProfile.Gaussian_Beam_Density[5,5,5,5]


#Calculate and savefields of distributions

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BeamFieldSaveLocation = 'G:\\May 2019\\Shield\\Beam Matching V2\\Testing\\BeamField' # This is the base data location the script will use 
BeamProfile.Find_Fields_Gaussian(c.elementary_charge,BeamProfile.distribution_G,driver.Beam_Density_Gaussian,1,BeamFieldSaveLocation) # The beam fields are found here 
#np.save("field_data_Gaussian",BeamProfile.Efield)

#
SlabProfile.Find_Fields_SG(c.elementary_charge,SlabProfile.distribution_SG,slab.Density) # shield fields are found here 
#print SlabProfile.Efield_SG[1,20]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Summing Efields
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#Absolute value is used for the beam e field 

DataLocation ='G:\\May 2019\\Shield\\Beam Matching V2\\Testing' # this is the data location that is used for the adk calculations 
totals = SumFields(100)
totals.Assign_BeamField(BeamProfile.Efield_G) # sets the beam field in the totals class 
#print np.shape(SlabProfile.Efield_SG)
totals.Assign_ShieldField(SlabProfile.Efield_SG) # sets the shield field in the totals class 
totals.SumFields()
#totals.LoadSumFields("Combo__9_8_9")
#totals.ADK(1,totals.Selected_Sum_Field[9,9,:])

totals.ADK_All(1,'Combo',DataLocation,1)
totals.ADK_All(1,'BeamField',BeamFieldSaveLocation,1)



#ktest= totals.LoadSumFields_return('Combo_0_1_3',DataLocation)
Comboakdlocation ='G:\\May 2019\\Shield\\Beam Matching V2\\Testing' # sets the save location for the comparison of the adk data 
#ltest = totals.LoadSumFields_return('ADK_0_0_3',Comboakdlocation)
#totals.LoadSumFields("ADK_0_2_9",'D:\\May 2019\\Shield\\Beam Matching V2\\Testing\\adk')
#akka = totals.Compare_ADK(BeamFieldSaveLocation,"ADK_0_1_9",Comboakdlocation,"ADK_0_1_9")
#print "akka=" +str(akka)
#print np.shape(akka)
Results = totals.Mass_Compare_ADK(BeamFieldSaveLocation,Comboakdlocation,BuildFO=True,SaveResults=True,PandaStart=False) # triggers the mass adk compare once that has been calculated 
#print totals.KpVarRB
#print totals.KpVarRC
#print Results
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Plotting
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
plot_Profiles_Many =0
plot_Profile = 0
plot_EField_SG =1
plot_EFIeld_G = 1 
plot_BeamDensity_G = 0
plot_LoadedSumField = 0
plot_ADK_LoadedField = 0
plot_Fraction_Plasmakp =1
plot_Wavelength_BeamRadius=0


if plot_Profiles_Many == 1:
    for kappa in range(0,100):
        fig0= plt.figure()
        ax0= fig0.add_subplot(111)

        ax0.plot(SlabProfile.range,SlabProfile.distribution_SG,color ="blue")
        ax0.plot(BeamProfile.range,BeamProfile.distribution_G[kappa,:,kappa],color ="green")
        ax0.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax0.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax0.autoscale(enable=True, axis='x', tight=True)
        



if plot_Profile == 1:
        fig0= plt.figure()
        ax0= fig0.add_subplot(111)

        ax0.plot(SlabProfile.range,SlabProfile.distribution_SG[1,:],color ="blue")
        ax0.plot(BeamProfile.range,BeamProfile.distribution_G[1,1,1],color ="green")
        ax0.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax0.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax0.autoscale(enable=True, axis='x', tight=True)
        


if plot_EField_SG == 1:
        fig1= plt.figure()
        ax1= fig1.add_subplot(111)
        fig1.set_size_inches(10.5, 10.5)
        ax1.set_title("Field for Super Gaussian")
        ax1.plot(SlabProfile.range,SlabProfile.Efield_SG[2,2,:]/1e9,color ="blue")
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax1.autoscale(enable=True, axis='x', tight=True)
        
if plot_EFIeld_G == 1:
        fig1= plt.figure()
        ax1= fig1.add_subplot(111)
        fig1.set_size_inches(10.5, 10.5)
        ax1.set_title("Field for Gaussian")
        ax1.plot(BeamProfile.range,abs(BeamProfile.Efield_G[1,1,1,:]/1e9),color ="green") #self.Efield_G[s1,s2,s3,r4]=
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax1.autoscale(enable=True, axis='x', tight=True)
        
        
        
if plot_BeamDensity_G == 1:
        fig1= plt.figure()
        ax1= fig1.add_subplot(111)
        fig1.set_size_inches(10.5, 10.5)

        ax1.plot(BeamProfile.range,driver.Beam_Density_Gaussian[1,1,1,:],color ="green") #self.Efield_G[s1,s2,s3,r4]=
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax1.autoscale(enable=True, axis='x', tight=True)
        
if plot_LoadedSumField == 1:
        fig1= plt.figure()
        ax1= fig1.add_subplot(111)
        fig1.set_size_inches(10.5, 10.5)

        ax1.plot(BeamProfile.range,totals.Selected_Sum_Field[9,9,:],color ="green") #self.Efield_G[s1,s2,s3,r4]=
        ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax1.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax1.autoscale(enable=True, axis='x', tight=True)
        
if plot_ADK_LoadedField ==1:
        fig6= plt.figure()
        ax6= fig6.add_subplot(111)
        fig6.set_size_inches(10.5, 10.5)

        ax6.plot(BeamProfile.range,totals.ADK_rate,color ="green") #self.Efield_G[s1,s2,s3,r4]=
        ax6.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax6.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax6.autoscale(enable=True, axis='x', tight=True)
        
        
if plot_Fraction_Plasmakp == 1:
        fig0= plt.figure()
        ax0= fig0.add_subplot(111)
        #ax0.set_yscale("log")
        intAddr = totals.KpVarRB[:,0].astype(int)
        #print intAddr
        #intAddr =np.flip(intAddr,0)
        xprop = LIT_Plasma.Wavelength_Combo[intAddr]
        #print xprop
        #print intAddr
        #ax0.scatter(xprop,LIT_Plasma.Wavelength_Combo)
        
        #ax0.scatter(xprop,LIT_Plasma.Wavelength)
        ax0.scatter(xprop,totals.KpVarRB[:,1],color ="blue") #beam
        ax0.scatter(xprop,totals.KpVarRC[:,1],color ="green") #Combined
        #ax0.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax0.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        ax0.set_xlim((np.min(xprop)-10e-6,np.max(xprop)+10e-6))
        ax0.set_title("Plasma Wavelength Scaling 36um rad , 5GeV 1nC, 13um shield radius")
        ax0.set_ylabel("Ionisation Fraction")
        ax0.set_xlabel("Combined Plasma wavelength")
        #ax0.autoscale(enable=True, axis='x', tight=True)
        fig0.savefig("C:\\Users\\Andrew\\Documents\\Git\\General-Scripts\\Python\\PhD\\Scaling\\Wavelength\\\Wavelegnth-Energy\\WavelengthScaling_36umrad_5gev_1nc_13umShield.png",bbox_inches="tight",dpi=250)
        

if plot_Wavelength_BeamRadius ==1:
    #fig1b = plt.figure()
    #ax0=fig1b.add_subplot(111,projection='3d')
    Z = totals.FractionReductionF[:,0]
    #X = LIT_Plasma.Wavelength_Combo[:.0]
    print totals.ScalingRunLog
    Y = np.zeros(master_Div)
    for k1 in range(0,master_Div):
        
        Y[k1]= driver.Radius_Matched[int(totals.ScalingRunLog[k1,0]),int(totals.ScalingRunLog[k1,1]),int(totals.ScalingRunLog[k1,2])]
    intAddr = totals.KpVarRB[:,0].astype(int)
    intAddr =np.flip(intAddr,0)
    X =  LIT_Plasma.Wavelength_Combo[intAddr]
    
    print Y   
    print X
    #Y =  driver.Radius_Matched[0,0,0]
    #print  X
    #print Y
print "Done!"