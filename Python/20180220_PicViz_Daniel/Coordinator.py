# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 21:53:09 2016

@author: Paul Scherkl
"""
from dataManager import dataManager
from dumpSummarizer import dumpSummarizer

from plotter2D import plotter2D
from plotterPhaseSpace import plotterPhaseSpace

from elecDump import elecDump

import matplotlib.pyplot as plt

import numpy as np

class Coordinator(object):
    """class that maintains the whole program. includes loading, plotting, and summarizing of PIC output"""   
    
    
    def __init__(self, path, dumpNumbers, prefix, particleDumpList, fieldDumpList):
        #--------------------------------------------------------
        #       Main objects and dump identifiers
        #--------------------------------------------------------
        self._info()
        self.filePath    = path
        if isinstance(dumpNumbers, (int, float)):
            self.dumpNumbers = [dumpNumbers]
        else:
            self.dumpNumbers = np.zeros(len(dumpNumbers), dtype = int)        
            for i in range(len(dumpNumbers)):
                self.dumpNumbers[i] = int(dumpNumbers[i])
        
        
        self.prefix      = prefix     
        self.particleDumpList = particleDumpList
        self.fieldDumpList    = fieldDumpList

        self._Manager           = dataManager(particleDumpList, fieldDumpList, self.filePath, self.prefix)
        self._Plotter2D         = plotter2D(self._Manager)
        self._PlotterPhaseSpace = plotterPhaseSpace(self._Manager)
        self._Summarizer        = dumpSummarizer(self._Manager, self.dumpNumbers)
        

        self.cutList        = []
        self.phaseSpaceList = [] 
        
        #--------------------------------------------------------
        #       Plotting identifiers
        #--------------------------------------------------------
        
        self.loadFieldDumps       = 0
        self.loadElecMultiField   = 0
        self.loadLasersPlusPlasma = 0
        self.loadBeamElectrons    = 0
        self.loadParticlesDumps   = 0

        
        
  








      
    def dumpLoop(self):
        """ iterates over all sepcified .h5 files, calls loading, plotting, and summarizing methods"""
        
        if len(self.dumpNumbers) == 0:
                raise Exception("    (!) Loading aborted: list of dumpNumbers empty!")
                

        for currentDump in self.dumpNumbers:
            
            #--------------------------------------------------------
            #       Load specified .h5 files
            #--------------------------------------------------------

            self._Manager.dumpNumber   = currentDump
            self._loadFiles(currentDump)
                
            
            #--------------------------------------------------------
            #       Cut phase space
            #--------------------------------------------------------
            
            self._cutPhaseSpace()
            
            
            
            #--------------------------------------------------------
            #       Analyze + Summarize
            #--------------------------------------------------------
            self._Summarizer.update()
            self._Summarizer.exportDataToFile()
            
            
            #--------------------------------------------------------
            #       Plot
            #--------------------------------------------------------
            self._plotPhaseSpaces()
            self._Plotter2D.plot2DWindow()  
            plt.close("all")   
        self._finalMessage()
        
        
        
    def plotPhaseSpaces(self, bunch, comp):
        """ plot phase spaces for given @bunch and component @comp (can be x, y, z, and t)"""
        self.loadParticlesDumps = 1
        self.phaseSpaceList.append((bunch, comp))
        
        
    def _plotPhaseSpaces(self):
        """plots single phase space images, only to be used in Coordinator.pz!!!"""
        for i in range(len(self.phaseSpaceList)): 
                self._PlotterPhaseSpace.plotSingle(self.phaseSpaceList[i][0], self.phaseSpaceList[i][1])
        
             
                
                
                
        
    def cutPhaseSpace(self, bunch, comp, lower, upper):
        """ to be called in main.py.  all particles of specified @bunch from phase space @comp outside of |lower - upper| are ignored in plots. 
        bunch can be either 0,1,.. or HeElectrons, NeElectrons etc for witness beams or driver for the drive beam"""
        self.cutList.append((bunch,comp, lower, upper))
        
        
        
    def _cutPhaseSpace(self):
        """ calls cutting routine, only to be used in Coordinator.pz!!!"""
        for i in range(len(self.cutList)): 
            if isinstance(self.getParticleDump(self.cutList[i][0]), elecDump) and self.getParticleDump(self.cutList[i][0]).loaded:
                self.getParticleDump(self.cutList[i][0]).cutPhaseSpace(self.cutList[i][1], self.cutList[i][2], self.cutList[i][3])
        
        
        
        
                
                
                
                
                
                
                
#--------------------------------------------------------
#       Loading methods
#--------------------------------------------------------
    
        
    def _checkLoadingIdentifiers(self):
        """ checks for plot or analysis requests from user and enables loading of dump types"""
        
        
           
        for field in self._Manager.fieldSpecies:        
                if field.analyze or field.plot2D or field.plotFieldLO or field.plotFieldGradLO or field.plotPotLO or field.plotPot2D or field.plot3D:
                    self.loadFieldDumps = 1
                    if field.name == "lasersPlusPlasma":
                        self.loadLasersPlusPlasma = 1
                    elif field.name =="ElecMultiField":
                        self.loadElecMultiField   = 1
             
        if self.getParticleDump("driver").plot2D or self.getParticleDump("driver").analyze or self.getParticleDump("driver").plot3D or self.getParticleDump("driver").plotHist or self.getParticleDump("driver").historyTags is not []:
            self.loadBeamElectrons = 1
            if self.getParticleDump("driver").saveAccField or self.getParticleDump("driver").saveZeroCrossing:
                    self.loadElecMultiField = 1
                    self.loadLasersPlusPlasma = 1
                    

            
        for dump in self._Manager.particleSpecies:
                if dump.analyze or dump.plot2D or dump.plot3D or dump.plotHist or dump.historyTags is not []:
                    self.loadParticlesDumps = 1
                    if dump.saveAccField or dump.saveZeroCrossing:
                        self.loadElecMultiField   = 1
                        self.loadLasersPlusPlasma = 1
                                
                        
            
            
            
            
    def _loadFiles(self, dumpNumber):
        """loads and saves all specified .h5 dumps for an individual PIC snapshot """

        #--------------------------------------------------------
        #       Replace data from previous dump with empty objects
        #--------------------------------------------------------
        self._Manager.beamElectrons.reset()
        for i in range(len(self.fieldDumpList)):
            self._Manager.fieldSpecies[i].reset()
            self._Manager.fieldDumpList[i] = self._Manager.fieldDumpListDummy[i]
            self._Manager.fieldSpecies[i].name = self._Manager.fieldDumpListDummy[i]    
        
        for i in range(len(self.particleDumpList)):
            self._Manager.particleSpecies[i].reset()
            self._Manager.particleDumpList[i] = self._Manager.particleDumpListDummy[i]
            self._Manager.particleSpecies[i].name = self._Manager.particleDumpListDummy[i]    
        #--------------------------------------------------------
        #       Start loading
        #--------------------------------------------------------
        print "\n----------------------------------------------------"
        print "\nStart loading dumps: "  + self.prefix + "_" + str(dumpNumber)
       
        self._checkLoadingIdentifiers()
        
        if self.loadFieldDumps:
            for numField in range(len(self.fieldDumpList)):
                self._Manager.loadFieldDump(numField)  
               
        if self.loadBeamElectrons:
            self._Manager.loadBeamElectrons()

        if self.loadParticlesDumps:
            for numSpecies in range(len(self.particleDumpList)):
                self._Manager.loadWitnessSpecies(numSpecies)  
            


#

    def _info(self):
        print "--------------------------------------------------------------------------------------------------------------------------------------"
        print "picVisualizer v1.01.20 (Dec2017) by P. Scherkl*, O. Karger, T. Heinemann, A. Beaton, A. F. Habib (University of Strathclyde, University of Hamburg).\n Please report bugs, suggestions, or questions to Paul (paul.scherkl@strath.ac.uk)"
        print "--------------------------------------------------------------------------------------------------------------------------------------"

    def _finalMessage(self):
        print "\n \n"
        print "-----------------------------------------------------------------------------"
        print "                 picVisualizer finished. Great success!"
        print "-----------------------------------------------------------------------------"
           
#--------------------------------------------------------
#       Dump object getters
#--------------------------------------------------------
                
                
    def getParticleDump(self, dump):
        return self._Manager.getParticleDump(dump)
  
    def getFieldDump(self, dump):
        return self._Manager.getFieldDump(dump)
      
    def getElecMultiField(self):
        return self._Manager.getElecMultiField()
                
    def getLasersPlusPlasma(self):
        return self._Manager.getLasersPlusPlasma()
        
    def getSumRhoJ(self):
       return self._Manager.getSumRhoJ()
       
        
                
   
