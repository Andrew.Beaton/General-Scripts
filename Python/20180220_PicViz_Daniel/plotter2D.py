# -*- coding: utf-8 -*-
"""
Created on Fri Dec 02 14:07:50 2016

@author: Paul Scherkl
"""

from plotter import *
import matplotlib as mpl
from matplotlib.collections import LineCollection
import scipy.ndimage.interpolation as interpolation
mpl.use('agg')

mpl.rcParams['font.family'] = 'DejaVu Sans'
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['font.sans-serif'] = "DejaVu Sans"
mpl.rcParams['mathtext.cal'] = 'DejaVu Sans'
mpl.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
mpl.rcParams['mathtext.rm'] = 'DejaVu Sans'
mpl.rcParams['text.usetex'] = False
hfont = {'fontname':'Courier'}




class plotter2D(plotter):
    """plotter subclass that maintains 2D plots of the co-movin window in VSIM"""
    def __init__(self, manager):
        plotter.__init__(self, manager)

        self.xLim                = []
        self.yLim                = []
        self.yLimFieldLO         = []
        self.yLimFieldGradLO     = []
        self.yLimPotLO           = []
        self.yLimParticleLO      = []
        self.colorbar            = 0 
        
        self.labelXAxis          = "$\\xi$ [$\mathrm{\mu} \mathrm{m}$]"
        self.labelYAxis          = "y [$\mathrm{\mu} \mathrm{m}$]"
        self.labelYFieldLO       = ""
        self.labelYFieldGradLO   = ""
        self.labelYPotLO         = "$\Delta\Psi$ [e m$_{\mathrm{e}}$$^{\mathrm{-1}}$c$^{\mathrm{-2}}$]"
        self.labelYParticleLO    = "$\mathrm{I}$ [kA]"
        self.labelColorbar       = ""
        self.labelYShift             = 0        
        self.labelYShiftPotLO        = 0        
        self.labelYShiftFieldLO      = 0        
        self.labelYShiftFieldGradLO  = 0        
        self.labelYShiftParticleLO       = 5     
        self.labelSize               = 25
        
        self.showColBar              = 0
        self.tickSize                = 15
        self.tickNumberXAxis         = 5
        self.tickNumberYAxis         = 5
        self.tickNumberYFieldLO      = 5
        self.tickNumberYFieldGradLO  = 5
        self.tickNumberYPotLO        = 5
        self.tickNumberYParticleLO       = 5
        
        self.potLOShowTrappingArea   = 0      
        self.potLOTrappingAreaColor  = "blue"
        self.potLOAxisColor          = "blue"
        self.potLOAxisShiftFactor    = 1
        self.pot2DMax                = -1
        
        
        self.fieldLOAxisColor  = "black"
        self.fieldLOAxisShiftFactor = 1
        
        self.fieldGradLOAxisColor       = "green"
        self.fieldGradLOAxisShiftFactor = 1
        
        self.histLOShiftFactor  = 1
        self.histLOColor        = "red"
       
        
        self.plotLotov               = 0            
        self.lotovRange              = [50,200]
        self.lotovDens               = 4.46224e+22
        self.lotovSlopeFactor        = 2     
        self.plotTzoufras            = 0      
        self.axisMain                = 0
        self.axisParticleLO              = 0
        self.axisFieldLO             = 0
        self.axisFieldGradLO         = 0
        self.axisPotLO               = 0
        
        self.plane = []       
             
        self.colbarTickLeftColor = "w"
        self.colbarTickRightColor = "k"
        self.colbarLabelColor = "k"        
        self.colbarZero = 0

        self.plotParticleColorbar      = 0  
        self.particleColorCodeList     = []
        self.labelParticleColorbar     = 0

        self.setFieldValues              = 0


#--------------------------------------------------------
#       2D Plot setters
#--------------------------------------------------------

    def setLabelParicleColorbat(self, label):
        self.labelParticleColorbar = label

    def setPlotParticleColorbar(self, switch):
        """switches on/off colorbar of particle dumps. Note: requires colormap to be activated!"""
        self.plotParticleColorbar = switch


    def setColbarTickLeftColor(self, color):
        self.colbarTickLeftColor = color

    def setColbarTickRightColor(self, color):
        self.colbarTickRightColor = color

    def setColbarLabelColor(self, color):
        self.colbarLabelColor = color
        
    def setColbarZero(self, value):
        self.colbarZero = value
    

    def setFieldLOAxisColor(self, color):
        self.fieldLOAxisColor = color

    def setFieldLOAxisShiftFactor(self, factor):
        self.fieldLOAxisShiftFactor = factor

    def setFieldLOGradAxisColor(self, color):
        self.fieldLOGradAxisColor = color

    def setFieldLOGradAxisShiftFactor(self, factor):
        self.fieldLOGradAxisShiftFactor = factor

    def setPotLOAxisColor(self, color):
        self.potLOAxisColor = color

    def setPotLOAxisShiftFactor(self, factor):
        self.potLOAxisShiftFactor = factor



    def setHistLOShiftFactor(self, factor):
        """moves y-axis. 1 corresponds to rigth side of plot, factor > 1 moves to the right"""
        self.histLOShiftFactor = factor
        
    def setHistLOColor(self, color):
        """ sets color of charge density lineout"""
        self.histLOColor = color

        
    def setPot2DMax(self, maxValue):
        """sets the highes value that is shown in the 2D trapping potential. default is -1"""
        self.pot2DMax = maxValue
        
      
        
    def plotPotLOTrappingArea(self, switch):
        self.potLOShowTrappingArea = switch
        
    def setPotLOTrappingAreaColor(self, color):
        self.potLOTrappingAreaColor = color
        
       
    
       
    def setLabelYShift(self, shift):
        """ shifts y label position (use positive or negative values)"""
        self.labelYShift

    def setLabelYShiftFieldLO(self, shift):
        """ shifts y label position (use positive or negative values)"""
        self.labelYShiftFieldLO

    def setLabelYShiftPotLO(self, shift):
        """ shifts y label position (use positive or negative values)"""
        self.labelYShiftPotLO

    def setLabelYShiftParticleLO(self, shift):
        """ shifts y label position (use positive or negative values)"""
        self.labelYShiftParticleLO

      
    def setTickSize(self, size):
        """Sets the font size of all axes ticks"""
        if isinstance(size, (int, float)) and size > 0:
            self.tickSize = size
        
        
    def setTickNumberXAxis(self, number):
        """Sets the amount of ticks shown at x-axis """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberXAxis = number
  
          
    def setTickNumberYAxis(self, number):
        """Sets the amount of ticks shown at y-axis (2D plot) """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberYAxis = number
  
          
    def setTickNumberYFieldLO(self, number):
        """Sets the amount of ticks shown at y-axis (fields) """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberYFieldLO = number

    def setTickNumberYFieldGradLO(self, number):
        """Sets the amount of ticks shown at y-axis (fieldGrad) """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberYFieldGradLO = number


            
    def setTickNumberYPotLO(self, number):
        """Sets the amount of ticks shown at y-axis (potentials)"""
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberYPotLO = number
            
    def setTickNumberYParticleLO(self, number):
        """Sets the amount of ticks shown at y-axis (potentials)"""
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberYParticleLO = number
                 

          
    def setLabelSize(self, size):
        """Sets the font size of all axes labels"""
        if isinstance(size, (int, float)) and size > 0:
            self.labelSize = size
        else:
            raise Exception("Please enter a valid number > 0 for the size of axes labels")

  
    def setLabelXAxis(self, label):
        """sets label of lower x-axis. either use any string as input OR use syntax (e.g. $\mathrm{\\xi \\ [\mu m]}$) for LaTeX"""
        self.labelXAxis = label
        
    def setLabelYAxis(self, label):
        """sets label of first y-axis (2D). either use any string as input OR use syntax (e.g.$\mathrm{y \\ [\mu m]}$) for LaTeX"""
        self.labelYAxis = label
        
    def setLabelYFieldLO(self, label):
        """sets label of  y-axis (long fields). either use any string as input OR use syntax (e.g. $\mathrm{E \\ [V/m]}$) for LaTeX"""
        self.labelYFieldLO = label
        
    def setLabelYFieldGradLO(self, label):
        """sets label of  y-axis (long field grad). either use any string as input OR use syntax (e.g. $\mathrm{E \\ [V/m]}$) for LaTeX"""
        self.labelYFieldGradLO = label
        
    def setLabelYPotLO(self, label):
        """sets label of  y-axi (long potentials)s. either use any string as input OR use syntax (e.g. $\mathrm{E \\ [V/m]}$) for LaTeX"""
        self.labelYPotLO = label
        
    def setLabelYParticleLO(self, label):
        """sets label of  y-axi (long potentials)s. either use any string as input OR use syntax (e.g. $\mathrm{E \\ [V/m]}$) for LaTeX"""
        self.labelYParticleLO = label
        
    def setLabelColorbar(self, label):
        """sets label of  y-axi (long potentials)s. either use any string as input OR use syntax (e.g. $\mathrm{E \\ [V/m]}$) for LaTeX"""
        self.labelColorbar = label

        

        
    def setShowColBar(self, switch):
        """show (1) or hide (0) colorbar for """
        if isinstance(switch, (int,float)):
            self.showColBar = switch
        else:
            raise Exception("Wrong switch for colorbar entered. Please use either 0 (off) or 1 (on)")
        
    
    def setXLim(self, lim1, lim2):
        """sets range of x-axis. if not used, axis will be set automatically"""
        self.xLim = [lim1, lim2]
        
    def setYLimFieldLO(self, lim1, lim2):
        """sets range of field lineout y-axis. if not used, axis will be set automatically"""
        self.yLimFieldLO = [lim1, lim2]
        
    def setYLimFieldGradLO(self, lim1, lim2):
        """sets range of field lineout y-axis. if not used, axis will be set automatically"""
        self.yLimFieldGradLO = [lim1, lim2]
        
    def setYLim(self, lim1, lim2):
       """sets range of plot y-axis. if not used, axis will be set automatically"""
       self.yLim = [lim1, lim2]
       
    def setYLimPotLO(self, lim1, lim2):
        """sets range of lineout y-axis. if not used, axis will be set automatically"""
        self.yLimPotLO = [lim1, lim2]

    def setYLimParticleLO(self, lim1, lim2):
        """sets range of lineout y-axis. if not used, axis will be set automatically"""
        self.yLimParticleLO = [lim1, lim2]
        
        
    def setPlotLotov(self, switch):
        self.plotLotov = switch            
        
    def setLotovRange(self, lower, upper):
        """ range in xi where lotov field shall be calculated """
        self.lotovRange = [lower, upper]
   
   
    def setLotovDens(self, dens):
        """ plasma density in 1/m3 """
        self.lotovDens = dens
      
    def setLotovSlopeFactor(self, factor):
        self.lotovSlopeFactor = factor
        
        
        
        
        
        
        
        
        
#--------------------------------------------------------
#       2D plotters
#--------------------------------------------------------
        
        
    def plot2DField(self, fieldObject, axis):
        """plots 2D slice of given fieldDump"""
        if fieldObject.comp2D > -1:
            currentPlane = fieldObject.get2DPlane()
        else:
            currentPlane = fieldObject.get2DPlaneFieldSum()
            
        if fieldObject.cutValue is not "":
            
            if fieldObject.cutDirection == "<":
                if np.max(currentPlane) < fieldObject.cutValue:
                    print "          (!) Problem in cutting field: cut criterion results in empty matrix, command ignored" 
                else:    
                    currentPlane= interpolation.zoom(currentPlane, fieldObject.cutZoomFactor)
                    currentPlane= np.ma.masked_where(currentPlane < fieldObject.cutValue ,currentPlane)
                    
            elif fieldObject.cutDirection == ">":
                if np.min(currentPlane) > fieldObject.cutValue:
                    print "          (!) Problem in cutting field: cut criterion results in empty matrix, command ignored" 
                else:    
                    currentPlane= interpolation.zoom(currentPlane, fieldObject.cutZoomFactor)
                    currentPlane= np.ma.masked_where(currentPlane > fieldObject.cutValue ,currentPlane)
            
        X = fieldObject.xAxis
        Y = fieldObject.yAxis

        if fieldObject.plane2D[1] == 0 and (fieldObject.plane2D[2] == "z" or     fieldObject.plane2D[2] > 0):
            self.labelYAxis =  "z [$\mathrm{\mu} \mathrm{m}$]"
            Y = fieldObject.zAxis
            
        if isinstance(fieldObject.plane2D[0], (float, int)): 
            X, Y = np.meshgrid(Y, fieldObject.zAxis)
        
        if fieldObject.cutZoomFactor > 1 and fieldObject.cutValue is not "":
            X = np.linspace(min(X), max(X), len(X)*fieldObject.cutZoomFactor)
            Y = np.linspace(min(Y), max(Y), len(Y)*fieldObject.cutZoomFactor)
        
        if self.export:
            self.exportData(currentPlane, fieldObject.name + "_backgroundField")
            self.exportData(X, fieldObject.name + "_xAxisField")
            self.exportData(Y, fieldObject.name + "_yAxisField")
            if self.dumpNote:
                print "          2D field, fieldSum, and x,y axes for " + fieldObject.name + " dumped"
                
                
                
                        
        levelVector = np.linspace(np.min(currentPlane), np.max(currentPlane), 1000)
        if fieldObject.colorBarMin == "" and fieldObject.colorBarMax == "":
                if np.min(currentPlane) == np.max(currentPlane):
                    colorbar= axis.imshow(np.transpose(currentPlane), aspect='auto', cmap =fieldObject.colorbar2D, alpha = fieldObject.transparency, origin='lower', extent=[np.min(X),np.max(X),np.min(Y), np.max(Y)])
                else:
                    colorbar = axis.contourf(X, Y, np.transpose(currentPlane),  levels= levelVector, norm=MidpointNormalize(midpoint= self.colbarZero), alpha = fieldObject.transparency, cmap = fieldObject.colorbar2D)
                    for c in colorbar.collections:
                            c.set_edgecolor("face")


        else:
             minVal = 0
             maxVal = 0
             if fieldObject.colorBarMin == "":
                  minVal = np.min(currentPlane)
             elif fieldObject.colorBarMin > np.max(currentPlane):
                minVal = np.max(currentPlane)- 0.0001
             else:
                minVal = fieldObject.colorBarMin
             
             if fieldObject.colorBarMax == "":
                  maxVal = np.max(currentPlane)
             elif fieldObject.colorBarMax < np.min(currentPlane):
                maxVal = np.min(currentPlane) + 0.001
             else:
                 maxVal = fieldObject.colorBarMax
             colorbar= axis.imshow(np.transpose(currentPlane), aspect='auto',vmin=minVal, vmax = maxVal, cmap =fieldObject.colorbar2D, alpha = fieldObject.transparency, origin='lower', extent=[np.min(X),np.max(X),np.min(Y), np.max(Y)])
  
        if fieldObject.showColorbar == 1:
            self.colorbar = colorbar
            self.plane = currentPlane
        
        if self.labelColorbar == "":
            
            if fieldObject.fieldType is not "electric":
                self.labelColorbar = fieldObject.getName()
            else:
                if fieldObject.comp2D == 0:
                    self.labelColorbar = "E$_{\mathrm{x}}$ [GV m$^{\mathrm{-1}}$]"                
                if fieldObject.comp2D == 1:
                    self.labelColorbar = "E$_{\mathrm{y}}$ [GV m$^{\mathrm{-1}}$]"                
                if fieldObject.comp2D == 2:
                    self.labelColorbar = "E$_{\mathrm{z}}$ [GV m$^{\mathrm{-1}}$]"                
                if fieldObject.comp2D == -1:
                    self.labelColorbar = "E$_{\mathrm{sum}}$ [GV m$^{\mathrm{-1}}$]"                
                if fieldObject.comp2D == -2:
                    self.labelColorbar = "E$_{\mathrm{trans}}$ [GV m$^{\mathrm{-1}}$]"        
        
        
    def plotFieldLO(self, fieldObject, axis):
        """plot longitudinal lineout of given fieldDump"""
        if self.export:
            self.exportData(fieldObject.getFieldLO(), fieldObject.name + "_fieldLO")
            if self.dumpNote:
                print "          Field LO for " + fieldObject.name + " dumped"
        if self.labelYFieldLO == "":
            if fieldObject.fieldLOComp == 0:
                self.labelYFieldLO = "E$_{\mathrm{x}}$ [GV m$^{\mathrm{-1}}$]"
            elif fieldObject.fieldLOComp == 1:
                self.labelYFieldLO = "E$_{\mathrm{y}}$ [GV m$^{\mathrm{-1}}$]"
            elif fieldObject.fieldLOComp == 2:
                self.labelYFieldLO = "E$_{\mathrm{z}}$ [GV m$^{\mathrm{-1}}$]"
        xAxis = fieldObject.xAxis
        axis.plot(xAxis, fieldObject.getFieldLO(), '-', color = fieldObject.fieldLOColor, linewidth = fieldObject.fieldLOWidth)

        if fieldObject.fieldType is not "electric":
            self.labelYFieldLO = fieldObject.getName() + " " + str(fieldObject.fieldLOComp)
        return [np.min(fieldObject.xAxis), np.max(fieldObject.xAxis)]    
        
    
    
    def plotFieldGradLO(self, fieldObject, axis):
        """plot longitudinal gradient lineout of given fieldDump"""
        if self.export:
            self.exportData(fieldObject.getFieldGradLO(), fieldObject.name + "_fieldGradLO")
            if self.dumpNote:
                print "          Field gradient LO for " + fieldObject.name + " dumped"
        if self.labelYFieldGradLO == "":
            if fieldObject.fieldLOComp == 0:
                self.labelYFieldGradLO = "$\partial$E$_{\mathrm{x}}$ [GV m$^{\mathrm{-2}}$]"
            elif fieldObject.fieldLOComp == 1:
                self.labelYFieldGradLO = "$\partial$E$_{\mathrm{y}}$ [GV m$^{\mathrm{-2}}$]"
            elif fieldObject.fieldLOComp == 2:
                self.labelYFieldGradLO = "$\partial$E$_{\mathrm{z}}$ [GV m$^{\mathrm{-2}}$]"
        xAxis = fieldObject.xAxis
        axis.plot(xAxis, fieldObject.getFieldGradLO(), '-', color = fieldObject.fieldGradLOColor, linewidth = fieldObject.fieldGradLOWidth)
        if fieldObject.fieldType is not "electric":
            self.labelYFieldGradLO = "$\partial$" +  fieldObject.getName() + " " + str(fieldObject.fieldLOComp)
        return [np.min(fieldObject.xAxis), np.max(fieldObject.xAxis)]    
       
        
    
        
    def plotPotLO(self, fieldObject, axis):
        """plot longitudinal lineout of given fieldDump"""
        if self.export:
            self.exportData(fieldObject.getPotLO(), fieldObject.name +"_potLO")
            if self.dumpNote:
                print "          Potential LO for " + fieldObject.name + " dumped"

#        xAxis = (np.linspace(0, len(fieldObject.potLO)-1, len(fieldObject.potLO), endpoint = True)) * fieldObject.cellSizeX
        xAxis = fieldObject.xAxis
        axis.plot(xAxis, fieldObject.getPotLO(), '-', color = fieldObject.potLOColor, linewidth = fieldObject.potLOWidth)
        if self.potLOShowTrappingArea:
            xAxis = fieldObject.xAxis
            crossingList =[]
            for i in range(len(fieldObject.getPotLO())-1, 0 ,-1):
                if i > 0:
                    cond1 = (fieldObject.getPotLO()[i] > -1 and fieldObject.getPotLO()[i-1] < -1)
                    cond2 = (fieldObject.getPotLO()[i] < -1 and fieldObject.getPotLO()[i-1] > -1)
                    cond3 = (i == len(fieldObject.getPotLO())-1  and fieldObject.getPotLO()[i] < -1 )
                    if  cond1 or cond2 or cond3:
                        crossingList.append(i)
                if len(crossingList) == 2:
                    break
            if len(crossingList) == 2:
                potLineX = np.array([crossingList[0]*fieldObject.cellSizeX, crossingList[1]*fieldObject.cellSizeX]) + np.min(xAxis)
                axis.plot(potLineX, (-1, -1), color = self.potLOTrappingAreaColor, alpha = 0.1 )
                condition = (xAxis >= np.min(potLineX)) & ( xAxis <= np.max(potLineX))
                axis.fill_between(xAxis, -1,  fieldObject.getPotLO(), where= condition, alpha = 0.1, color = self.potLOTrappingAreaColor)
        if fieldObject.fieldType is not "electric":
            self.labelYPotLO= fieldObject.getName() + " potential " + str(fieldObject.fieldLOComp)
        return [np.min(fieldObject.xAxis), np.max(fieldObject.xAxis)] 

                
               
                
    def plotTrapPot2D(self, fieldObject, axis):
            """ plots 2D trapping potential. 3 options: 1: contourLine, 2: constant color, 3: gradient colors"""     
            potPlane = fieldObject.getPot2D()
            
            X = fieldObject.xAxis
            Y = fieldObject.yAxis
            
            
            if fieldObject.plotPot2D == 3:
                levelVec = np.linspace(np.min(potPlane), self.pot2DMax, 1000)
                colorbar = axis.contourf(X, Y, np.transpose(potPlane),  levels = levelVec, alpha = 0.05, cmap = fieldObject.pot2DColorMap)
                for c in colorbar.collections:
                    c.set_edgecolor("face")
                    
            if fieldObject.plotPot2D < 3:

                potPlane =  ndimage.zoom(potPlane, 3)
                X = np.linspace(np.min(X), np.max(X), len(X)*3, endpoint = True)
                Y = np.linspace(np.min(Y), np.max(Y), len(Y)*3, endpoint = True)
                potPlane[np.where(potPlane > self.pot2DMax)] = 10
                potPlane[np.where(potPlane <= self.pot2DMax)] = self.pot2DMax
                if fieldObject.plotPot2D == 1:
                    colorbar = axis.contour(X, Y, np.transpose(potPlane),  levels = [self.pot2DMax, 10], antialiased = True)
                elif fieldObject.plotPot2D == 2:
                    colorbar = axis.contourf(X, Y, np.transpose(potPlane), colors = [(0.1, 0.1, 0.5, 0.15),(0.1, 0.1, 0.5, 0.0001)], antialiased = True)
                    for c in colorbar.collections:
                        c.set_edgecolor("face")

            if self.export:
                self.exportData(potPlane, fieldObject.name + "_2DPotential")
                if self.dumpNote:
                       print "          2D potential for " + fieldObject.name + " dumped"

            
 
                
                
        
    def plotParticleDump(self, dumpObject, axis):
        """plots projection of given ParticletronDump """
        yVector = 0
        if dumpObject.comp2D == 0 or dumpObject.comp2D == 1:
            yVector = dumpObject.Y
        elif dumpObject.comp2D == 2:
            yVector = dumpObject.Z
            
        delStep = np.int((1/dumpObject.plotParticleRatio))
        if self.export:
            a = np.column_stack((dumpObject.X[0::delStep], yVector[0::delStep]))
            self.exportData(a, dumpObject.name)
            a = np.column_stack((dumpObject.X[0::delStep], dumpObject.Y[0::delStep], dumpObject.Z[0::delStep], dumpObject.E[0::delStep]))
            self.exportData(a, dumpObject.name + "_XYZE")
            if self.dumpNote:
                print "          Real space coordinates for " + dumpObject.name + " dumped"
        
        colormap = 0
        if isinstance (dumpObject.getColorCodeVector(), (float, int)):
                marker = dumpObject.plotMarker
                if len(marker) > 1:
                    color = marker[:-1]
#               
                if dumpObject.transparencyVector is not 0:   
                    colorvector = self.getColorVector(dumpObject, color = color)[0::delStep]
                    axis.scatter(dumpObject.X[0::delStep], yVector[0::delStep], c= colorvector,   s = dumpObject.plotMarkerSize, edgecolor='')
                else:
                    axis.scatter(dumpObject.X[0::delStep], yVector[0::delStep], c= color,   s = dumpObject.plotMarkerSize, alpha =dumpObject.transparency, edgecolor='')
                   
        else:
                delStep = np.int((1/dumpObject.plotParticleRatio))
                
                for i in range(len(self.particleColorCodeList[0])):
                    if self.particleColorCodeList[0][i].getName() == dumpObject.getName():
                        minVal = self.particleColorCodeList[3][i][0]
                        maxVal = self.particleColorCodeList[3][i][1]
                        xVec = dumpObject.X[0::delStep]
                        yVec = yVector[0::delStep]
                        colCodeVec = dumpObject.getColorCodeVector()[0::delStep]
                        if self.export:
                #            a = np.column_stack((dumpObject.X, yVector))
                            self.exportData(colCodeVec, dumpObject.name + "_colCodeVec")
                            if self.dumpNote:
                                print "          colCodeVec for " + dumpObject.name + " dumped"
                        
                        # plot data with running transparency
                        if dumpObject.transparencyVector is not 0:   
                            colorVec = self.getColorVector(dumpObject, minColorMap = minVal, maxColorMap = maxVal)[0::delStep]
                            colormap = axis.scatter(xVec, yVec, c = colorVec, s = dumpObject.plotMarkerSize, edgecolor="")
                            if self.export:
                                self.exportData(colorVec, dumpObject.name + "_colormap")
                                if self.dumpNote:
                                    print "          colormap for " + dumpObject.name + " dumped"
                        
                        # normal scatter plot without transparancy vector
                        else: 
                             colormap = axis.scatter(xVec, yVec, c = colCodeVec, cmap = dumpObject.colorMap , s = dumpObject.plotMarkerSize, vmin = minVal , vmax = maxVal, alpha = dumpObject.transparency, edgecolor="")
                        

                        if self.particleColorCodeList[4][i] == 1:
                            
                            self.drawParticleColorMap(self.particleColorCodeList[0][i], self.particleColorCodeList[2][i],  colormap, self.particleColorCodeList[3][i])
   
    
    
    
    def getColorVector(self, dumpObject, **kwargs):
        """return color and transparency vector as RGBa matrix"""
        minColorMap = 0
        maxColorMap = 0
        color       = 0
        for key in kwargs:
            if key == "minColorMap":
                minColorMap = kwargs[key]
            if key == "maxColorMap":
                maxColorMap = kwargs[key]
            if key == "color":
                color = kwargs[key]
        
        transVec = dumpObject.getTransparencyVector()
        colorCodeVec = dumpObject.getColorCodeVector()
        if dumpObject.colorBarNorm == 0:
            if np.min(transVec) < np.max(transVec):
                dumpObject.colorBarNorm = [np.min(transVec), np.max(transVec)]
            else:
                dumpObject.colorBarNorm = [0.,1.]
        
        
        if colorCodeVec is not 0:
            cmap = cm.get_cmap(dumpObject.colorMap, 100)         
            cmap_vals = cmap(np.arange(cmap.N)) 
        
        
        outputColor = 0
        
        for i in range(len(dumpObject.X)):
            
            if colorCodeVec is not 0 :
                currentColor = cmap_vals[int(cmap.N*(colorCodeVec[i] -  minColorMap)/ ( maxColorMap - minColorMap))-1]
            else:
                RGBA = matplotlib.colors.colorConverter.to_rgba(color)                
                currentColor = [RGBA[0], RGBA[1], RGBA[2], 1.]
                
            
            if transVec[i] < np.min(dumpObject.colorBarNorm):
                currentColor[3] = dumpObject.reverseTransparency - 0.
            elif transVec[i] > np.max(dumpObject.colorBarNorm):
                if dumpObject.reverseTransparency == 1:
                    currentColor[3] = 0.
                else:
                    currentColor[3] = 1.
            else:
                if dumpObject.reverseTransparency == 1:
                    currentColor[3] =  1 - (transVec[i] -  np.min(dumpObject.colorBarNorm))/ ( np.max(dumpObject.colorBarNorm) - np.min(dumpObject.colorBarNorm))
                elif dumpObject.reverseTransparency == 0:
                    currentColor[3] =  (transVec[i] -  np.min(dumpObject.colorBarNorm))/ ( np.max(dumpObject.colorBarNorm) - np.min(dumpObject.colorBarNorm))
            if i == 0:
                outputColor = currentColor
            else:
                outputColor = np.vstack((outputColor, currentColor))
            
        return outputColor
                     
        
            
    def drawParticleColorMap(self, dump, colbarAxis,  colormap, colBarLimits):
            if dump.colorCodeComp == 1 or dump.colorCodeComp.lower() == "x" :
                self.labelParticleColorbar = "x [$\mu$m]"
            if dump.colorCodeComp == 2 or dump.colorCodeComp.lower() == "y" :
                self.labelParticleColorbar = "y [$\mu$m]"
            if dump.colorCodeComp == 3 or dump.colorCodeComp.lower() == "z" :
                self.labelParticleColorbar = "z [$\mu$m]"
            if dump.colorCodeComp == 4 or dump.colorCodeComp.lower() == "px" :
                self.labelParticleColorbar = "P$_{\mathrm{x}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 5 or dump.colorCodeComp.lower() == "py" :
                self.labelParticleColorbar = "P$_{\mathrm{y}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 6 or dump.colorCodeComp.lower() == "pz" :
                self.labelParticleColorbar = "P$_{\mathrm{z}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 7 or dump.colorCodeComp.lower() == "yp" :
                self.labelParticleColorbar = "YP [mrad]"
            if dump.colorCodeComp == 8 or dump.colorCodeComp.lower() == "zp" :
                self.labelParticleColorbar = "ZP [mrad]"
            if dump.colorCodeComp == 9 or dump.colorCodeComp.lower() == "e" :
                self.labelParticleColorbar = "W [MeV]"
            if dump.colorCodeComp == 10 or dump.colorCodeComp.lower() == "tag" :
                self.labelParticleColorbar = "Tag"
            if dump.colorCodeComp == 11 or dump.colorCodeComp.lower() == "weight" :
                self.labelParticleColorbar = "Weight"
            if dump.colorCodeComp == 12 or dump.colorCodeComp.lower() == "ex" :
                self.labelParticleColorbar = "W$_{\mathrm{x}}$ [MeV]"
            if dump.colorCodeComp == 13 or dump.colorCodeComp.lower() == "ey" :
                self.labelParticleColorbar = "W$_{\mathrm{y}}$ [MeV]"
            if dump.colorCodeComp == 14 or dump.colorCodeComp.lower() == "ez" :
                self.labelParticleColorbar = "W$_{\mathrm{z}}$ [MeV]"
            if dump.colorCodeComp == 15 or dump.colorCodeComp.lower() == "etrans" :
                self.labelParticleColorbar = "W$_{\mathrm{trans}}$ [MeV]"
                    
            ticks = np.linspace(colBarLimits[0], colBarLimits[1]*0.9, 5)
            ticks = np.round(ticks, 2)

            if dump.transparencyVector == 0: # plot data with running transparency
                cbar1 = self.figure.colorbar(colormap,cax=colbarAxis, orientation='horizontal', ticks = ticks)      
            else:
                cmap = cm.get_cmap(dump.colorMap, 100)  
                norm = matplotlib.colors.Normalize(vmin=colBarLimits[0], vmax=colBarLimits[1])
                cbar1 = matplotlib.colorbar.ColorbarBase(colbarAxis, cmap=cmap, norm=norm, orientation='horizontal', ticks = ticks)


              
            colbarAxis.text(0.43, (np.max(colbarAxis.get_ylim())-np.min(colbarAxis.get_ylim()))/3.4, self.labelParticleColorbar,   color = self.colbarLabelColor, size=20)
            cbar1.solids.set(alpha=1)
            

            ticklabels = colbarAxis.get_xticklabels()
            minVal = colBarLimits[0]
            maxVal = colBarLimits[1]
            step = (maxVal-minVal)/len(ticklabels)
            if minVal < -999.9 or maxVal > 999.9:
                colbarAxis.set_xticklabels(["{0:.1e}".format(round(minVal + i*step,2)) for i in range(len(ticklabels))])
            for label in ticklabels:
                label.set_fontsize(self.tickSize)
              
            
            
            
    def plotHistLO(self, dumpObject, axis):
           """plots current or charge histograms of Particletron beams"""
           if axis == 0:
               self.axisParticleLO = self.axisMain.twinx()
               axis = self.axisParticleLO
           xVector = 0
           yVector = 0
           if dumpObject.plotHist == 1:
               xVector, yVector = dumpObject.getCurrentHist()
           elif dumpObject.plotHist == 2:
               xVector, yVector = dumpObject.getChargeHist()
           if len(yVector) - len(xVector) == 1: # sometimes yVector is one entry longer.. in this case, add another entry in x 
               xVector = np.append(xVector, xVector[len(xVector) - 1] + (xVector[len(xVector) - 1]-xVector[len(xVector) - 2]))
           if self.export:
               if dumpObject.plotHist == 1:
                   self.exportData(yVector, dumpObject.name + "_ChargeHistLO")
                   if self.dumpNote:
                       print "          Charge HistLO for " + dumpObject.name + " dumped"
               elif dumpObject.plotHist == 2:
                   self.exportData(yVector, dumpObject.name + "_CurrentHistLO")
                   if self.dumpNote:
                       print "          Current HistLO for " + dumpObject.name + " dumped"

           axis.plot(xVector, yVector, self.histLOColor, linewidth = 1)
#           plt.figure(2)
#           plt.plot(xVector, yVector)
           axis.set_ylim(0, 1)
           axis.get_yaxis().set_ticks([])                
        
    def plotLotovFieldPot(self, fieldObject, axis2, axis3):
        """experimental plot routine. uses Lotov2004..not acurate, requires one free parameter to match VSim (LotovSlopeFactor)"""
        xAxis = fieldObject.xAxis
        xAxis = xAxis[xAxis>np.min(self.lotovRange)]
        xAxis = xAxis[xAxis<np.max(self.lotovRange)]
        field         = np.zeros(len(xAxis))
        potential     = np.zeros(len(xAxis))
        zeroCrossing = fieldObject.getFieldZeroCrossing() 
        for i in range(len(xAxis)):
            i = np.int(i)
            field[i]         = 2/self.lotovSlopeFactor * np.pi * 1.602e-19 * (self.lotovDens)**(1.5) * np.sqrt(9.109e-31*(3e8)**2/(8.85e-12))*(xAxis[i]-zeroCrossing)*1e-6*1e-9               
#            potential[i]     = 1* np.pi * 1.602e-19 * (plasmaDens)**(1.5) * np.sqrt(9.109e-31*(3e8)**2/(8.85e-12))*((xAxis[i]-zeroCrossing)*1e-6)**2              
        potential = fieldObject.getLinePotential(field, 1) #integrates field
        axis2.plot(xAxis, field,"w")           
        axis3.plot(xAxis, potential,'w')    
        





    def plotTzoufrasFieldPot(self, fieldObject, axis2, axis3):
        """experimental plot routine. uses W.Lu2006 and Tzoufras2008 and Thomas2016.. currently only for 100 um plasma wavelength and 12.8 kA driver current, no switches installed yet"""
        xAxis = fieldObject.xAxis
        xAxis2 = xAxis
#        xAxis = xAxis[xAxis>np.min(self.lotovRange)]
#        xAxis = xAxis[xAxis<np.max(self.lotovRange)]
        field         = np.zeros(len(xAxis))
        field2        = np.zeros(len(xAxis))
        rb            = np.zeros(len(xAxis))
        zeroCrossing  = self._M.offsetX + fieldObject.getFieldZeroCrossing()
        kP = 62831
        current = 11.2
        L = 2. * 2./kP*np.sqrt(2.*current/17.)*np.sqrt(2.*np.pi)*0.337989*1e6 
        for i in range(len(xAxis)):
            i = np.int(i)
            field[i] = -np.sqrt(current/17.)*np.sqrt(1.-(xAxis[i]-zeroCrossing)**2/(L)**2)*np.sqrt(1./(1.-(xAxis[i]-zeroCrossing)**2/(L)**2)**2-1)
#            field2[i] = np.sqrt(2.)*(xAxis[i]-zeroCrossing)/L+ math.pow((xAxis[i]-zeroCrossing)/L,3)/2/np.sqrt(2.) + 7 * math.pow((xAxis[i]-zeroCrossing)/L,5)/16/np.sqrt(2.)
            field2[i] = np.sqrt(2.)*(xAxis[i]-zeroCrossing)/L
            if (xAxis[i]-zeroCrossing) > 0:
                 field[i] =  field[i]*-1
            rb[i] = 2*1./kP*np.sqrt(2*current/17.)*np.sqrt(1.-(xAxis[i]-zeroCrossing)**2/(L)**2)*1e6

        field = field*(3.0e8**2*9.10938e-31*kP/1.602e-19)*1e-9
        xAxis = xAxis[np.where( field >-1e50) ]
        rb = rb[np.where( field >-1e50) ]
        field = field[np.where( field >-1e50) ]
        axis2.plot(xAxis, field,"w")       
        
        field2 = field2*(3.0e8**2*9.10938e-31*kP/1.602e-19)*1e-9
#        xAxis2 = xAxis[(xAxis> zeroCrossing - 1.1*L) & (xAxis < zeroCrossing+1.1*L)]
        
#        potential2 = fieldObject.getLinePotential(field2 ,0)
        axis2.plot(xAxis2, field2,"r")     
#        axis3.plot(xAxis2, potential2, "r")
        
        self.axisMain.plot(xAxis, rb,"y")
        








      
    def setAxisProperties(self, axis, dumpObject, switch):
        """ sets properties for 2D field, fieldLO, potLO, and ParticleLO"""
        
        if switch  == "default":
#            shiftFactor = 1.0
            color = "k"
            tickNumberYAxis = self.tickNumberYAxis
            yLabel          = self.labelYAxis
            axis.set_xlabel(self.labelXAxis,fontsize=self.labelSize)
            axis.yaxis.tick_left()
#            axis.spines['right'].set_visible(False)
            shiftYLabel = self.labelYShift
            
            
        
        
        if switch == "2Dfield":
            shiftFactor = 1.0
            color = "k"
            tickNumberYAxis = self.tickNumberYAxis
            yLabel          = self.labelYAxis
            axis.set_xlabel(self.labelXAxis,fontsize=self.labelSize)
            axis.yaxis.tick_left()
#            axis.spines['right'].set_visible(False)
            shiftYLabel = self.labelYShift
            
        elif switch == "potLO":
            shiftFactor = self.potLOAxisShiftFactor
            color       = self.potLOAxisColor
            tickNumberYAxis = self.tickNumberYPotLO
            yLabel          = self.labelYPotLO
            shiftYLabel     = self.labelYShiftPotLO

        elif switch == "fieldLO":
            shiftFactor = self.fieldLOAxisShiftFactor
            color       = self.fieldLOAxisColor
            tickNumberYAxis = self.tickNumberYFieldLO
            yLabel          = self.labelYFieldLO
            shiftYLabel     = self.labelYShiftFieldLO
            
        elif switch == "fieldGradLO":
            shiftFactor = self.fieldGradLOAxisShiftFactor
            color       = self.fieldGradLOAxisColor
            tickNumberYAxis = self.tickNumberYFieldGradLO
            yLabel          = self.labelYFieldGradLO
            shiftYLabel     = self.labelYShiftFieldGradLO
            
        elif switch == "driver" or switch == "ParticleLO":
            shiftFactor = self.histLOShiftFactor
            color       = self.histLOColor
            tickNumberYAxis = self.tickNumberYParticleLO
            yLabel          = self.labelYParticleLO
            shiftYLabel     = self.labelYShiftParticleLO
         
            
        axis.set_ylabel(yLabel,fontsize=self.labelSize, color = color)
        axis.yaxis.labelpad = shiftYLabel
        
        if switch is not "2Dfield" and switch is not "default":
            axis.yaxis.tick_right()
            axis.spines['right'].set_position(('axes', shiftFactor))
            axis.spines['right'].set_color(color)
            axis.spines['left'].set_visible(False)
            axis.tick_params(axis='y', colors=color)
        
            for tick in axis.get_yticklines():
                tick.set_color(color)
                
            
        if self.tickNumberXAxis == 0:
                axis.get_xaxis().set_ticks([])           
        else:
            axis.xaxis.set_major_locator(MaxNLocator(self.tickNumberXAxis))

        if tickNumberYAxis == 0:
            axis.get_yaxis().set_ticks([])           
        else:
            axis.yaxis.set_major_locator(MaxNLocator(tickNumberYAxis))
               
                           
        ticklabels = axis.get_xticklabels()
        for label in ticklabels:
            label.set_fontsize(self.tickSize)
            
        ticklabels = axis.get_yticklabels()
        for label in ticklabels:
            label.set_fontsize(self.tickSize)
        
        
        
    def plot2DColorbar(self, axis):
            transFigure = self.figure.transFigure.inverted()
            coord1 = transFigure.transform(axis.transData.transform([np.min(axis.get_xlim()), np.max(axis.get_ylim())*0.785]))
            coord2 = transFigure.transform(axis.transData.transform([np.max(axis.get_xlim()), np.max(axis.get_ylim())*0.785]))
            cbaxes1 = self.figure.add_axes([coord1[0], 0.95, coord2[0]-coord1[0], 0.08], transform=self.figure.transFigure) 
             
            ticks = np.linspace(np.min(self.plane), np.max(self.plane), 5)
            ticks = np.round(ticks, 2)

            cbar1 = self.figure.colorbar(self.colorbar,cax=cbaxes1,orientation='horizontal',ticks = ticks)      
#            cbar1.outline.set_visible(False)
            minVal = np.min(self.plane)
            maxVal = np.max(self.plane)
            if minVal == maxVal:
                print "     (!) Note: colorbar disabled since 2D matrix is constant"
                return
            
            cbaxes1.text(0.43, (np.max(cbaxes1.get_ylim())-np.min(cbaxes1.get_ylim()))/3.3, self.labelColorbar,   color = self.colbarLabelColor, size=20)

           
            ticklabels = cbaxes1.get_xticklabels()
            step = (maxVal-minVal)/len(ticklabels)
            
            
            if minVal < -999.9 or maxVal > 999.9:
                cbaxes1.set_xticklabels(["{0:.1e}".format(round(minVal + i*step,2)) for i in range(len(ticklabels))])
                    
                    
            for label in ticklabels:
                label.set_fontsize(self.tickSize)

            
            
    def setupParticleColorbars(self):
        manager = self._M
        for dump in manager.particleSpecies:
            if dump.loaded and dump.plot2D:
                if dump.getColorCodeComp() is not "" and len(dump.getColorCodeVector()) > 0:
                    minVal = np.min(dump.getColorCodeVector())
                    maxVal = np.max(dump.getColorCodeVector())
                    if self.particleColorCodeList == []:
                         self.particleColorCodeList = ([[dump],[dump.getColorCodeComp()],[0],[(minVal, maxVal)],[0]])
                    else:
                        self.particleColorCodeList[0].append(dump) 
                        self.particleColorCodeList[1].append(dump.getColorCodeComp()) 
                        self.particleColorCodeList[2].append(0) 
                        self.particleColorCodeList[3].append((minVal, maxVal)) 
                        self.particleColorCodeList[4].append(0) 
        if manager.beamElectrons.loaded and manager.beamElectrons.plot2D and manager.beamElectrons.getColorCodeComp() is not "":
                minVal = np.min(manager.beamElectrons.getColorCodeVector())
                maxVal = np.max(manager.beamElectrons.getColorCodeVector())
                if self.particleColorCodeList == []:
                     self.particleColorCodeList = ([[manager.beamElectrons],[manager.beamElectrons.getColorCodeComp()],[0],[(minVal, maxVal)],[0]])
                else:
                    self.particleColorCodeList[0].append(manager.beamElectrons) 
                    self.particleColorCodeList[1].append(manager.beamElectrons.getColorCodeComp()) 
                    self.particleColorCodeList[2].append(0) 
                    self.particleColorCodeList[3].append((minVal, maxVal)) 
                    self.particleColorCodeList[4].append(0) 
            
        testListDict = {}
        colBarCounter = 0 
        if len(self.particleColorCodeList) == 0:
            return

        for i in range(len(self.particleColorCodeList[1])):
          item = self.particleColorCodeList[1][i]
          try:
            testListDict[item] += 1
            #adjust min/max
            for j in range(i):
                if self.particleColorCodeList[1][j] == item: 
                    minVal = np.min((self.particleColorCodeList[3][i][0], self.particleColorCodeList[3][j][0]))
                    maxVal = np.max((self.particleColorCodeList[3][i][1], self.particleColorCodeList[3][j][1]))
                    self.particleColorCodeList[3][i] = (minVal, maxVal)                        
                    self.particleColorCodeList[3][j] = (minVal, maxVal)        
                    self.particleColorCodeList[0][i].setColorMap(self.particleColorCodeList[0][j].colorMap)
          except:
            testListDict[item] = 1
            if self.plotParticleColorbar == 1:
                self.particleColorCodeList[4][i] = 1
                transFigure = self.figure.transFigure.inverted()
                coord1 = transFigure.transform(self.axisMain.transData.transform([np.min(self.axisMain.get_xlim()), np.max(self.axisMain.get_ylim())*0.785]))
                coord2 = transFigure.transform(self.axisMain.transData.transform([np.max(self.axisMain.get_xlim()), np.max(self.axisMain.get_ylim())*0.785]))
                if self.showColBar:
                    cbaxes = self.figure.add_axes([coord1[0], 1.1 + colBarCounter*0.15, coord2[0]-coord1[0], 0.08], transform =self.figure.transFigure)                             
                else: 
                    cbaxes = self.figure.add_axes([coord1[0], 0.95 + colBarCounter*0.15, coord2[0]-coord1[0], 0.08], transform =self.figure.transFigure)                             
                    
                self.particleColorCodeList[2][i] = cbaxes
                colBarCounter = colBarCounter + 1
            
            
            
            
    def plotParticleHistory(self, particleSpecies, axis):
#        print (particleSpecies.particleTrajectories)
#        print particleSpecies.runTime
#        print particleSpecies.runTime/particleSpecies.timeStep
        tagList = []
        for tag in range(len(particleSpecies.historyTags)):
#            print tag
            if not isinstance(particleSpecies.particleTrajectories[tag], (int,float)):
                
                xVector = particleSpecies.particleTrajectories[tag][:,0] - particleSpecies.windowStart
                if particleSpecies.comp2D == 1:
                    yVector = particleSpecies.particleTrajectories[tag][:,1]
                elif particleSpecies.comp2D == 2:
                    yVector = particleSpecies.particleTrajectories[tag][:,2]
                else:
                    continue
#                print "plot tag = " +str(tag)
                stop = int(particleSpecies.runTime/particleSpecies.timeStep)
                start = np.where(xVector > self._M.offsetX)[0]
                if len(start) > 0 and len(np.where(particleSpecies.Tag == particleSpecies.historyTags[tag])[0]) > 0:
                    start = start[0] + 1 
                    if start < stop:
    #                    print start
                        xVector = xVector[start:stop]
                        yVector = yVector[start:stop]
                        for i in range(len(xVector)-1, -1, -1):
                            xVector[i] = xVector[i] + (len(xVector)-i)*particleSpecies.timeStep*3.0e8

                        yVector = yVector[xVector > self._M.offsetX]
                        xVector = xVector[xVector > self._M.offsetX]
                        if len(xVector) < 2: 
                            continue
#                        if len(xVector) >= 50:
                            
                        
                        if len(particleSpecies.historyTransMin) == 1:
                            if min(yVector) < particleSpecies.historyTransMin[0]:
                                continue                            
                        elif len(particleSpecies.historyTransMin) == 2:
                            if min(yVector) < particleSpecies.historyTransMin[0] or min(yVector) > particleSpecies.historyTransMin[1]:
                                continue
                            
                            
                        if len(particleSpecies.historyTransMax) == 1:
                            if max(yVector) > particleSpecies.historyTransMax[0]:
                                continue
                        elif len(particleSpecies.historyTransMax) == 2:
                            if max(yVector) < particleSpecies.historyTransMax[0] or max(yVector) > particleSpecies.historyTransMax[1]:
                                continue
                            
                            
                        if len(particleSpecies.historyXMin) == 1:
                            if min(xVector) < particleSpecies.historyXMin[0]:
                                continue                            
                        elif len(particleSpecies.historyXMin) == 2:
                            if min(xVector) < particleSpecies.historyXMin[0] or min(xVector) > particleSpecies.historyXMin[1]:
                                continue
                            
                            
                        if len(particleSpecies.historyXMax) == 1:
                            if max(xVector) > particleSpecies.historyXMax[0]:
                                continue
                        elif len(particleSpecies.historyXMax) == 2:
                            if max(xVector) < particleSpecies.historyXMax[0] or max(xVector) > particleSpecies.historyXMax[1]:
                                continue
                        
                        if particleSpecies.historyPrintTag:                            #print str(particleSpecies.historyTags[tag]) +  " with lenght = " + str(len(xVector))
                            tagList.append(tag)
                        color = self.colorline(xVector, yVector, axis, particleSpecies.historyColor)
                        if self.export:
                            self.exportData(np.column_stack((xVector, yVector, color)), particleSpecies.name + "_histTag_" + str(tag))
                            if self.dumpNote:
                                print "          History data for " + particleSpecies.name + " tag = " + str(tag) + " dumped"
                                
        if particleSpecies.historyPrintTag:
            tagList = np.asarray(tagList)
            if particleSpecies.historyPrintTag is not 1:
                tagList = tagList[tagList.argsort()[-particleSpecies.historyPrintTag:]]
            print ','.join(str(x) for x in tagList) 
            
    def colorline(self,x, y, axis, color):
#        z = np.linspace(0.0, 1.0, len(x)) 
        outputColor = 0
        for i in range(len(x)):
            RGBA = matplotlib.colors.colorConverter.to_rgba(color)                
            currentColor = [RGBA[0], RGBA[1], RGBA[2], 1.]
            currentColor[3] = float(i)/(len(x)-1)
            if i == 0:
                outputColor = currentColor
            else:
                outputColor = np.vstack((outputColor, currentColor))
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc = LineCollection(segments,  color = outputColor, linewidth=1)
        axis.add_collection(lc)
        return outputColor



        
    def plot2DWindow(self):
        """plots longitudinal distributions of all objects specified in main file. 
        Note: most plot properties such as colors, colorbar, markers, and plotrange can be adjusted by using small methods defined above"""
        manager = self._M
        
        #check if anything should be plottet at all
        for i in range(len(manager.fieldSpecies)):
           if (manager.fieldSpecies[i].plot2D == True or manager.fieldSpecies[i].plotFieldLO == True or manager.fieldSpecies[i].plotFieldGradLO == True or manager.fieldSpecies[i].plotPotLO == True or manager.fieldSpecies[i].plotPot2D == True) and manager.fieldSpecies[i].loaded == True:
               break
           elif i == len(manager.fieldSpecies) - 1:
               for j in range(len(manager.particleSpecies)):
                  if (manager.particleSpecies[j].plot2D == True or manager.particleSpecies[j].plotHist == True) and manager.particleSpecies[j].loaded == True:
                      break
                  elif j == len(manager.particleSpecies) - 1:
                      if (manager.getParticleDump("driver").plot2D == False and manager.getParticleDump("driver").plotHist == False) or manager.getParticleDump("driver").loaded == False:
                          return
                          
            
        self.figure = plt.figure(figsize= [5,5])
        gs = gridspec.GridSpec(1, 1)

        self.axisMain               = self.figure.add_subplot(gs[0])

        self.axisFieldLO            = 0
        self.axisFieldGradLO        = 0
        self.axisPotLO              = 0
        self.axisParticleLO             = 0

          
        xLimLocal     = []
        yLimLocal     = []
        yLimParticleLocal = []
        
        
        """       
        #######################################
        #       plot data
        #######################################    
        """

        """
        # 2D field 
        """
        for species in manager.fieldSpecies:
            if species.plot2D and species.loaded:
                self.plot2DField(species, self.axisMain)
                xLimLocal = [self._M.offsetX, np.max(species.xAxis)] 
                yLimLocal = [np.min(species.yAxis), np.max(species.yAxis)]
                
        
        """
        # 2D potential
        """
        for species in manager.fieldSpecies:
            if species.plotPot2D and species.loaded:
                self.plotTrapPot2D(species, self.axisMain)
                xLimLocal = [self._M.offsetX, np.max(species.xAxis)]        
                yLimLocal = [np.min(species.yAxis), np.max(species.yAxis)]
                

        """
        # particle dumps
        """
        
        #check for activated colorCodes
        self.setupParticleColorbars()        
        
        for numSpecies in range(len(manager.particleSpecies)):
            particleSpecies = self._M.particleSpecies[numSpecies]
                
            if particleSpecies.plot2D and particleSpecies.loaded and len(particleSpecies.X) > 10:
                if len(particleSpecies.historyTags) > 0:
                    self.plotParticleHistory(particleSpecies, self.axisMain)
                self.plotParticleDump(particleSpecies, self.axisMain)
                if len(yLimParticleLocal) == 0:
                    yLimParticleLocal = [-np.max(particleSpecies.Y)*1.1,np.max(particleSpecies.Y)*1.1] 
                else:
                    if yLimParticleLocal[0] > -np.max(particleSpecies.Y)*1.1:
                        yLimParticleLocal[0] = -np.max(particleSpecies.Y)*1.1
                    if yLimParticleLocal[1] < np.max(particleSpecies.Y)*1.1:
                        yLimParticleLocal[1] = np.max(particleSpecies.Y)*1.1
                
            if particleSpecies.plotHist and particleSpecies.loaded  and len(particleSpecies.X) > 10:
                self.plotHistLO(particleSpecies, self.axisParticleLO)
                if particleSpecies.plotHist == 2:
                   self.labelYParticleLO = "$\\rho$ [pC $\mu$m$^{\mathrm{-1}}$]"
                            
        # beamElectrons
        if manager.beamElectrons.plot2D and manager.beamElectrons.loaded and len(manager.beamElectrons.X) > 10:
            self.plotParticleDump(manager.beamElectrons, self.axisMain)
            if len(yLimParticleLocal) == 0:
                yLimParticleLocal = [-np.max(manager.beamElectrons.Y)*1.1,np.max(manager.beamElectrons.Y)*1.1] 
            else:
                if yLimParticleLocal[0] > -np.max(manager.beamElectrons.Y)*1.1:
                    yLimParticleLocal[0] = -np.max(manager.beamElectrons.Y)*1.1
                if yLimParticleLocal[1] < np.max(manager.beamElectrons.Y)*1.1:
                    yLimParticleLocal[1] = np.max(manager.beamElectrons.Y)*1.1
            
        if manager.beamElectrons.plotHist and manager.beamElectrons.loaded and len(manager.beamElectrons.X) > 10:
                self.plotHistLO(manager.beamElectrons, self.axisParticleLO)
                if manager.beamElectrons.plotHist == 2:  
                    self.labelYParticleLO = "$\\rho$ [pC $\mu$m$^{\mathrm{-1}}$]"							   
               
                
                
                
                
                
                
        # field lineout               
        for species in manager.fieldSpecies:
            if species.plotFieldLO and species.loaded:
                if self.axisMain == 0:
                    self.axisFieldLO = self.axisMain
                elif self.axisFieldLO == 0:
                    self.axisFieldLO = self.axisMain.twinx()
                xLimLocal = self.plotFieldLO(species, self.axisFieldLO)    
                
                
        # field lineout gradient               
        for species in manager.fieldSpecies:
            if species.plotFieldGradLO and species.loaded:
                if self.axisMain == 0:
                    self.axisFieldGradLO = self.axisMain
                elif self.axisFieldGradLO == 0:
                    self.axisFieldGradLO = self.axisMain.twinx()
                xLimLocal = self.plotFieldGradLO(species, self.axisFieldGradLO)    

        # field potential lineout
        for species in manager.fieldSpecies:
            if species.plotPotLO and species.loaded:
                if self.axisMain == 0:
                    self.axisPotLO = self.axisMain
                elif self.axisPotLO == 0:
                    self.axisPotLO = self.axisMain.twinx()
                xLimLocal = self.plotPotLO(species, self.axisPotLO)  
                
    

            
        if self.plotLotov and (manager.getLasersPlusPlasma().loaded or manager.getParticleMultiField().loaded):
            if self.axisPotLO == 0:
                self.axisPotLO = self.axisMain.twinx()
            if manager.getParticleMultiField().fieldMatrix is not 0:
                self.plotLotovFieldPot(manager.getParticleMultiField(), self.axisFieldLO,self.axisPotLO)
            elif manager.getLasersPlusPlasma() is not 0:
                self.plotLotovFieldPot(manager.getLasersPlusPlasma(),self.axisFieldLO, self.axisPotLO)
            else:
                print "Lotov requires a loaded field"
        
        if self.plotTzoufras and (manager.getLasersPlusPlasma().loaded or manager.getElecMultiField().loaded):
            if self.axisPotLO == 0:
                self.axisPotLO = self.axisMain.twinx()
            if manager.getElecMultiField().fieldMatrix is not 0:
                self.plotTzoufrasFieldPot(manager.getElecMultiField(), self.axisFieldLO,self.axisPotLO)
            elif manager.lasersPlusPlasma is not 0:
                self.plotTzoufrasFieldPot(manager.getLasersPlusPlasma(),self.axisFieldLO, self.axisPotLO)
            else:
                print "Tzoufras requires a loaded field"
            
            
            
        #######################################
        #       set plot limits
        #######################################     
           
            
             
        if len(self.xLim) == 0:
             self.xLim = xLimLocal
        if len(self.xLim) == 2:
            self.axisMain.set_xlim(self.xLim)
        
        if len(self.yLim) == 0:
            lim=[0,0]
            if len(yLimParticleLocal) > 0 and len(yLimLocal) > 0:
                if yLimLocal[0] < yLimParticleLocal[0]:
                    lim[0] = yLimLocal[0]
                else: 
                    lim[0] = yLimParticleLocal[0]
                if yLimLocal[1] > yLimParticleLocal[1]:
                    lim[1] = yLimLocal[1]
                else: 
                    lim[1] = yLimParticleLocal[1]
            elif len(yLimLocal) > 0:
                lim = yLimLocal
            elif len(yLimParticleLocal) > 0:
                lim = yLimParticleLocal
            self.yLim = lim 
            
        elif len(self.yLim) == 2:
            self.axisMain.set_ylim(self.yLim)
            
            
            
        if isinstance(self.axisFieldLO, int) == 0 and len(self.yLimFieldLO) == 2:
            self.axisFieldLO.set_ylim(self.yLimFieldLO)
            
        if isinstance(self.axisFieldGradLO, int) == 0 and len(self.yLimFieldGradLO) == 2:
            self.axisFieldGradLO.set_ylim(self.yLimFieldGradLO)
      
        if isinstance(self.axisPotLO, int) == 0 and len(self.yLimPotLO) == 2:
            self.axisPotLO.set_ylim(self.yLimPotLO)
        
        if isinstance(self.axisParticleLO, int) == 0 and len(self.yLimParticleLO) == 2:
            self.axisParticleLO.set_ylim(self.yLimParticleLO)




        #######################################
        #       make axes nice
        #######################################     
        if self.axisMain is not 0:
            
            self.setAxisProperties(self.axisMain, 0, "default")
            
            for species in manager.fieldSpecies:
                if species.plot2D and species.loaded:
                    self.setAxisProperties(self.axisMain, species, "2Dfield")


        if self.axisFieldLO is not 0:
            for species in manager.fieldSpecies:
                if species.plotFieldLO and species.loaded:
                    self.setAxisProperties(self.axisFieldLO, species, "fieldLO")

     
        if self.axisFieldGradLO is not 0:
            if self.axisFieldLO is not 0 and self.fieldGradLOAxisShiftFactor ==1 :
                self.fieldGradLOAxisShiftFactor = 1.15
            for species in manager.fieldSpecies:
                if species.plotFieldGradLO and species.loaded:
                    self.setAxisProperties(self.axisFieldGradLO, species, "fieldGradLO")
             
        
        if self.axisPotLO is not 0:
            if self.axisFieldLO is not 0 and self.axisFieldGradLO is not 0 and self.potLOAxisShiftFactor ==1:
                self.potLOAxisShiftFactor = 1.3
            elif ((self.axisFieldLO is not 0 and self.axisFieldGradLO == 0) or (self.axisFieldLO== 0 and self.axisFieldGradLO is not 0)) and self.potLOAxisShiftFactor ==1:
                self.potLOAxisShiftFactor = 1.15
            for species in manager.fieldSpecies:
                if species.plotPotLO and species.loaded:
                    self.setAxisProperties(self.axisPotLO, species, "potLO")

        
        if self.axisParticleLO is not 0:
            if self.axisFieldLO is not 0 and self.axisFieldGradLO is not 0 and self.axisPotLO is not 0 and self.histLOShiftFactor ==1:
                self.histLOShiftFactor = 1.45
            elif ((self.axisFieldLO == 0 and self.axisFieldGradLO is not 0 and self.axisPotLO is not 0) or (self.axisFieldLO is not 0 and self.axisFieldGradLO == 0 and self.axisPotLO is not 0) or (self.axisFieldLO is not 0 and self.axisFieldGradLO is not 0 and self.axisPotLO == 0)) and self.histLOShiftFactor ==1:
                self.histLOShiftFactor = 1.3
            elif ((self.axisFieldLO == 0 and self.axisFieldGradLO == 0 and self.axisPotLO is not 0) or (self.axisFieldLO is not 0 and self.axisFieldGradLO == 0 and self.axisPotLO == 0) or (self.axisFieldLO == 0 and self.axisFieldGradLO is not 0 and self.axisPotLO == 0)) and self.histLOShiftFactor ==1:
                self.histLOShiftFactor = 1.15
                
            
            if manager.beamElectrons.loaded and manager.beamElectrons.plotHist  and len(manager.beamElectrons.X) > 10:
                self.setAxisProperties(self.axisParticleLO, manager.beamElectrons, "driver")
                        
            for numSpecies in range(len(manager.particleSpecies)):
                particleSpecies = self._M.particleSpecies[numSpecies]
                if particleSpecies.plotHist and particleSpecies.loaded  and len(particleSpecies.X) > 10:
                    self.setAxisProperties(self.axisParticleLO, particleSpecies, "ParticleLO")
                
     

               
        plt.draw()     
        if self.showColBar:
            for species in manager.fieldSpecies:
                if species.plot2D and species.loaded:
                    self.plot2DColorbar(self.axisMain)    
                    break
        
        
        if self.plotPropLen:
            currentX = 0
            runTime  = 0
            if manager.beamElectrons.loaded:
                currentX = manager.beamElectrons.getWindowEnd()/1000
                runTime = manager.beamElectrons.runTime*1e12     
            elif len(manager.fieldSpecies) > 0:
                for species in manager.fieldSpecies:
                    if species.loaded:
                        currentX = species.getWindowEnd()/1000
                        runTime = species.runTime*1e12 
            if runTime == 0 and len(manager.particleSpecies)>0:
                for particles in manager.particleSpecies:
                    if particles.loaded:
                        currentX = particles.getWindowEnd()/1000
                        runTime = particles.runTime*1e12   
                        break
                                                
            if self.plotPropLen == 1:
                self.axisMain.text((np.max(self.axisMain.get_xlim())- np.min(self.axisMain.get_xlim()))*self.propLenPosX + np.min(self.axisMain.get_xlim()), (np.min(self.axisMain.get_ylim()))*self.propLenPosY, str("{0:.1f}".format(np.round(currentX,2))) + " mm", color= self.propLenColor, size=15)
            elif self.plotPropLen == 2:
#                print runTime
                if runTime == 0:
                    runTime = currentX/3e8/1000*1e12
		
                self.axisMain.text((np.max(self.axisMain.get_xlim())- np.min(self.axisMain.get_xlim()))*self.propLenPosX + np.min(self.axisMain.get_xlim()), (np.min(self.axisMain.get_ylim()))*self.propLenPosY, "t $_{\mathrm{sim}}$ = " + str("{0:.1f}".format(np.round(runTime,2))) + " ps", color= self.propLenColor, size=15)
        
        
        if len(self.figSize) == 0:
            self.figure.set_size_inches(7.5, 7.5*(   np.max(self.axisMain.get_ylim()) - np.min(self.axisMain.get_ylim())   )/(  (np.max(self.axisMain.get_xlim()) - np.min(self.axisMain.get_xlim()))   ) )
        else:
            self.figure.set_size_inches( self.figSize )
            
        if self.saveFigures == 1:
                if self.outFileFormat == "pdf":
                      plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_plot2D.pdf",format = "pdf", bbox_inches='tight')
                elif self.outFileFormat == "png" or self.outFileFormat == "jpg":
                      plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_plot2D." +self.outFileFormat, dpi=self.DPI, bbox_inches='tight')         
                elif self.outFileFormat == "svg":
                      plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_plot2D.svg",format = "svg", dpi=self.DPI, bbox_inches='tight')         
                elif self.outFileFormat == "eps":
                      plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_plot2D.eps",format = "eps", dpi=self.DPI, bbox_inches='tight')         
               
        plt.show()
        self.figure.clf()
        self.particleColorCodeList = []
        
        
        
        
        
        
        
        
        
        
        
        
        
        