# -*- coding: utf-8 -*-
"""
Created on Wed Feb 03 18:23:10 2016

@author: P. Scherkl, O. Karger
"""
import sys
import matplotlib
#matplotlib.pyplot.switch_backend('agg')
matplotlib.use('agg')

#####################
# Uncomment for cluster use and adapt path
#import imp, sys    
#sys.path.append('//lustre//project//k1191//picViz//')  # uncomment for SHAHEEN
sys.path.append('//scratch2//scratchdirs//hidding//picViz//') # uncomment for NERSC
#sys.path.append('//work//hhh36//hhh362//picViz//') # uncomment for JURECA




from dataManager import *
from dumpSummarizer import *
from Coordinator import *



from dump import *
from elecDump import *
from fieldDump import *


"""
#--------------------------------------------------------
#       Define .h5 files to be post-processed: specify path, prefix, dumpNumbers, and witness species
#--------------------------------------------------------
"""
#pathToData     = u"//scratch2//scratchdirs//hidding//e210//PseudoTorch//CylindricTorch//20180301_H_HePlus_Torch_avADK//20um_0.5mJ//"
#prefix         = "20180301_H_HePlus_Torch_avADK"
pathToData     = "/scratch2/scratchdirs/hidding/e210/20180303_Trojan_DumpPer/1mJ/1mJ_0.2ps/"
prefix         = "20180303_TrojanV4_1_DumpPer_1mJ_02ps"

#pathToData 		= u"//scratch2//scratchdirs//hidding//e210//20180303_Trojan_DumpPer//5mJ//5mJ_0.2ps//"
#prefix 			= "20180303_TrojanV4_1_DumpPer_5mJ_02ps"

#particleDumpList = ["ChannelElectrons", "H_TORCHElectrons", "He_TORCHElectrons", "HeElectrons"]
particleDumpList = ["ChannelElectrons", "HElectrons", "HeElectrons"]

#particleDumpList    = ["TORCHElectrons", "TORCHIons"]
fieldDumpList    = ["ElecMultiField"]
#fieldDumpList    = ["ElecMultiField", "lasersPlusPlasma", "SumRhoJ", "smRhoJ", "MagMultiField","auxRhoJ"]

#load and plot following dump numbers:
#dumpNumbers    = np.arange(2, 218 + 1)
#dumpNumbers    = np.arange(430, 440 + 1, 10) # NOTE: if you only want every n-th dump loaded use this line
dumpNumbers  = [258] # use this for single dumps
#

'''         HISTORY SETTINGS FOR PSEUDO TORCH RESTARTS              '''
'''         CHANGE SETTIGS IN PLOTTER2D LINE 1051 and DATAMANAGER 244 '''

Comp2D       = 2 #1:xy, 2:xz 															   

coordinator = Coordinator(pathToData, dumpNumbers, prefix, particleDumpList, fieldDumpList)
#coordinator._Manager.setOffsetX(-120.38200692) # moves xAxis in all plots by given number [um]


coordinator.plotSummarizedDumps = 0


coordinator._Manager.setOutpath(pathToData)


"""
#--------------------------------------------------------
#       Plot switches for driver electron beams
0#--------------------------------------------------------
"""
coordinator.getParticleDump("driver").plot2D    = 0
coordinator.getParticleDump("driver").plotHist  = 0 #"1" current, "2" line charge density
coordinator.getParticleDump("driver").plot3D    = 0
coordinator.getParticleDump("driver").comp2D    = Comp2D #1 == y, 2 == z component

coordinator.getParticleDump("driver").setPlotParticleRatio(1)
															   
coordinator.getParticleDump("driver").setTransparency(0.1)
														 
														   
coordinator.getParticleDump("driver").setPlotMarkerSize(0.2)
coordinator.getParticleDump("driver").setPlotMarker("k.")

#coordinator.plotPhaseSpaces("driver","x")
"""
#--------------------------------------------------------
#       Plot switches for witness electron beams
#--------------------------------------------------------
"""

cmap                = "inferno"
ratio               = 1.0# 1.0
transparency        = 0.1#0.1
msize               = 0.5							   

coordinator.getParticleDump(0).plot2D    = 0

coordinator.getParticleDump(0).plot3D    = 0

coordinator.getParticleDump(0).setPlotMarkerSize(msize)
coordinator.getParticleDump(0).setColorMap(cmap)
coordinator.getParticleDump(0).setPlotParticleRatio(ratio)
coordinator.getParticleDump(0).comp2D    = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(0).setColorCodeComp("E")
#coordinator.getParticleDump(0).plotParticleTrajectories( np.ogrid[0:433200] )
#coordinator.getParticleDump(0).setPlotMarker("blue.")

#coordinator.getParticleDump(0).plotHist  = 0  #"1" current, "2" line charge density
#coordinator.getParticleDump(0).setTransparency(transparency)

#coordinator.getParticleDump(0).setHistBinSize(0.01)


#coordinator.getParticleDump(0).setHistSmooth(1)

#coordinator.getParticleDump(0).setTransparencyVec("E")
#coordinator.getParticleDump(0).setTransparencyVec("E")

##

#coordinator.cutPhaseSpace(0,"E", 5 ,2e4)
#coordinator.cutPhaseSpace(0,"x", 30 , 80)
#coordinator.cutPhaseSpace(0,"y", 1.8, 2.2)
##coordinator.cutPhaseSpace(0,"z", -2 ,2)


#coordinator.plotPhaseSpaces(0,"x")
#coordinator.plotPhaseSpaces(0,"y")
#coordinator.plotPhaseSpaces(0,"y")
#coordinator.plotPhaseSpaces(0,"t")
#

coordinator.getParticleDump(1).plot2D    = 0
#coordinator.getParticleDump(1).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(1).plot3D    = 0

coordinator.getParticleDump(1).setPlotMarkerSize(msize)
coordinator.getParticleDump(1).setColorMap(cmap)
coordinator.getParticleDump(1).setPlotParticleRatio(ratio)
coordinator.getParticleDump(1).comp2D     = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(1).setColorCodeComp("E")
coordinator.getParticleDump(1).setPlotMarker("m.")
#loadtags = np.array(np.loadtxt(r'D:\Pseudotorch\170407_2nd40umSubseqDehosedAxilensCylPseudoTorchRestart\20um_5mJ_2ndGenTracking\20um_5mJ_Torch_SloMo_HistPer_5\ind_symmetric.txt'), np.int)
#coordinator.getParticleDump(1).plotParticleTrajectories( loadtags )


#coordinator.getParticleDump(1).setTransparency(transparency)
#coordinator.getParticleDump(1).setHistBinSize(0.001)
#coordinator.getParticleDump(1).setHistSmooth(1)
#coordinator.getParticleDump(1).setTransparencyVec("e",  norm = (580,600), reverse = False)

##

coordinator.getParticleDump(2).plot2D    = 0
#coordinator.getParticleDump(2).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(2).plot3D    = 0

coordinator.getParticleDump(2).setPlotMarkerSize(msize)
coordinator.getParticleDump(2).setColorMap(cmap)
coordinator.getParticleDump(2).setPlotParticleRatio(ratio)
coordinator.getParticleDump(2).comp2D     = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(2).setColorCodeComp("E")
coordinator.getParticleDump(2).setPlotMarker("m.")



coordinator.getParticleDump(3).plot2D    = 0
#coordinator.getParticleDump(2).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(3).plot3D    = 0

coordinator.getParticleDump(3).setPlotMarkerSize(msize)
coordinator.getParticleDump(3).setColorMap(cmap)
coordinator.getParticleDump(3).setPlotParticleRatio(ratio)
coordinator.getParticleDump(3).comp2D     = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(3).setColorCodeComp("E")
coordinator.getParticleDump(3).setPlotMarker("m.")


coordinator.getParticleDump(4).plot2D    = 0
#coordinator.getParticleDump(2).plotHist  = 0  #"1" current, "2" line charge density
coordinator.getParticleDump(4).plot3D    = 0

coordinator.getParticleDump(4).setPlotMarkerSize(msize)
coordinator.getParticleDump(4).setColorMap(cmap)
coordinator.getParticleDump(4).setPlotParticleRatio(ratio)
coordinator.getParticleDump(4).comp2D     = Comp2D #1 == y, 2 == z component
coordinator.getParticleDump(4).setColorCodeComp("E")
coordinator.getParticleDump(4).setPlotMarker("m.")
#coordinator.cutPhaseSpace(1,"E",400,1000)
#coordinator.cutPhaseSpace(1,"x", 76 , 82)
#####coordinator.cutPhaseSpace(1,"yp",-100, 100)
###
#coordinator.plotPhaseSpaces(1,"x")
#coordinator.plotPhaseSpaces(1,"y")
##coordinator.plotPhaseSpaces(1,"y")
##coordinator.plotPhaseSpaces(1,"t")
#coordinator.plotPhaseSpaces((0,1),"x")
#
#coordinator.cutPhaseSpace(2,"E",0,350)
#coordinator.cutPhaseSpace(2,"x", 70 , 82)
#
#coordinator.plotPhaseSpaces((0,1),"x")
#coordinator.plotPhaseSpaces((0),"y")


coordinator.cutPhaseSpace(0,"E",5.0,10000.0)
coordinator.cutPhaseSpace(1,"E",5.0,10000.0)
coordinator.cutPhaseSpace(2,"E",5.0,10000.0)
coordinator.cutPhaseSpace(3,"E",5.0,10000.0)
#coordinator.cutPhaseSpace(4,"E",5.0,10000.0)

coordinator.plotPhaseSpaces((0,1,2,3),"x")
coordinator.plotPhaseSpaces((0,1,2,3),"y")
coordinator.plotPhaseSpaces((0,1,2,3),"z")


				
coordinator.cutPhaseSpace(0,"x", 0.0 , 100.0)
coordinator.cutPhaseSpace(1,"x", 0.0 , 100.0)
coordinator.cutPhaseSpace(2,"x", 0.0 , 100.0)
coordinator.cutPhaseSpace(3,"x", 0.0 , 100.0)
#coordinator.cutPhaseSpace(4,"x", 0.0 , 100.0)										


		   
"""
#--------------------------------------------------------
#       Plot switches for field dumps
#--------------------------------------------------------
"""
#######  2D switches   ###############################

coordinator.getElecMultiField().plot2D          = 0
coordinator.getElecMultiField().comp2D          = 0 # 0,1,2 for x,y, z and -1 for geometric sum
#coordinator.getElecMultiField().plane2D         = ["x", "y",0]
coordinator.getElecMultiField().setColorbar2D("RdBu_r")
if Comp2D == 1:
    coordinator.getElecMultiField().plane2D         = ["x", "y",0]
elif Comp2D == 2:
    coordinator.getElecMultiField().plane2D         = ["x", 0, "z"]
coordinator.getElecMultiField().plotFieldLO     = 0
coordinator.getElecMultiField().fieldLOComp     = 0
coordinator.getElecMultiField().fieldLORadius   = 2
coordinator.getElecMultiField().fieldLOShift    = [0, 0] #[um]
coordinator.getElecMultiField().plotFieldGradLO = 0
coordinator.getElecMultiField().plotPotLO       = 0
coordinator.getElecMultiField().plotPot2D       = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
#coordinator.getElecMultiField().setPot2DColorMap("magma")
coordinator.getElecMultiField().setPotLOColor("b")
coordinator.getElecMultiField().plot3D          = 0


coordinator.getSumRhoJ().plot2D                 = 0
coordinator.getSumRhoJ().comp2D                 = 0 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getSumRhoJ().setColorbar2D("gray")
coordinator.getSumRhoJ().plotFieldLO            = 0
coordinator.getSumRhoJ().fieldLOComp            = 0
coordinator.getSumRhoJ().fieldLORadius          = 1
coordinator.getSumRhoJ().fieldLOShift           = [0, 0] #[um]
coordinator.getSumRhoJ().plotFieldGradLO        = 0
coordinator.getSumRhoJ().plotPotLO              = 0
coordinator.getSumRhoJ().plotPot2D              = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getSumRhoJ().setPot2DColorMap("gray")


coordinator.getLasersPlusPlasma().plot2D           = 0
coordinator.getLasersPlusPlasma().comp2D           = 1 # 0,1,2 for x,yz and -1 for geometric sum
coordinator.getLasersPlusPlasma().setColorbar2D("coolwarm")
coordinator.getLasersPlusPlasma().plotFieldLO      = 0
coordinator.getLasersPlusPlasma().fieldLOComp      = 0
coordinator.getLasersPlusPlasma().fieldLORadius    = 1
coordinator.getLasersPlusPlasma().fieldLOShift     = [0, 0] #[um]
coordinator.getLasersPlusPlasma().plotFieldGradLO  = 0
coordinator.getLasersPlusPlasma().plotPotLO        = 0
coordinator.getLasersPlusPlasma().plotPot2D        = 0 # 0: off, 1: contourLine, 2: constant color, 3: gradient colors
coordinator.getLasersPlusPlasma().setPot2DColorMap("gray")

coordinator.getLasersPlusPlasma().plot3D           = 0



#coordinator.getFieldDump(2).plot2D           = 1
#coordinator.getFieldDump(2).comp2D           = 1 # 0,1,2 for x,yz and -1 for geometric sum
#coordinator.getFieldDump(2).fieldLOComp      = 1 
#coordinator.getFieldDump(2).plotFieldLO      = 1 
#coordinator.getFieldDump(2).plotPotLO        = 1 
#coordinator.getFieldDump(2).plotFieldGradLO  = 1 



"""
#--------------------------------------------------------
#       2D plotting options
#--------------------------------------------------------
"""

coordinator._Plotter2D.saveFigures            = 0
coordinator._Plotter2D.export                 = 0  
coordinator._Plotter2D.setDumpNote(0)

coordinator._Plotter2D.setPlotPropLen(1)
coordinator._Plotter2D.setPropLenPosX(0.66)
coordinator._Plotter2D.setPropLenPosY(0.9)
coordinator._Plotter2D.setPropLenColor("k")
coordinator._Plotter2D.setDPI(350)           

coordinator._Plotter2D.setPlotParticleColorbar(1)												 
coordinator._Plotter2D.setColbarLabelColor("k")
coordinator._Plotter2D.setColbarTickLeftColor("w")
coordinator._Plotter2D.setColbarTickRightColor("k")
coordinator._Plotter2D.setColbarZero(0)


coordinator._Plotter2D.setPlotParticleColorbar(1)


#######  Figure limits   ###############################
#coordinator._Plotter2D.setXLim(78,84)
#coordinator._Plotter2D.setYLim(0,5)
#       TRACKING STYLE
coordinator._Plotter2D.setXLim(180-180,450-180)
coordinator._Plotter2D.setYLim(-75,75)
coordinator._Plotter2D.setYLimFieldLO(-70,60)
coordinator._Plotter2D.setYLimFieldGradLO(-20,20)
coordinator._Plotter2D.setYLimPotLO(-3.5,3)
coordinator._Plotter2D.setYLimParticleLO(0,20)
coordinator._Plotter2D.setFigSize(8*1.5,3.8*1.5)
coordinator._Manager.setOffsetX(-180)		

 
#######  2D field style options ########################
col2                = "c"
coordinator._Plotter2D.setFieldLOAxisColor(col2)
coordinator.getElecMultiField().setFieldLOColor(col2)	
coordinator.getElecMultiField().setFieldLOWidth(1.0)

coordinator._Plotter2D.setShowColBar(1)

coordinator._Plotter2D.plotLotov        = 0
coordinator._Plotter2D.lotovRange       = [50, 175] 
coordinator._Plotter2D.lotovDens        = 6.0e22
coordinator._Plotter2D.lotovSlopeFactor = 3.5 
coordinator._Plotter2D.plotTzoufras     = 0

#######  Potential style options #######################
col                 = "fuchsia"
coordinator._Plotter2D.setPotLOAxisColor(col)
coordinator._Plotter2D.plotPotLOTrappingArea(1)
coordinator._Plotter2D.setPotLOAxisColor(col)
coordinator.getElecMultiField().setPotLOColor(col)											 
coordinator._Plotter2D.setPotLOTrappingAreaColor(col)
coordinator._Plotter2D.setPot2DMax(-1)
coordinator.getElecMultiField().setPotLOWidth(1.0)												  







"""
#--------------------------------------------------------
#       Phase  space plotting style options
#--------------------------------------------------------
"""


coordinator._PlotterPhaseSpace.saveFigures = 1
coordinator._PlotterPhaseSpace.export      = 0  
coordinator._PlotterPhaseSpace.setDumpNote(1)
coordinator._PlotterPhaseSpace.setDPI(500)

coordinator._PlotterPhaseSpace.setFigSize(7, 6)
coordinator._PlotterPhaseSpace.setPlotPropLen(1)
coordinator._Manager.setOffsetX(-180)

#coordinator._PlotterPhaseSpace.setXLim(-200,300)
#coordinator._PlotterPhaseSpace.setYLim(0,4)
#coordinator._PlotterPhaseSpace.setLabelX("adasd")

coordinator._PlotterPhaseSpace.setPlotParticleColorbar(1)





"""
#--------------------------------------------------------
#       define parameters that should be analyzed/calculated (print to terminal and saved in summarizer output files!)
#--------------------------------------------------------
"""


coordinator.getParticleDump(0).analyzeDumps(1)
coordinator.getParticleDump(0).saveCharge                  = 1
coordinator.getParticleDump(0).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(0).saveEnergyMean              = 1
coordinator.getParticleDump(0).saveEnergyMax               = 1
coordinator.getParticleDump(0).saveGammaMean               = 1
coordinator.getParticleDump(0).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(0).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(0).saveEnergyDevRMS            = 1
coordinator.getParticleDump(0).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(0).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(0).savePositionMean            = 1
coordinator.getParticleDump(0).savePositionMeanWeighted    = 0
coordinator.getParticleDump(0).savePositionMeanLabframe    = 1
coordinator.getParticleDump(0).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(0).saveBunchLengthRMS          = 1
coordinator.getParticleDump(0).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(0).saveBunchLengthMax          = 1
coordinator.getParticleDump(0).saveWidthRMS                = 1
coordinator.getParticleDump(0).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(0).saveWidthMax                = 1
coordinator.getParticleDump(0).saveDivergenceRMS           = 1
coordinator.getParticleDump(0).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(0).saveEmittance               = 1
coordinator.getParticleDump(0).saveAccField                = 0
coordinator.getParticleDump(0).saveZeroCrossing            = 0
coordinator.getParticleDump(0).saveCurrentMax              = 0
coordinator.getParticleDump(0).saveBrightness5D            = 0
coordinator.getParticleDump(0).saveAccFieldGradient        = 0
                       
                       
coordinator.getParticleDump(1).analyzeDumps(1)
coordinator.getParticleDump(1).saveCharge                  = 1
coordinator.getParticleDump(1).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(1).saveEnergyMean              = 1
coordinator.getParticleDump(1).saveEnergyMax               = 0
coordinator.getParticleDump(1).saveGammaMean               = 0
coordinator.getParticleDump(1).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(1).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(1).saveEnergyDevRMS            = 1
coordinator.getParticleDump(1).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(1).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(1).savePositionMean            = 1
coordinator.getParticleDump(1).savePositionMeanWeighted    = 0
coordinator.getParticleDump(1).savePositionMeanLabframe    = 0
coordinator.getParticleDump(1).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(1).saveBunchLengthRMS          = 0
coordinator.getParticleDump(1).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(1).saveBunchLengthMax          = 1
coordinator.getParticleDump(1).saveWidthRMS                = 1
coordinator.getParticleDump(1).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(1).saveWidthMax                = 0
coordinator.getParticleDump(1).saveDivergenceRMS           = 1
coordinator.getParticleDump(1).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(1).saveEmittance               = 1
coordinator.getParticleDump(1).saveAccField                = 1
coordinator.getParticleDump(1).saveZeroCrossing            = 1
coordinator.getParticleDump(1).saveCurrentMax              = 1
coordinator.getParticleDump(1).saveBrightness5D            = 1



coordinator.getParticleDump(2).analyzeDumps(1)
coordinator.getParticleDump(2).saveCharge                  = 1
coordinator.getParticleDump(2).saveEnergyMeanWeighted      = 0
coordinator.getParticleDump(2).saveEnergyMean              = 1
coordinator.getParticleDump(2).saveEnergyMax               = 0
coordinator.getParticleDump(2).saveGammaMean               = 0
coordinator.getParticleDump(2).saveGammaMeanWeighted       = 0
coordinator.getParticleDump(2).saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump(2).saveEnergyDevRMS            = 1
coordinator.getParticleDump(2).saveEnergySpreadRMS         = 1
coordinator.getParticleDump(2).saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump(2).savePositionMean            = 1
coordinator.getParticleDump(2).savePositionMeanWeighted    = 0
coordinator.getParticleDump(2).savePositionMeanLabframe    = 0
coordinator.getParticleDump(2).savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump(2).saveBunchLengthRMS          = 0
coordinator.getParticleDump(2).saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump(2).saveBunchLengthMax          = 1
coordinator.getParticleDump(2).saveWidthRMS                = 1
coordinator.getParticleDump(2).saveWidthRMSWeighted        = 0
coordinator.getParticleDump(2).saveWidthMax                = 0
coordinator.getParticleDump(2).saveDivergenceRMS           = 1
coordinator.getParticleDump(2).saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump(2).saveEmittance               = 1
coordinator.getParticleDump(2).saveAccField                = 1
coordinator.getParticleDump(2).saveZeroCrossing            = 1
coordinator.getParticleDump(2).saveCurrentMax              = 1
coordinator.getParticleDump(2).saveBrightness5D            = 1


coordinator.getParticleDump("driver").analyzeDumps(1) 
coordinator.getParticleDump("driver").saveCharge                  = 1
coordinator.getParticleDump("driver").saveEnergyMeanWeighted      = 0
coordinator.getParticleDump("driver").saveEnergyMean              = 1
coordinator.getParticleDump("driver").saveEnergyMax               = 0
coordinator.getParticleDump("driver").saveGammaMean               = 1
coordinator.getParticleDump("driver").saveGammaMeanWeighted       = 0
coordinator.getParticleDump("driver").saveEnergyDevRMSWeighted    = 0
coordinator.getParticleDump("driver").saveEnergyDevRMS            = 1
coordinator.getParticleDump("driver").saveEnergySpreadRMS         = 1
coordinator.getParticleDump("driver").saveEnergySpreadRMSWeighted = 0
coordinator.getParticleDump("driver").savePositionMean            = 1
coordinator.getParticleDump("driver").savePositionMeanWeighted    = 0
coordinator.getParticleDump("driver").savePositionMeanLabframe    = 1
coordinator.getParticleDump("driver").savePositionMeanLabframeWeighted    = 0
coordinator.getParticleDump("driver").saveBunchLengthRMS          = 1
coordinator.getParticleDump("driver").saveBunchLengthRMSWeighted  = 0
coordinator.getParticleDump("driver").saveBunchLengthMax          = 1
coordinator.getParticleDump("driver").saveWidthRMS                = 1
coordinator.getParticleDump("driver").saveWidthRMSWeighted        = 0
coordinator.getParticleDump("driver").saveWidthMax                = 0
coordinator.getParticleDump("driver").saveDivergenceRMS           = 1
coordinator.getParticleDump("driver").saveDivergenceRMSWeighted   = 0
coordinator.getParticleDump("driver").saveEmittance               = 1
coordinator.getParticleDump("driver").saveAccField                = 0
coordinator.getParticleDump("driver").saveZeroCrossing            = 0
coordinator.getParticleDump("driver").saveCurrentMax              = 1
coordinator.getParticleDump("driver").saveBrightness5D            = 0
coordinator.getParticleDump("driver").saveAccFieldGradient        = 1
  
                       
coordinator.getElecMultiField().analyzeDumps(0)
coordinator.getElecMultiField().saveZeroCrossing            = 1
coordinator.getElecMultiField().saveMinField                = 1
coordinator.getElecMultiField().saveMaxField                = 1
coordinator.getElecMultiField().saveTransformerRatio        = 1

coordinator.getLasersPlusPlasma().analyzeDumps(0)
coordinator.getLasersPlusPlasma().saveZeroCrossing            = 1
coordinator.getLasersPlusPlasma().saveMinField                = 1
coordinator.getLasersPlusPlasma().saveMaxField                = 1
coordinator.getLasersPlusPlasma().saveTransformerRatio        = 1


#coordinator.getFieldDump(2).analyzeDumps(1)
#coordinator.getFieldDump(2).saveZeroCrossing            = 1
#coordinator.getFieldDump(2).saveMinField                = 1
#coordinator.getFieldDump(2).saveMaxField                = 1
#coordinator.getFieldDump(2).saveTransformerRatio        = 1
#

"""
#--------------------------------------------------------
#       execute code
#--------------------------------------------------------
"""

coordinator.dumpLoop()
     
"""   
#--------------------------------------------------------
#       plot summarized data
#--------------------------------------------------------
"""
    
if coordinator.plotSummarizedDumps:
#    coordinator._Summarizer.plotData("x",("driverCharge","HeElectronscharge",))
    coordinator._Summarizer.plotData("x",("driverCharge","0charge")) # note: putting a number into the string gives the corresponding witness beam (ofc, you can also use its name)
#    coordinator._Summarizer.plotData("x",("driverCharge", "EnergyMaxdriver"))
    coordinator._Summarizer.exportDataToFile()

del coordinator


