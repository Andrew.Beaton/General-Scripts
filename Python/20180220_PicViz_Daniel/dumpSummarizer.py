# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 08:56:24 2016

@author: Paul Scherkl
"""

import numpy as np
from matplotlib import pyplot as plt

class dumpSummarizer(object):
    """class definition to collect data from several dumps, also includes corresponding plots """
    
    def __init__(self, manager, dumpNumbers):
        self._M = manager
        self.currentDump       = 0
        self.dumpNumbers       = dumpNumbers
        self.xAxis             = []
        
        self.driver            = np.zeros((len(dumpNumbers),38))
        
        self.particleMatrixList         = []
        for i in range(len(self._M.particleDumpList)):
            newMatrix = np.zeros((len(dumpNumbers),38)) 
            self.particleMatrixList.append(newMatrix)

        self.fieldMatrixList         = []
        for i in range(len(self._M.fieldDumpList)):
            newMatrix = np.zeros((len(dumpNumbers),6)) 
            self.fieldMatrixList.append(newMatrix)

                        
        self.chargeAxis    = 0
        self.percentAxis   = 0
        self.energyAxis    = 0
        self.lengthAxis    = 0
        self.angleAxis     = 0
        self.emittanceAxis = 0
        self.noUnitAxis    = 0
        self.fieldAxis     = 0
        self.gradientAxis  = 0       
        self.brightnessAxis  = 0
        
        self.plotFigSize       = [9, 6]
        self.lineStyles        = []
        
        
    def update(self):
        """general update routine, checks for all possible dumps and then calls individual data collection methods"""
        currentX = -1
        self.currentDump = self._M.dumpNumber
#        if self._M.elecMultiField.loaded and self._M.elecMultiField.analyze:
#            self.getFieldData("elecMultiField")
#            if currentX == -1:
#                currentX  = self._M.elecMultiField.getWindowStart()
#            
#        if self._M.lasersPlusPlasma.loaded and self._M.lasersPlusPlasma.analyze:
#            self.getFieldData("lasersPlusPlasma")
#            if currentX == -1:
#                currentX  = self._M.lasersPlusPlasma.getWindowStart()
#            
#        if self._M.sumRhoJ.loaded and self._M.sumRhoJ.analyze:
#            self.getFieldData("sumRhoJ")
#            if currentX == -1:
#                currentX  = self._M.sumRhoJ.getWindowStart()
        for i in range(len(self._M.fieldSpecies)):
            if self._M.fieldSpecies[i].loaded and self._M.fieldSpecies[i].analyze:
                self.getFieldData(i)
                if currentX == -1:
                    currentX  = self._M.fieldSpecies[i].getWindowStart()
            
        if self._M.beamElectrons.loaded and self._M.beamElectrons.analyze and len(self._M.beamElectrons.X) > 10:
            self.getElecData("driver")
            if currentX == -1:
                currentX  = self._M.beamElectrons.getWindowStart()
            
        for i in range(len(self._M.particleSpecies)):
            if self._M.particleSpecies[i].loaded and self._M.particleSpecies[i].analyze and len(self._M.particleSpecies[i].X) > 10:
                self.getElecData(i)
                if currentX == -1:
                    currentX  = self._M.particleSpecies[i].getWindowStart()
                    
        self.xAxis.append(currentX)
            
        
      
     
    def exportDataToFile(self):
        """exports collected data to txt file at source file location"""
#        print "Beam data saved to " + self._M.outPath
        for i in range(len(self.particleMatrixList)):
            if self._M.getParticleDump(i).analyze == 1:
                try:
                    np.savetxt(self._M.outPath + 'Summarized_' + self._M.particleDumpList[i] + '.txt', self.particleMatrixList[i], fmt='%.6e', delimiter=' , ', newline='\n', header = self.getElecDataHeader(), comments='# ')
                except:
                    print "   (!) Could not save Summarized " + self._M.particleDumpList[i] + " file"  
        if self._M.getParticleDump("driver").analyze == 1:
            try:
                np.savetxt(self._M.outPath + 'Summarized_Driver.txt', self.driver, fmt='%.6e', delimiter=' , ', newline='\n', header = self.getElecDataHeader(), footer='', comments='# ')
            except:
                print "   (!) Could not save Summarized Driver file"  

        for i in range(len(self.fieldMatrixList)):
            if self._M.getFieldDump(i).analyze == 1:
                try:
                    np.savetxt(self._M.outPath + 'Summarized_' + self._M.fieldDumpList[i] + '.txt', self.fieldMatrixList[i], fmt='%.6e', delimiter=' , ', newline='\n', header = self.getFieldDataHeader(), comments='# ')
                except:
                    print "   (!) Could not save Summarized " + self._M.fieldDumpList[i] + " file"  
                    
                

    def getElecDataHeader(self):
        """returns header string for summarized electron files"""
        header = "  dump# , Charge [pC] , EmeanW [MeV] , Emean [MeV] , Emax [MeV] , GammaMean , dWrmsW [MeV] , dWrms [MeV] , dW/W W , dW/W , x mean [um] , y mean [um] , z mean [um] , x mean L [um] , y mean L [um] , z mean L [um] , LenRMSW [um] , LenRMS [um] , LenMax [um] , YwidthrmsW [um] , ZwidthrmsW [um] , Ywidthrms [um] , Zwidthrms[um] , Ywidthmax [um] , Zwidthmax [um] , divYrms [mrad] , divZrms [mrad] , emitYrms [m rad] , emitZrms [m rad] , accField [GV/m] , zeroCross [um] , peakCurr [kA] , bright5D , dAccField [GV/m^2] , Emin [MeV], emitYrmsDisp [m rad] , emitZrmsDisp [m rad], totalE [J] \n"
        return header
    
    
    def getFieldDataHeader(self):
        """returns header string for summarized field files"""
        header = "  dump# , zeroCross[um] , minField [GV/m] , maxField [GV/m] , transformer , potMin \n"
        return header
    
    def getElecData(self, dump):
        """Calculates parameters for electronDumps and saves them into matrix (Note: please do NOT change indizes! they are required for accessing correct entries!)"""
        particleDump = 0
        matrix       = 0
        index        = 0

        if isinstance(dump,basestring) and dump.lower() == "driver":
                particleDump = self._M.getParticleDump("driver")  
                index = -1
                matrix = self.driver
        elif isinstance(dump,(int,float)) or (isinstance(dump,basestring) and dump.lower() is not "driver"):
                particleDump = self._M.getParticleDump(dump)
                for i in range(len(self._M.particleDumpList)):
                    if self._M.particleDumpList[i] == particleDump.getName():
                        index = i
                        break
                matrix = self.particleMatrixList[index]
        else:
            raise Exception("   (!) Please pass valid beam identifier")
            
        if particleDump.name == "driver":
            print "        -----------------------"
            print "        Driver parameters"
        else:
            print "        -----------------------"
            print "        " + particleDump.name + " parameters"
            
        
        if len(self.dumpNumbers) == 1:
            currentRow = 0
        else:
            currentRow   = np.where(self.dumpNumbers == self.currentDump)
        matrix[currentRow, 0] = self.currentDump
     
        
            
        if particleDump.saveCharge:
            value = particleDump.getCharge()
            matrix[currentRow, 1] = value
            print "        Charge = " + str(value) + " pC"
            
        if particleDump.saveEnergyMeanWeighted:
            value = particleDump.getEnergyMeanWeighted()
            matrix[currentRow, 2] = value
            print "        Mean Energy (weighted)= " + str(value) + " MeV"
            
        if particleDump.saveEnergyMean:
            value = particleDump.getEnergyMean()
            matrix[currentRow, 3] = value
            print "        Mean Energy= " + str(value) + " MeV"
           
        if particleDump.saveEnergyMax:
            value = particleDump.getEnergyMax()
            matrix[currentRow, 4] = value
            print "        Max Energy= " + str(value) + " MeV"
            
        if particleDump.saveMinEnergy:
            value = 0
            value = np.min(particleDump.E)
            matrix[currentRow, 34] = value     
            print "        Min Energy = " + str(value) + " MeV"



        if particleDump.saveGammaMean:
            value = particleDump.getGammaMean()
            matrix[currentRow, 5] = particleDump.getGammaMean()
            print "        gamma mean= " + str(value) 
            
        if particleDump.saveEnergyDevRMSWeighted:
            value = particleDump.getEnergyDevRMSWeighted()
            matrix[currentRow, 6] = value
            print "        Energy deviation rms (weighted)= " + str(value) + " MeV"
            
        if particleDump.saveEnergyDevRMS:
            value = particleDump.getEnergyDevRMS()
            matrix[currentRow, 7] = value
            print "        Energy deviation rms = " + str(value) + " MeV"
            
        if particleDump.saveEnergySpreadRMSWeighted:
            value = particleDump.getEnergySpreadRMSWeighted()      
            matrix[currentRow, 8] =  value
            print "        Energy spread rms (weighted)= " + str(value) 
            
            
        if particleDump.saveEnergySpreadRMS:
            value = particleDump.getEnergySpreadRMS()      
            matrix[currentRow, 9] = value            
            print "        Energy spread rms = " + str(value) 
            
        if particleDump.savePositionMean:
             value = particleDump.getPositionMean("x")     
             matrix[currentRow, 10]  = value
             print "        Mean position X = " + str(value) + " um"
             
             value = particleDump.getPositionMean("y")     
             matrix[currentRow, 11] = value
             print "        Mean position Y = " + str(value) + " um"

             value = particleDump.getPositionMean("z")     
             matrix[currentRow, 12] = value    
             print "        Mean position Z = " + str(value) + " um"
             
        if particleDump.savePositionMeanLabframe:
             value = particleDump.getPositionMeanLabframe("x")     
             matrix[currentRow, 13]  = value        
             print "        Mean position X (labframe)= " + str(value/1000) + " mm"

             value = particleDump.getPositionMeanLabframe("y")     
             matrix[currentRow, 14]  = value          
             print "        Mean position Y (labframe)= " + str(value) + " um"

             value = particleDump.getPositionMeanLabframe("z")     
             matrix[currentRow, 15]  = value         
             print "        Mean position Z (labframe)= " + str(value) + " um"
 
        if particleDump.saveBunchLengthRMSWeighted:
             value = particleDump.getBunchLengthRMSWeighted()   
             matrix[currentRow, 16] = value 
             print "        Bunch length rms (weighted)= " + str(value) + " um"
             
        if particleDump.saveBunchLengthRMS:
             value = particleDump.getBunchLengthRMS()   
             matrix[currentRow, 17] = value          
             print "        Bunch length rms = " + str(value) + " um"
 
        if particleDump.saveBunchLengthMax:
             value = particleDump.getBunchLengthMax()   
             matrix[currentRow, 18] = value
             print "        Bunch length max = " + str(value) + " um"
             
          
        if particleDump.saveWidthRMSWeighted:
             value = particleDump.getBunchWidthRMSWeighted("y")   
             matrix[currentRow, 19] = value     
             print "        Bunch width Y rms (weighted) = " + str(value) + " um"
             
             value = particleDump.getBunchWidthRMSWeighted("z")   
             matrix[currentRow, 20] = value          
             print "        Bunch width Z rms (weighted) = " + str(value) + " um"
          
        if particleDump.saveWidthRMS:
             value = particleDump.getBunchWidthRMS("y")   
             matrix[currentRow, 21] = value
             print "        Bunch width Y rms = " + str(value) + " um"
             
             value = particleDump.getBunchWidthRMS("z")   
             matrix[currentRow, 22] = value     
             print "        Bunch width Z rms = " + str(value) + " um"
          
        if particleDump.saveWidthMax:
             value = particleDump.getBunchWidthMax("y")   
             matrix[currentRow, 23] = value 
             print "        Bunch width Y max = " + str(value) + " um"
             
             value = particleDump.getBunchWidthMax("z")   
             matrix[currentRow, 24] = value            
             print "        Bunch width Z max = " + str(value) + " um"
          
        if particleDump.saveDivergenceRMS:
             value = particleDump.getDivergenceRMS("y")   
             matrix[currentRow, 25] = value         
             print "        Bunch divergence Y rms = " + str(value) + " mrad"
             
             value = particleDump.getDivergenceRMS("z")   
             matrix[currentRow, 26] = value      
             print "        Bunch divergence Z rms = " + str(value) + " mrad"
          
        if particleDump.saveEmittance:
             value = particleDump.getEmittance("y", 0)   
             matrix[currentRow, 27] = value
             print "        Bunch emittance Y rms = " + str(value) + " m rad"

             value = particleDump.getEmittance("z", 0)   
             matrix[currentRow, 28] = value            
             print "        Bunch emittance Z rms = " + str(value) + " m rad"
             
             
        if particleDump.saveEmittanceDispersion:
             value = particleDump.getEmittance("y", 1)   
             matrix[currentRow, 35] = value
             print "        Bunch emittance Y rms with dispersion = " + str(value) + " m rad"

             value = particleDump.getEmittance("z", 1)   
             matrix[currentRow, 36] = value            
             print "        Bunch emittance Z rms with dispersion = " + str(value) + " m rad"

             
             

        if particleDump.saveAccField:
            value = 0
            if self._M.getElecMultiField().loaded:
                value = self._M.getElecMultiField().getField(particleDump.getPositionMean(0))
            elif self._M.getLasersPlusPlasma().loaded:
                value = self._M.getLasersPlusPlasma().getField(particleDump.getPositionMean(0))     
            matrix[currentRow, 29] = value     
            if value is not 0:
                print "        Accelerating field @ bunch position = " + str(value) + " GV/m"
                

        if particleDump.saveZeroCrossing:
            value = 0
                    
            if self._M.getElecMultiField().loaded:
                value = self._M.getElecMultiField().getFieldZeroCrossing()  
            elif self._M.getLasersPlusPlasma().loaded:
                value = self._M.getLasersPlusPlasma().getFieldZeroCrossing()        
            matrix[currentRow, 30] =   value     
            if value is not 0:
                print "        Field zero crossing position = " + str(value) + " um"
            

        if particleDump.saveCurrentMax:
             value = particleDump.getCurrentMax()   
             matrix[currentRow, 31] =   value          
             print "        Beam max current = " + str(value) + " kA"

        if particleDump.saveBrightness5D:
             value = particleDump.getBrightness5D()       
             matrix[currentRow, 32] = value
             print "        5D brightness = " + str(value) + " A /(m^2 rad^2)"
             
        if particleDump.saveAccFieldGradient:
            value = 0
            if self._M.getElecMultiField().loaded:
                value = self._M.getElecMultiField().getFieldGradient(particleDump.getPositionMean(0))  
            elif self._M.getLasersPlusPlasma().loaded:
                value = self._M.getLasersPlusPlasma().getFieldGradient(particleDump.getPositionMean(0))  
            matrix[currentRow, 33] = value     
            if value is not 0:
                print "        Gradient of Accelerating field @ bunch position = " + str(value) + " GV/m^2"
        
        
        if particleDump.saveTotalEnergy:
             value = particleDump.getTotalEnergy()   
             matrix[currentRow, 37] =   value          
             print "        Total energy = " + str(value) + " J"
        
#        if particleDump.saveMinField:
#            value = 0
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldLOMin()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldLOMin()
#            matrix[currentRow, 32] = value     
#            if value is not 0:
#                print "        Min Accelerating field = " + str(value) + " GV/m"        
#        
#        if particleDump.saveMaxField:
#            value = 0
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldLOMax()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldLOMax()
#            matrix[currentRow, 33] = value     
#            if value is not 0:
#                print "        Max Accelerating field = " + str(value) + " GV/m"        
#       
#        if particleDump.saveTransformerRatio:
#            value = 0
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldTransformerRatio()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldTransformerRatio()
#            matrix[currentRow, 34] = value     
#            if value is not 0:
#                print "        Transformer Ratio  = " + str(value)

    
    def getFieldData(self, field):
        matrixField   = 0
        fieldDump = 0
        
        
        if isinstance(field,(int,float)) or (isinstance(field,basestring)):
                fieldDump = self._M.getFieldDump(field)
                
                for i in range(len(self._M.fieldDumpList)):
                    if self._M.fieldDumpList[i] == fieldDump.getName():
                        index = i
                        break
                matrixField = self.fieldMatrixList[index]
                print "        -----------------------"
                print "        "+ fieldDump.getName() + " parameters"
        else:
            raise Exception("   (!) Please pass valid field identifier")
        
        
        
        if len(self.dumpNumbers) == 1:
            currentRow = 0
        else:
            currentRow   = np.where(self.dumpNumbers == self.currentDump)
            
        matrixField[currentRow, 0] = self.currentDump        
        
        if fieldDump.saveZeroCrossing:
            value = fieldDump.getFieldZeroCrossing()   
            matrixField[currentRow, 1] = value         
            if value is not 0:
                print "        Field zero crossing position = " + str(value) + " um"                
                          
        if fieldDump.saveMinField:
#            value = 0
            value = fieldDump.getFieldLOMin()
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldLOMin()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldLOMin()
            matrixField[currentRow, 2] = value     
            if value is not 0:
                print "        Min Accelerating field = " + str(value) + " GV/m"        
        
        if fieldDump.saveMaxField:
#            value = 0
            value = fieldDump.getFieldLOMax()
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldLOMax()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldLOMax()
            matrixField[currentRow, 3] = value     
            if value is not 0:
                print "        Max Accelerating field = " + str(value) + " GV/m"        
       
        if fieldDump.saveTransformerRatio:
#            value = 0
            value = fieldDump.getFieldTransformerRatio()
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldTransformerRatio()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldTransformerRatio()
            matrixField[currentRow, 4] = value     
            if value is not 0:
                print "        Transformer Ratio  = " + str(value)    
        
    
        if fieldDump.saveMinPotential:
#            value = 0
            value = fieldDump.getPotLOMin()
#            if self._M.elecMultiField.loaded:
#                value = self._M.elecMultiField.getFieldLOMax()
#            elif self._M.lasersPlusPlasma.loaded:
#                value = self._M.lasersPlusPlasma.getFieldLOMax()
            matrixField[currentRow, 5] = value     
            if value is not 0:
                print "        Min potential = " + str(value) + " e/mc^2"        
    
    
    def plotData(self, xAxis, yAxis):
        """ plots summarized data. xAxis can be either VSIM "x", "xlab", or "t" or "nr" (dumpnumber). 
        You may insert a list of variables for y-axis that are plotted e.g. yAxis=(WitnessCharge, WitnessEnergyMean,...) \n
        Charge               \n
        EnergyMean           \n
        EnergyMeanWeighted      \n
        EnergyMax            \n
        GammaMean            \n
        EnergyDevRMS         \n
        EnergyDevRMSWeighted    \n
        EnergySpreadRMS      \n
        EnergySpreadRMSWeighted \n
        PositionMeanX/Y/Z         \n
        PositionMeanLabframeX/Y/Z \n
        BunchLengthRMS       \n
        BunchLengthRMSWeighted  \n
        BunchLengthMax       \n
        WidthRMSY/Z          \n  
        WidthRMSGaussY/Z     \n
        WidthMaxX/Y          \n  
        DivergenceRMSY/Z     \n
        EmittanceY/Z         \n      
        AccField             \n      
        LongFieldZeroCrossing       \n
        CurrentMax           \n
        Brightness5D         \n
        and similar for driver beams \n
        \n        
        For field parameters use \n            
        MinField                    \n
        MaxField                    \n
        TransformerRatio            \n
        and similar for driver beams""" 
        
        fig, mainAxis = plt.subplots(figsize=self.plotFigSize)
      
  
        

        if isinstance(xAxis, basestring):
            if xAxis.lower()   == "x":
                xAxis = [x/1000.0 for x in self.xAxis]

            elif xAxis.lower()   == "xlab":
#                xAxis = self.xAxis + 
                a = 0
            elif xAxis.lower() == "t":
                xAxis = [x/1e6/299792458.0 for x in self.xAxis]
            elif xAxis.lower() == "nr":
                xAxis = self.dumpNumbers
            else:
                raise Exception("xAxis can be either VSIM x, xlab, t or nr (dumpnumber)")

        if isinstance(yAxis, basestring):
            yAxis = [yAxis]
            
        for i in range(len(yAxis)):
#            if yAxis[i].lower().find("witness") > -1:
#                matrix = self.witness
#            elif yAxis[i].lower().find("driver") > -1:
#                matrix = self.driver
            if isinstance(yAxis[i], basestring) and yAxis[i].lower().find("driver") > -1:
                matrix = self.driver
                                          
            elif yAxis[i].lower().find("elecMultiField".lower()) > -1:
                matrix = self.elecMultiField
            elif yAxis[i].lower().find("laserPlusPlasma".lower()) > -1:
                matrix = self.laserPlusPlasma
            elif yAxis[i].lower().find("sumRhoJ".lower()) > -1:
                matrix = self.sumRhoJ
                
            elif isinstance(yAxis[i],(int,float)) or (isinstance(yAxis[i],basestring) and yAxis[i].lower() is not "driver"):
                index = 0
                expr = [int(s) for s in yAxis[i].split() if s.isdigit()]
                if len(expr) > 0:
                    index = expr[0]
                else:
                    for j in range(len(self._M.particleDumpList)):
                        if yAxis[i].lower().find(self._M.particleDumpList[j].lower()) > -1:
                            index = j
                            break
                matrix = self.particleMatrixList[index]
            
            else:
                raise Exception("   (!) Please pass valid dump indentifier!")            
            
            
            if yAxis[i].lower().find("charge") > -1:
                if i == 0:
                    self.chargeAxis = mainAxis
                else:
                    if self.chargeAxis == 0:
                        self.chargeAxis    = mainAxis.twinx()
                self.chargeAxis.plot(xAxis, matrix[:,0], "g-")
                self.chargeAxis.set_xlabel("d [mm]")                                   
                self.chargeAxis.set_ylabel("Charge [pC]")         

            elif yAxis[i].lower().find("EnergyMeanWeighted".lower()) > -1:
                if i == 0:
                    self.energyAxis = mainAxis
                else:
                    if self.energyAxis == 0:
                        self.energyAxis = mainAxis.twinx()
                self.energyAxis.plot(xAxis, matrix[:,1], "r-")
                self.energyAxis.set_xlabel("d [mm]")                                   
                self.energyAxis.set_ylabel("EnergyMeanWeighted [MeV]")         

            elif yAxis[i].lower().find("EnergyMean".lower()) > -1:
                if i == 0:
                    self.energyAxis = mainAxis
                else:
                    if self.energyAxis == 0:
                        self.energyAxis = mainAxis.twinx()
                self.energyAxis.plot(xAxis, matrix[:,2], "r-")    
                self.energyAxis.set_xlabel("d [mm]")                                   
                self.energyAxis.set_ylabel("EnergyMean [MeV]")                         
                                
            elif yAxis[i].lower().find("EnergyMax".lower()) > -1:
                if i == 0:
                    self.energyAxis = mainAxis
                else:
                    if self.energyAxis == 0:
                        self.energyAxis = mainAxis.twinx()
                self.energyAxis.plot(xAxis, matrix[:,3], "r-")            
                self.energyAxis.set_xlabel("d [mm]")                                   
                self.energyAxis.set_ylabel("WitnessEnergyMax [MeV]")         
                
            elif yAxis[i].lower().find("GammaMean".lower()) > -1:
                if i == 0:
                    self.noUnitAxis = mainAxis
                else:
                    if self.noUnitAxis == 0:
                        self.noUnitAxis = mainAxis.twinx()
                self.noUnitAxis.plot(xAxis, matrix[:,4], "k-")            
                self.noUnitAxis.set_xlabel("d [mm]")                                   
                self.noUnitAxis.set_ylabel("GammaMean")         
                
            elif yAxis[i].lower().find("EnergyDevRMSWeighted".lower()) > -1:
                if i == 0:
                    self.energyAxis = mainAxis
                else:
                    if self.energyAxis == 0:
                        self.energyAxis = mainAxis.twinx()
                self.energyAxis.plot(xAxis, matrix[:,5], "r-")         
                self.energyAxis.set_xlabel("d [mm]")                                   
                self.energyAxis.set_ylabel("EnergyDevRMSWeighted [MeV]")         
        
            elif yAxis[i].lower().find("EnergyDevRMS".lower()) > -1:
                if i == 0:
                    self.energyAxis = mainAxis
                else:
                    if self.energyAxis == 0:
                        self.energyAxis = mainAxis.twinx()
                self.energyAxis.plot(xAxis, matrix[:,6], "r-")            
                self.energyAxis.set_xlabel("d [mm]")                                   
                self.energyAxis.set_ylabel("EnergyDevRMSGauss [MeV]") 
                
            elif yAxis[i].lower().find("EnergySpreadRMSWeighted".lower()) > -1:
                if i == 0:
                    self.percentAxis = mainAxis
                else:
                    if self.percentAxis == 0:
                        self.percentAxis = mainAxis.twinx()
                self.percentAxis.plot(xAxis, matrix[:,7], "g-")
                self.percentAxis.set_xlabel("d [mm]")                                   
                self.percentAxis.set_ylabel("EnergySpreadRMSWeighted [um]") 
        
            elif yAxis[i].lower().find("EnergySpreadRMS".lower()) > -1:
                if i == 0:
                    self.percentAxis = mainAxis
                else:
                    if self.percentAxis == 0:
                        self.percentAxis = mainAxis.twinx()
                self.percentAxis.plot(xAxis, matrix[:,8], "g-")           
                self.percentAxis.set_xlabel("d [mm]")                                   
                self.percentAxis.set_ylabel("EnergySpreadRMS")                 
        
            elif yAxis[i].lower().find("PositionMeanX".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,9], "-")           
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanX [um]") 

            elif yAxis[i].lower().find("PositionMeanY".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,10], "-")   
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanY [um]") 
                
            elif yAxis[i].lower().find("PositionMeanZ".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,11], "-")           
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanZ [um]")            
                                
            elif yAxis[i].lower().find("PositionMeanLabframeX".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,12], "-")
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanLabframeX [um]") 

            elif yAxis[i].lower().find("PositionMeanLabframeY".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,13], "-")    
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanLabframeY [um]") 
                
            elif yAxis[i].lower().find("PositionMeanLabframeZ".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,14], "-")    
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("PositionMeanLabframeZ [um]") 
                                 
            elif yAxis[i].lower().find("BunchLengthRMSWeighted".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,15], "-")             
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("BunchLengthRMSWeighted [um]") 
        
            elif yAxis[i].lower().find("BunchLengthRMS".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,16], "-")               
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("BunchLengthRMS [um]") 
        
            elif yAxis[i].lower().find("BunchLengthMax".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,17], "-")       
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("BunchLengthMax [um]") 
                
        
            elif yAxis[i].lower().find("WidthRMSYWeighted".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,18], "-")           
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthRMSYWeighted [um]") 

            elif yAxis[i].lower().find("WidthRMSZWeighted".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,19], "-")        
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthRMSZWeighted [um]") 
                
                
            elif yAxis[i].lower().find("WidthRMSY".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,20], "-")          
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthRMSY [um]") 
                
            elif yAxis[i].lower().find("WidthRMSZ".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,21], "-")    
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthRMSZ [um]") 
                
            elif yAxis[i].lower().find("WidthMaxY".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,22], "-")    
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthMaxY [um]") 
                                
            elif yAxis[i].lower().find("WidthMaxZ".lower()) > -1:
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,23], "-")      
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("WidthMaxZ [um]") 
                    
                    
                    
            elif yAxis[i].lower().find("DivergenceRMSY".lower()) > -1:
                if i == 0:
                    self.angleAxis = mainAxis
                else:
                    if self.angleAxis == 0:
                        self.angleAxis = mainAxis.twinx()
                self.angleAxis.plot(xAxis, matrix[:,24], "-")   
                self.angleAxis.set_xlabel("d [mm]")
                self.angleAxis.set_ylabel("DivergenceRMSY [mrad]")
                
            elif yAxis[i].lower().find("DivergenceRMSZ".lower()) > -1:
                if i == 0:
                    self.angleAxis = mainAxis
                else:
                    if self.angleAxis == 0:
                        self.angleAxis = mainAxis.twinx()
                self.angleAxis.plot(xAxis, matrix[:,25], "-")     
                self.angleAxis.set_xlabel("d [mm]")                
                self.angleAxis.set_ylabel("DivergenceRMSZ [mrad]")
              
                
            elif yAxis[i].lower().find("EmittanceY".lower()) > -1:
                if i == 0:
                    self.emittanceAxis = mainAxis
                else:
                    if self.emittanceAxis == 0:
                        self.emittanceAxis = mainAxis.twinx()
                self.emittanceAxis.plot(xAxis, matrix[:,26], "-") 
                self.emittanceAxis.set_xlabel("d [mm]")
                self.emittanceAxis.set_ylabel("$\\varepsilon_n^y$ [m rad]")
                
                
            elif yAxis[i].lower().find("EmittanceZ".lower()) > -1:
                if i == 0:
                    self.emittanceAxis = mainAxis
                else:
                    if self.emittanceAxis == 0:
                        self.emittanceAxis = mainAxis.twinx()
                self.emittanceAxis.plot(xAxis, matrix[:,27], "-")       
                self.emittanceAxis.set_xlabel("d [mm]")
                self.emittanceAxis.set_ylabel("$\\varepsilon_n^z$ [m rad]")
            
            
            elif yAxis[i].lower().find("AccField".lower()) > -1:
                if i == 0:
                    self.fieldAxis = mainAxis
                else:
                    if self.fieldAxis == 0:
                        self.fieldAxis = mainAxis.twinx()
                self.fieldAxis.plot(xAxis, matrix[:,28], "-")       
                self.fieldAxis.set_xlabel("d [mm]")                                   
                self.fieldAxis.set_ylabel("AccField [GV/m]") 
             
            elif yAxis[i].lower().find("LongFieldZeroCrossing".lower()) > -1: 
                if i == 0:
                    self.lengthAxis = mainAxis
                else:
                    if self.lengthAxis == 0:
                        self.lengthAxis = mainAxis.twinx()
                self.lengthAxis.plot(xAxis, matrix[:,29], "-")                    
                self.lengthAxis.set_xlabel("d [mm]")                                   
                self.lengthAxis.set_ylabel("LongFieldZeroCrossing [um]") 
             
            elif yAxis[i].lower().find("CurrentMax".lower()) > -1: 
                if i == 0:
                    self.currentAxis = mainAxis
                else:
                    if self.currentAxis == 0:
                        self.currentAxis = mainAxis.twinx()
                self.currentAxis.plot(xAxis, matrix[:,30], "-") 
                self.currentAxis.set_xlabel("d [mm]")                   
                self.currentAxis.set_ylabel("CurrentMax [kA]")                   
             
             
            elif yAxis[i].lower().find("Brightness5D".lower()) > -1:
                if i == 0:
                    self.brightnessAxis = mainAxis
                else:
                    if self.brightnessAxis == 0:
                        self.brightnessAxis = mainAxis.twinx()
                self.brightnessAxis.plot(xAxis, matrix[:,31], "-")
                self.brightnessAxis.set_xlabel("d [mm]")                   
                self.brightnessAxis.set_ylabel("Brightness5D [A m^-2 rad^-2]")   
             
            
            elif yAxis[i].lower().find("AccFieldGradient".lower()) > -1:
                if i == 0:
                    self.gradientAxis = mainAxis
                else:
                    if self.gradientAxis == 0:
                        self.gradientAxis = mainAxis.twinx()
                self.gradientAxis.plot(xAxis, matrix[:,32], "-")
                self.gradientAxis.set_xlabel("d [mm]")                   
                self.gradientAxis.set_ylabel("AccFieldGradients [GV/m^2]")    

            elif yAxis[i].lower().find("minField".lower()) > -1:
                if i == 0:
                    self.fieldAxis = mainAxis
                else:
                    if self.fieldAxis == 0:
                        self.fieldAxis = mainAxis.twinx()
                self.fieldAxis.plot(xAxis, matrix[:,1], "-")
                self.fieldAxis.set_xlabel("d [mm]")                   
                self.fieldAxis.set_ylabel("min Field [GV/m]")    
             
            elif yAxis[i].lower().find("maxField".lower()) > -1:
                if i == 0:
                    self.fieldAxis = mainAxis
                else:
                    if self.fieldAxis == 0:
                        self.fieldAxis = mainAxis.twinx()
                self.fieldAxis.plot(xAxis, matrix[:,2], "-")
                self.fieldAxis.set_xlabel("d [mm]")                   
                self.fieldAxis.set_ylabel("max Field [GV/m]") 
            
            elif yAxis[i].lower().find("transformerRatio".lower()) > -1:
                if i == 0:
                    self.noUnitAxis = mainAxis
                else:
                    if self.noUnitAxis == 0:
                        self.noUnitAxis = mainAxis.twinx()
                self.noUnitAxis.plot(xAxis, matrix[:,3], "-")
                self.noUnitAxis.set_xlabel("d [mm]")                   
                self.noUnitAxis.set_ylabel("transformer ratio")            
            
            else:
                print "Could not read input " + yAxis[i] + ", check for spelling!"
                
                
                

                   