# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 11:21:46 2016

@author: Paul Scherkl
"""
from dump import *

class elecDump(dump):
    """class definition for electron Dump objects saving 6D phase space distributions. 
    for witness species assigned to these objects, tags and weights are also saved"""
    
    _eMass      = 9.1093821499999992e-31    # kg
    _elemChar   = 1.6021766208e-19          # C
    _eMassMeV   = 1e-6*510998.90984764055   # MeV
    _c          = 299792458.0               # m/s
    
    def __init__(self, name):
        dump.__init__(self, name, "electrons")
        
        self.X      = 0
        self.Y      = 0
        self.Z      = 0
        self.PX     = 0
        self.PY     = 0
        self.PZ     = 0
        self.YP     = 0
        self.ZP     = 0
        self.E      = 0
        self.EX     = 0
        self.EY     = 0
        self.EZ     = 0
        self.Etrans     = 0
        self.T      = 0
        self.Tag    = 0
        self.Weight = 0

        self.runTime = 0
        self.comp2D  = 1
        
        self.numPtclsInMacro    = 0
        self.plotMarker         = "k."
        self.plotMarkerSize     = 1      
        
        self.HistBinSize        = 0.06
        self.plotHist           = 0
        self.histSmooth         = 0
                
        self.debugAutoCutter    = 0
        
        self.colorMap           = plt.cm.get_cmap('bwr')
        self.colorCodeComp      = ""

        self.plotParticleRatio = 1
        
        self.height3D = 0


        self.saveCharge                  = 0
        self.saveEnergyMeanWeighted      = 0
        self.saveEnergyMean              = 0
        self.saveEnergyMax               = 0
        self.saveGammaMean               = 0
        self.saveGammaMeanWeighted       = 0
        self.saveEnergyDevRMSWeighted    = 0
        self.saveEnergyDevRMS            = 0
        self.saveEnergySpreadRMS         = 0
        self.saveEnergySpreadRMSWeighted = 0
        self.savePositionMean            = 0
        self.savePositionMeanWeighted    = 0
        self.savePositionMeanLabframe    = 0
        self.savePositionMeanLabframeWeighted    = 0
        self.saveBunchLengthRMS          = 0
        self.saveBunchLengthRMSWeighted  = 0
        self.saveBunchLengthMax          = 0
        self.saveWidthRMS                = 0
        self.saveWidthRMSWeighted        = 0
        self.saveWidthMax                = 0
        self.saveDivergenceRMS           = 0
        self.saveDivergenceRMSWeighted   = 0
        self.saveEmittance               = 0
        self.saveAccField                = 0
        self.saveZeroCrossing            = 0
        self.saveCurrentMax              = 0
        self.saveBrightness5D            = 0
        self.saveAccFieldGradient        = 0
        self.saveEmittanceDispersion     = 0
        self.saveMinEnergy               = 0
        self.saveTotalEnergy             = 0


        self.reverseTransparency         = 0
        self.transparencyVector          = 0
        self.colorBarNorm                = 0


        self.particleTrajectories = []
        self.historyTags          = []
        self.historyName          = ""
        self.historyColor         = "blue"
        self.historyTransMin           = []
        self.historyTransMax           = []
        self.historyXMin           = []
        self.historyXMax           = []
        self.historyPrintTag           = 0
           
        
        
    def reset(self):
        """deletes all individual properties"""
        self.loaded = 0
        self.X  = 0
        self.Y  = 0
        self.Z  = 0
        self.PX = 0
        self.PY = 0
        self.PZ = 0
        self.E  = 0
        self.EX     = 0
        self.EY     = 0
        self.EZ     = 0
        self.Etrans = 0
        self.YP = 0
        self.ZP = 0
        self.Tag    = 0
        self.Weight = 0
        self.windowStart        = 0
        self.windowEnd          = 0
        self.numPtclsInMacro    = 0
        self.runTime = 0
        
        
############################################# Plot-related methods ################################################################
 
    
    
    def plotParticleTrajectories(self, tags, **kwargs):
        """ plots trajectoies of tracked particles. 
        
        @tags (list, requried): can be single number or list. 
        
        keywords: 
            
        @history(string): name of the history from Vsim 
        @color(string): color of trajectories
        
        Trajectory selection: all min and max value conditions are additive.
        @transmin(float or list): only trajectories > transmin (defined comp2D, so y or z) are shown. if transmin = [a,b], then trajectories between a and b are shown
        @transmax(float or list): only trajectories < transmax (defined comp2D, so y or z) are shown. if transmax = [a,b], then trajectories between a,b are shown
        @xmin(float or list): only trajectories > xmin (defined comp2D, so y or z) are shown. if xmin = [a,b], then trajectories between a,b are shown
        @xmax(float or list): only trajectories < xmax (defined comp2D, so y or z) are shown. if xmax = [a,b], then trajectories between a,b are shown
        @printtag (int): if printtag == 1, show all tags. if printtag > 1, show #printtag longest trajectories (fulfilling conditions defined by min and max keywords)
        
        """
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                if key.lower() == "history":
                    self.historyName = value
                if key.lower() == "color":
                    self.historyColor = value
                    
                if key.lower() == "transmin":
                    if isinstance(value, (int, float)):
                        self.historyTransMin = [value]
                    elif isinstance(value, (list, tuple)):
                        if len(value) == 1 or len(value) == 2:
                                self.historyTransMin = [min(value), max(value)]
                        else:
                            print " (!)  Wrong input for history min trans tags, use single numbers or lists. Command will be ignored"
                    else:
                        print " (!)  Wrong input for history min trans tags, use single numbers or lists. Command will be ignored"
                                
                if key.lower() == "transmax":
                    if isinstance(value, (int, float)):
                        self.historyTransMax = [value]
                    elif isinstance(value, (list, tuple)):
                        if len(value) == 1 or len(value) == 2:
                            for i in range(len(value)):
                                self.historyTransMax = [min(value), max(value)]
                        else:
                            print " (!)  Wrong input for history trans max tags, use single numbers or lists with one or two entries. Command will be ignored"
                    else:
                        print " (!)  Wrong input for history trans max tags, use single numbers or lists with one or two entries. Command will be ignored"
             
                if key.lower() == "xmin":
                    if isinstance(value, (int, float)):
                        self.historyXMin = [value]
                    elif isinstance(value, (list, tuple)):
                        if len(value) == 1 or len(value) == 2:
                                self.historyXMin = [min(value), max(value)]
                        else:
                            print " (!)  Wrong input for history min X tags, use single numbers or lists. Command will be ignored"
                    else:
                        print " (!)  Wrong input for history min X tags, use single numbers or lists. Command will be ignored"
                                
                if key.lower() == "xmax":
                    if isinstance(value, (int, float)):
                        self.historyXMax = [value]
                    elif isinstance(value,(list, tuple)):
                        if len(value) == 1 or len(value) == 2:
                            for i in range(len(value)):
                                self.historyXMax = [min(value), max(value)]
                        else:
                            print " (!)  Wrong input for history X max tags, use single numbers or lists with one or two entries. Command will be ignored"
                    else:
                        print " (!)  Wrong input for history X max tags, use single numbers or lists with one or two entries. Command will be ignored"
        
                if key.lower() == "printtag":
                    if isinstance(value, (int, float)):
                        self.historyPrintTag = int(value)
                    else:
                        print " (!)  Wrong input for printtag. Use integer values. Command will be ignored"
                        
                
                
            if self.historyName == "":
                print "No name for history given. use default \"trajectory\""
                self.historyName = "trajectory"
                
        if isinstance(tags, (int, float)):
            self.historyTags = [tags]
        elif isinstance(tags, (list, tuple, np.ndarray, np.generic)):
            self.historyTags = tags
        else:
            print " (!)  Wrong input for particle tags, use single numbers or lists. Command will be ignored"
            self.historyTags = []



    def setTransparencyVec(self, comp, **kwargs):
        """set vector that defines transparency in colorcoded plots. **kwargs:   @norm = (min,max) values < min will have max opacity, values > max 1 will have no transparency.
        @reverse = True or False: reverses opacity values)""" 
        if isinstance(comp, basestring):
            comp = comp.lower()
        self.transparencyVector = comp
        normalization = 0
        for key in kwargs:
            if key.lower() == "reverse":
                reverse = kwargs[key]
                if isinstance(reverse, (int, float)):
                    self.reverseTransparency = kwargs[key]
                else:
                    if reverse == True:
                        self.reverseTransparency = 1
                    else: 
                        self.reverseTransparency = 0
                    
            if key.lower()  == "norm" or key.lower()  == "normalization":
                normalization = kwargs[key]
        if isinstance(normalization, (list, tuple)) and len(normalization) > 0:
            normalization = [np.float(np.min(normalization)), np.float(np.max(normalization))]
            self.colorBarNorm = normalization


    def setHistSmooth(self, switch):
        """makes a binned curve nice and cosy"""
        self.histSmooth = switch



    def setheight3D(self, height):
        """moves the z-component in the 3D plot"""
        self.height3D = height
        
    def setPlotParticleRatio(self, ratio):
        """set the number of plotted beam particles to value given by @ratio"""
        self.plotParticleRatio = ratio
        
    def setPlotHist(self,switch):
        """ """
        self.plotHist = switch




    def getColorCodeComp(self):
        return self.colorCodeComp
    
    
    def setColorCodeComp(self, comp):
        """ if @comp is set, the electron bunch will be plotted COLOR-CODED in all plots. @comp can be 0-11 or X, Y, Z, PX, PY, PZ, YP, ZP, E, Tag, or Weight!  0 or empty string turn option off"""
        if isinstance(comp, basestring):
            comp = comp.lower()
        self.colorCodeComp = comp
       
    
    def getTransparencyVector(self):
        """returns vector that's used to code transparency"""
        comp = self.transparencyVector
        return self.getVectorForComponent(comp)

        
    def getColorCodeVector(self):
        """identifies defined vector for color coding for particle beam plots"""
        comp = self.colorCodeComp
        return self.getVectorForComponent(comp)
    
    

    def setColorMap(self, colormap):
        """sets color map for plots """
        self.colorMap = plt.cm.get_cmap(colormap)

    def setTransparency(self, transparency):
        """ set transparency of dump object plots"""
        self.transparency = transparency            

    def setPlotMarker(self, marker):
        """defines plot marker of dump object plots. can be any typical python marker, e.g. r-, k., etc"""
        self.plotMarker = marker            
        
    def setPlotMarkerSize(self, size):
        """defines size of plot markers"""
        self.plotMarkerSize = size
        
      
    def setHistBinSize(self, binSize):
        """in um"""
        self.HistBinSize = binSize
        
        
    

    def getVectorForComponent(self, comp):
        if comp == "" or comp == 0:
            referenceVec = 0
        elif isinstance(comp, (int, float)):
            if comp == 1:
                referenceVec = self.X
            elif comp == 2:
                referenceVec = self.Y
            elif comp == 3:
                referenceVec = self.Z
            elif comp == 4:
                referenceVec = self.PX
            elif comp == 5:
                referenceVec = self.PY
            elif comp == 6:
                referenceVec = self.PZ
            elif comp == 7:
                referenceVec = self.YP
            elif comp == 8:
                referenceVec = self.ZP
            elif comp == 9:
                referenceVec = self.E
            elif comp == 10:
                referenceVec = self.Tag
            elif comp == 11:
                referenceVec = self.Weight
            elif comp == 12:
                referenceVec = self.EX
            elif comp == 13:
                referenceVec = self.EY
            elif comp == 14:
                referenceVec = self.EZ
            elif comp == 15:
                referenceVec = self.Etrans
            else:
                print "   (!) Component " + str(comp) + " for colorCodeVector is not available! Check input argument, try 0-15 or X, Y, Z, PX, PY, PZ, YP, ZP, E, Tag, or Weight, Ex, Ey, Ez, Etrans!  0 or empty string turn option off"
                return np.zeros(len(self.X))
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                referenceVec = self.X
            elif comp.lower() == "y":
                referenceVec = self.Y
            elif comp.lower() == "z":
                referenceVec = self.Z
            elif comp.lower() == "px":
                referenceVec = self.PX
            elif comp.lower() == "py":
                referenceVec = self.PY
            elif comp.lower() == "pz":
                referenceVec = self.PZ
            elif comp.lower() == "yp":
                referenceVec = self.YP
            elif comp.lower() == "zp":
                referenceVec = self.ZP
            elif comp.lower() == "e" or comp.lower() == "energy":
                referenceVec = self.E
            elif comp.lower() == "tag":
                referenceVec = self.Tag
            elif comp.lower() == "weight":
                referenceVec = self.Weight
            elif comp.lower() == "ex" :
                referenceVec = self.EX
            elif comp.lower() == "ey" :
                referenceVec = self.EY
            elif comp.lower() == "ez" :
                referenceVec = self.EZ
            elif comp.lower() == "etrans" :
                referenceVec = self.Etrans
            else:
                print "   (!) Component " + comp + " for colorCodeVector is not available! Check input argument, try 0-14 or X, Y, Z, PX, PY, PZ, YP, ZP, E, Tag, or Weight, Ex, Ey, Ez!  0 or empty string turn option off"
                return np.zeros(len(self.X))
        return referenceVec

       
        
########################################### Modify electron dump ####################################################################

    def setAutoCutterDebug(self, switch):
        """puts autpCutter in debug mode to find out what's going on"""
        self.debugAutoCutter    = switch
        


    

    def autoCutter(self, keepNumber):
        """EXPERIMENTAL. projects long. phase space to histogram, identifies peaks, and keeps #keepNumber counted from high energies downwards. 
        Hereby, the middle between the last dump to keep and the dirst dump to delete defines the cut in energy"""
        if keepNumber <= 0:
            return None
        nBins = 1001
        maxE = max(self.E)*1.1
        if min(self.E) < 1: #verly low energy, e.g. driver lensing etc
            minE = 0.001
        else:
            minE = min(self.E)*0.9
            
        binSize = (maxE - minE)/nBins
        bins = np.arange(minE, maxE, binSize)
#        
        vector = np.histogram(self.E,bins)[0]
        bins = np.delete(bins, len(bins)-1)

        peakList =  signal.find_peaks_cwt(vector, bins) # get indizes of peaks
        if self.debugAutoCutter:
            print "Peaks found at    " + str(peakList)
            print "Peak energies     " + str(bins[peakList])
            print "Particles in peak " + str(vector[peakList])
            plt.plot(bins, vector)
            plt.show()
            plt.close()
        peakList = np.delete(peakList, np.where(vector[peakList] < 3)) # delete errors (e.g. non-existing peaks)
        if len(peakList) > keepNumber:
            energyCut = (bins[peakList[len(peakList)-keepNumber]] - bins[peakList[len(peakList)-keepNumber-1]])/2
            self.cutPhaseSpace("E", bins[peakList[len(peakList)-keepNumber]] - energyCut, max(self.E))
            if self.debugAutoCutter:
                print "Energies after deleting errors: " + str(bins[peakList])
                print "Cut particles below " + str(bins[peakList[len(peakList)-keepNumber]] - energyCut) +" MeV"



    def cutPhaseSpace(self, comp, lower, upper):
        """ removes all electrons OUTSIDE of interval [lower,upper] for given phase space coordinate comp. 
        all units are in um, mrad, or MeV"""
        if lower > upper:
            dummy = lower
            lower = upper
            upper = dummy
        referenceVec = 0
        if isinstance(comp, (int, float)):
            if comp == 0:
                referenceVec = self.X
            elif comp == 1:
                referenceVec = self.Y
            elif comp == 2:
                referenceVec = self.Z
            elif comp == 3:
                referenceVec = self.PX
            elif comp == 4:
                referenceVec = self.PY
            elif comp == 5:
                referenceVec = self.PZ
            elif comp == 6:
                referenceVec = self.YP
            elif comp == 7:
                referenceVec = self.ZP
            elif comp == 8:
                referenceVec = self.E
            elif comp == 9:
                referenceVec = self.EX
            elif comp == 10:
                referenceVec = self.EY
            elif comp == 11:
                referenceVec = self.EZ
            elif comp == 12:
                referenceVec = self.Etrans
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 0-12 or X, Y, Z, PX, PY, PZ, YP, ZP, or E, Ex, Ey, Ez, Etrans!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                referenceVec = self.X
            elif comp.lower() == "y":
                referenceVec = self.Y
            elif comp.lower() == "z":
                referenceVec = self.Z
            elif comp.lower() == "px":
                referenceVec = self.PX
            elif comp.lower() == "py":
                referenceVec = self.PY
            elif comp.lower() == "pz":
                referenceVec = self.Z
            elif comp.lower() == "yp":
                referenceVec = self.YP
            elif comp.lower() == "zp":
                referenceVec = self.ZP
            elif comp.lower() == "e" or comp.lower() == "energy":
                referenceVec = self.E
            elif comp.lower() == "ex":
                referenceVec = self.EX
            elif comp.lower() == "ey":
                referenceVec = self.EY
            elif comp.lower() == "ez":
                referenceVec = self.EZ
            elif comp.lower() == "etrans":
                referenceVec = self.Etrans
                
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 0-12 or X, Y, Z, PX, PY, PZ, YP, ZP, or E, Ex, Ey, Ez, Etrans!")
        self.X  = self.X[(referenceVec >= lower) & (referenceVec <= upper)]
        self.Y  = self.Y[(referenceVec >= lower) & (referenceVec <= upper)]
        self.Z  = self.Z[(referenceVec >= lower) & (referenceVec <= upper)]
        self.PX = self.PX[(referenceVec >= lower) & (referenceVec <= upper)]
        self.PY = self.PY[(referenceVec >= lower) & (referenceVec <= upper)]
        self.PZ = self.PZ[(referenceVec >= lower) & (referenceVec <= upper)]
        self.E  = self.E[(referenceVec >= lower) & (referenceVec <= upper)]
        self.EX  = self.EX[(referenceVec >= lower) & (referenceVec <= upper)]
        self.EY  = self.EY[(referenceVec >= lower) & (referenceVec <= upper)]
        self.EZ  = self.EZ[(referenceVec >= lower) & (referenceVec <= upper)]
        self.Etrans  = self.Etrans[(referenceVec >= lower) & (referenceVec <= upper)]
        self.YP = self.YP[(referenceVec >= lower) & (referenceVec <= upper)]
        self.ZP = self.ZP[(referenceVec >= lower) & (referenceVec <= upper)]
        self.T  = self.T[(referenceVec >= lower) & (referenceVec <= upper)]
        if self.Tag is not 0:
            self.Tag = self.Tag[(referenceVec >= lower) & (referenceVec <= upper)]
        if self.Weight is not 0:
            self.Weight = self.Weight[(referenceVec >= lower) & (referenceVec <= upper)]



        
        
################## Methods that calculate and return physical values ###################################################################        
        
        
        
    def getCharge(self):       
        """ calculating the total charge of the bunch in pC """         
        if self.Weight is 0:
            charge = elecDump._elemChar*self.numPtclsInMacro * len(self.PX)*1e12
        else:   
            charge = elecDump._elemChar*self.numPtclsInMacro * np.sum(self.Weight)*1e12
        return charge        
        
    def getCurrentMax(self):
        """calculates the peak current in kA """
        return np.max(self.getCurrentHist()[1])
      
    def xCheckCharge(self):
        """x-check of current value - recaluculate charge """
        binSize = self.chargeHistBinSize
        return np.sum(self.getCurrentHist()[1] * binSize/(elecDump._c*1e-9))
        
 
    def getBrightness5D(self):
        """calculates 5D brightness"""
        return 2 * self.getCurrentMax()/(self.getEmittance("y",0) * self.getEmittance("z",0)) * 1e3 # factor 1e3 to convert from kA to A --> A /(m^2 rad^2)

        
        
################## Energy quantities ###################################################################        
        
    def getEnergyMax(self):
        """calculates the max longitudinal energy in MeV"""    
        return  np.max(self.E)

    def getTotalEnergy(self):
        """calculats the total kinetic energy of a particle beam in J"""
        if isinstance(self.Weight, (float, int)):          
            return np.sum(self.E * 1e6) * elecDump._elemChar * self.numPtclsInMacro 
        else:
            return np.sum(self.E * 1e6  * self.Weight) * elecDump._elemChar * self.numPtclsInMacro
        
    def getEnergyMean(self):
        """calculates mean longitudinal energy in MeV for Gaussian distributions"""
        if isinstance(self.Weight, (float, int)):          
            meanEnergy = np.mean(self.E)
        else:
            meanEnergy = (1./np.sum(self.Weight))*np.sum(self.Weight*self.E)
        return meanEnergy
        
        
    def getEnergyMeanWeighted(self):
        """calculates mean longitudinal energy in MeV weighted by particle distributions"""
        return self.getWeightedMean(self.E)
        
        
    def getGammaMean(self):
        """calculates mean Lorentz factor for Gaussian distributions"""
        meanGamma = self.getEnergyMean()/elecDump._eMassMeV# + 1
        return meanGamma

    def getGammaMeanWeighted(self):
        """calculates mean Lorentz factor weighted by particle distributions"""
        meanGamma = self.getEnergyMeanWeighted()/elecDump._eMassMeV# + 1
        return meanGamma
        
        
              
    def getEnergyDevRMSWeighted(self):
        """calculates the root mean square of the energy in MeV for all distributions"""
        return self.getWeightedRms(self.E)

        
    def getEnergyDevRMS(self):
        """calculates the root mean square of the energy in MeV for Gaussian distributions"""
        if self.Weight is 0:
            rmsEnergy = np.sqrt(np.mean(self.E**2) - np.mean(self.E)**2)
        else:
            N = np.sum(self.Weight)            
            rmsEnergy = np.sqrt((1./N)*np.sum(self.Weight*self.E**2) - ((1./N)*np.sum(self.Weight*self.E))**2)
        return rmsEnergy
        
   

      
    def getEnergySpreadRMSWeighted(self):
        """ calculates the energy spread as energy_rms/meanEnergy for all distributions"""
        return self.getEnergyDevRMSWeighted()/self.getEnergyMeanWeighted()

    def getEnergySpreadRMS(self):
        """ calculates the energy spread as energy_rms/meanEnergy for Gaussian distributions"""
        return self.getEnergyDevRMS()/self.getEnergyMean()



    def getMomentumDevRMS(self, momentum):
        """calculates the root mean square of the energy in MeV for Gaussian distributions"""
        if self.Weight is 0:
            rmsMomentum = np.sqrt(np.mean(momentum**2) - np.mean(momentum)**2)
        else:
            N = np.sum(self.Weight)            
            rmsMomentum = np.sqrt((1./N)*np.sum(self.Weight*momentum**2) - ((1./N)*np.sum(self.Weight*momentum))**2)
        return rmsMomentum
  
    def getMomentumMean(self, momentum):
        """calculates mean longitudinal energy in MeV for Gaussian distributions"""
        if isinstance(self.Weight, (float, int)):          
            meanMomentum = np.mean(momentum)
        else:
            meanMomentum = (1./np.sum(self.Weight))*np.sum(self.Weight*momentum)
        return meanMomentum
    
    
    
    
    ################## position quantities ###################################################################        
        
    def getPositionMean(self,comp):    
        """ returns mean position of selected component (x,y,z or 0,1,2) of bunch in co-moving frame"""
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 0:
                vector = self.X
            elif comp == 1:
                vector = self.Y
            elif comp == 2:
                vector = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 0, 1, or 2!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                vector = self.X
            elif comp.lower() == "y":
                vector = self.Y
            elif comp.lower() == "z":
                vector = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try x, y, or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        if isinstance(self.Weight, (float, int)):          
            return np.mean(vector)
        else:
            return (1./np.sum(self.Weight))*np.sum(self.Weight*vector)

        
    def getPositionMeanWeighted(self,comp):    
        """ returns mean position of selected component (x,y,z or 0,1,2) of bunch in co-moving frame weighted by distribution"""
        if isinstance(comp, (int, float)):
            if comp == 0:
                return self.getWeightedMean(self.X)
            elif comp == 1:
                return self.getWeightedMean(self.Y)
            elif comp == 2:
                return self.getWeightedMean(self.Z)
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 0, 1, or 2!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                return self.getWeightedMean(self.X)
            elif comp.lower() == "y":
                return self.getWeightedMean(self.Y)
            elif comp.lower() == "z":
                return self.getWeightedMean(self.Z)
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try x, y, or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")


    def getPositionMeanLabframe(self,comp):    
        """ returns mean position of selected component (x,y,z or 0,1,2) of bunch in labframe [um] """
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 0:
                vector = self.X + self.windowStart
            elif comp == 1:
                vector = self.Y
            elif comp == 2:
                vector = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                vector = self.X + self.windowStart
            elif comp.lower() == "y":
                vector = self.Y
            elif comp.lower() == "z":
                vector = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        if isinstance(self.Weight, (float, int)):          
            return np.mean(vector)
        else:
            return (1./np.sum(self.Weight))*np.sum(self.Weight*vector)
            
            
            
            
            
    def getPositionMeanLabframeWeighted(self,comp):    
        """ returns mean position of selected component (x,y,z or 0,1,2) of bunch in labframe [um] """
        if isinstance(comp, (int, float)):
            if comp == 0:
                return (self.getWeightedMean(self.X) + self.windowStart)
            elif comp == 1:
                return self.getWeightedMean(self.Y)
            elif comp == 2:
                return self.getWeightedMean(self.Z)
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                return (self.getWeightedMean(self.X) + self.windowStart)
            elif comp.lower() == "y":
                return self.getWeightedMean(self.Y)
            elif comp.lower() == "z":
                return self.getWeightedMean(self.Z)
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try (x,y,z or 0,1,2)!")

       

    def getBunchLengthRMS(self):
        """ calculates the rms length of the bunch for Gaussian distributions """
        if self.Weight is 0:
            rmsBunchLength = np.std(self.X)
        else:
            N = np.sum(self.Weight)            
            rmsBunchLength = np.sqrt((1./N)*np.sum(self.Weight*self.X**2) - ((1./N)*np.sum(self.Weight*self.X))**2)
        return rmsBunchLength

    
    def getBunchLengthRMSWeighted(self):
        """ calculates the rms length of the bunch weighted by distribution """
        return self.getWeightedRms(self.X)
    
               
        
    def getBunchLengthMax(self):
        """ calculates the max length of the bunch """
        return np.max(self.X) - np.min(self.X)

    
    
    def getBunchWidthRMSWeighted(self, comp):
        """ calculates the rms width of the bunch for all distributions"""     
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 1:
                vector = self.Y
            elif comp == 2:
                vector = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 1, 2, y or z!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                vector = self.Y
            elif comp.lower() == "z":
                vector = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        return self.getWeightedRms(vector)


    def getBunchWidthRMS(self,comp):                
        """ calculates the rms width of the bunch for Gaussian distributions"""     
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 1:
                vector = self.Y
            elif comp == 2:
                vector = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 1, 2, y or z!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                vector = self.Y
            elif comp.lower() == "z":
                vector = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        if isinstance(self.Weight, (int, float)):
            rmsBunchWidth = np.std(vector)
        else:
            N = np.sum(self.Weight)            
            rmsBunchWidth = np.sqrt((1./N)*np.sum(self.Weight*vector**2) - ((1./N)*np.sum(self.Weight*vector))**2)
        return rmsBunchWidth
   
    
    def getBunchWidthMax(self, comp):
        """ calculating the max width of the bunch """
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 1:
                vector = self.Y
            elif comp == 2:
                vector = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 1, 2, y or z!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                vector = self.Y
            elif comp.lower() == "z":
                vector = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try 1, 2, y or z!")
        return np.max(vector)- np.min(vector)

        
        
    def getDivergenceRMS(self, comp):
        """ gives rms beam divergence (mrad), Gaussian"""
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 1:
                vector = self.YP
            elif comp == 2:
                vector = self.ZP
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 1, 2, y or z!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                vector = self.YP
            elif comp.lower() == "z":
                vector = self.ZP
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try y or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try y or z!")
        if isinstance(self.Weight, (int, float)):
            return np.std(vector)
        else:
            N = np.sum(self.Weight)            
            return np.sqrt((1./N)*np.sum(self.Weight*vector**2) - ((1./N)*np.sum(self.Weight*vector))**2)

        
        
    def getDivergenceRMSWeighted(self, comp):
        """ gives rms beam divergence (mrad) weighted by distribution"""
        vector = 0
        if isinstance(comp, (int, float)):
            if comp == 1:
                vector = self.YP
            elif comp == 2:
                vector = self.ZP
            else:
                raise Exception("   (!) Component " + str(comp) + " is not available! Check input argument, try 1, 2, y or z!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                vector = self.YP
            elif comp.lower() == "z":
                vector = self.ZP
            else:
                raise Exception("   (!) Component " + comp + " is not available! Check input argument, try y or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not available! Check input argument, try y or z!")
        return self.getWeightedRms(vector)


        
    def getEmittance(self, comp, switch):
        """ calculating the transverse normalized trace space emittance in [m rad] in chosen component """
        momentum = 0
        location = 0
                
     
        if isinstance(comp, (int, float)):        
            if comp == 1 :
                momentum = self.PY
                location = self.Y
            elif comp == 2:
                momentum = self.PZ
                location = self.Z
            else:
                raise Exception("   (!) Component " + str(comp) + " is not valid for the transverse trace space emittance, try 1, or 2!")
        elif isinstance(comp, basestring):
            if comp.lower() == "y":
                momentum = self.PY
                location = self.Y
            elif comp.lower() == "z":
                momentum = self.PZ
                location = self.Z
            else:
                raise Exception("   (!) Component " + comp + " is not valid for the transverse trace space emittance, try y, or z!")
        else:
            raise Exception("   (!) Component " + comp + " is not valid for the transverse trace space emittance, try y, or z!")

                
        weightCol = self.Weight
        if weightCol is 0:
            weightCol = 1
            n           = len(self.PX)
        else:
            n           = np.sum(weightCol)
            
        ySquaredMean            = (1./n)*np.sum(weightCol*(location**2)) - ((1./n)*np.sum(weightCol*location))**2
        pyOverPxSquaredMean     = (1./n)*np.sum(weightCol*(momentum/self.PX)**2) - ((1./n)*np.sum(weightCol*momentum/self.PX))**2
        yPyOverPxMeanSquared    = ( (1./n) * np.sum(weightCol * location * (momentum/self.PX)) - (1./n**2)*np.sum(weightCol*location) * np.sum(weightCol*(momentum/self.PX)) )**2
        gammaBeta               = (1./n)*np.sum(weightCol*self.PX)/elecDump._c
 

        meanY = np.mean(location)*1e-6
        delta = self.getMomentumDevRMS(momentum)/self.getMomentumMean(momentum)
        dispersionTerm = (meanY/delta)**2*self.getEnergySpreadRMS()**2
 
#        print "meanY = " +str(meanY)
#        print "delta = " +str(delta)
#        print "dispersionTerm = " +str(dispersionTerm)
        
        
        
        
        try:
            if switch == 0:
                emittance = gammaBeta * np.sqrt(ySquaredMean * pyOverPxSquaredMean - yPyOverPxMeanSquared)
            elif switch == 1:
                emittance = gammaBeta * np.sqrt(ySquaredMean * pyOverPxSquaredMean - yPyOverPxMeanSquared - dispersionTerm)
                
        except:
            emittance = 0.
        return emittance*1e-6
     
  



#################################### Helping/useful methods ################################################################        

    def getWeightedRms(self, vector):
        """ calculates weighted RMS spread for any electron beam phase space vector. Weighted means that PIC weight as well as arbitrary distributions are included (e.g. non-gaussian)"""
        nBins = 151
        binSize = (max(vector)-min(vector))/nBins
        bins = np.arange(min(vector), max(vector), binSize)
        rmsSpread = 0
        if self.Weight is 0:
            yHist = np.histogram(vector,bins)
            bins = np.delete(bins,len(bins)-1)
            yVector = yHist[0]
            xVector = bins
            rmsSpread = np.sqrt((np.sum(yVector*xVector**2))/np.sum(yVector)-(np.sum(yVector*xVector)/np.sum(yVector))**2)
        else:
            #similar to code above, a histogram of the input vector is created. however, influence of each particle is given by its weight (instead of 1)
            yVector = np.zeros(len(bins))
            for i in range(len(bins)):
                for j in range(len(vector)):
                    if vector[j] >=  bins[i] and vector[j] < bins[i] + binSize:
                        yVector[i] = yVector[i] + self.Weight[j] 
            xVector = bins
            rmsSpread = np.sqrt((np.sum(yVector*xVector**2))/np.sum(yVector)-(np.sum(yVector*xVector)/np.sum(yVector))**2)
        return rmsSpread


    def getWeightedMean(self, vector):
        """calculates the weighted mean value of given vector"""
        nBins = 151
        binSize = (max(vector)-min(vector))/nBins
        bins = np.arange(min(vector), max(vector), binSize)
        xVector = bins
        if self.Weight is 0:
            yHist = np.histogram(vector,bins)
            bins = np.delete(bins,len(bins)-1)
            yVector = yHist[0]
            xVector = bins
        else:
            #similar to code above, a histogram of the input vector is created. however, influence of each particle is given by its weight (instead of 1)
            yVector = np.zeros(len(bins))
            for i in range(len(bins)):
                for j in range(len(vector)):
                    if vector[j] >=  bins[i] and vector[j] < bins[i] + binSize:
                        yVector[i] = yVector[i] + self.Weight[j] 
        return np.sum(xVector*yVector)/np.sum(yVector)


                
    def getChargeHist(self):
       """calculates longitudinal charge histogram in pC/um"""
       axis = self.X
       if len(axis) < 10:
           print "   (!) Can't calculate charge histogram! Check if there are electrons (most likely cutting problem or early in injection process (e.g. 1-10 particles released))"
           return [[0], [0]]
       binSize = self.HistBinSize
       bins = np.arange(min(axis), max(axis), binSize)
       yHist = np.histogram(axis,bins)
       yVector = np.insert(yHist[0],0,0)
       xVector = np.insert(yHist[1],0,np.min(yHist[1])-binSize)
       xVector = np.delete(xVector,len(xVector)-1)
       if isinstance(self.Weight, (int, float)):
           yVector = yVector*elecDump._elemChar*self.numPtclsInMacro *1e12/binSize
       else:
           yVector = yVector*elecDump._elemChar*self.numPtclsInMacro *1e12*np.mean(self.Weight)/binSize
       if self.histSmooth == 1:
               yVector = self.smooth(yVector, len(yVector)/5)
       if yVector[len(yVector)-1] is not 0:
           yVector = np.append(yVector, 0)
           xVector = np.append(xVector, xVector[len(xVector)-1] + self.HistBinSize)
       return (xVector,yVector)

    def getCurrentHist(self):
       """calculates longitudinal charge histogram in kA"""
       axis = self.X
       if len(axis) < 10:
           print "   (!) Can't calculate current histogram! Check if there are electrons (most likely cutting problem or early in injection process (e.g. 1-10 particles released))"
           return [[0], [0]]
       binSize = self.HistBinSize
       bins = np.arange(min(axis), max(axis), binSize)
       yHist = np.histogram(axis,bins)
       yVector = np.insert(yHist[0],0,0)
       xVector = np.insert(yHist[1],0,np.min(yHist[1])-binSize)
       xVector = np.delete(xVector,len(xVector)-1)
       if isinstance(self.Weight, (int, float)):
           yVector = yVector*elecDump._elemChar*self.numPtclsInMacro *1e12/binSize
           yVector = yVector * elecDump._c * 1e-9
       else:
           yVector = yVector*elecDump._elemChar*self.numPtclsInMacro *1e12*np.mean(self.Weight)/binSize
           yVector = yVector * elecDump._c * 1e-9
       if self.histSmooth == 1:
               yVector = self.smooth(yVector, len(yVector)/5)
       if yVector[len(yVector)-1] is not 0:
           yVector = np.append(yVector, 0)
           xVector = np.append(xVector, xVector[len(xVector)-1] + self.HistBinSize)
       if yVector[0] is not 0:
           yVector = np.append([0], yVector)
           xVector = np.append([xVector[0] - self.HistBinSize], xVector)
       return (xVector,yVector)

        
   











