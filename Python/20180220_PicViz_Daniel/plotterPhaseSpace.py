# -*- coding: utf-8 -*-
"""
Created on Fri Dec 02 14:08:40 2016

@author: Paul Scherkl
"""

from plotter import *
import matplotlib as mpl
mpl.use('agg')

mpl.rcParams['font.family'] = 'DejaVu Sans'
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['font.sans-serif'] = "DejaVu Sans"
mpl.rcParams['mathtext.cal'] = 'DejaVu Sans'
mpl.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
mpl.rcParams['mathtext.rm'] = 'DejaVu Sans'
mpl.rcParams['text.usetex'] = False
hfont = {'fontname':'Courier'}


class plotterPhaseSpace(plotter):
    """plotter subclass that maintains phase space plots"""
    def __init__(self, manager):
        plotter.__init__(self, manager)
    
        self.tickSize     = 14
        self.tickNumberX  = 5
        self.tickNumberY  = 5
        self.labelSize    = 25
        self.transparency = 0.5
        self.labelX       = 0
        self.xLim         = []
        self.yLim         = []
    
        self.plotParticleColorbar   = 0
        self.labelParticleColorbar  = 0
        self.particleColorCodeList = []
        self.colbarLabelColor = "k"
        
        
#--------------------------------------------------------
#       Setters
#--------------------------------------------------------
    def setLabelParicleColorbat(self, label):
        self.labelParticleColorbar = label

    def setPlotParticleColorbar(self, switch):
        """switches on/off colorbar of particle dumps. Note: requires colormap to be activated!"""
        self.plotParticleColorbar = switch
        
     
    def setTickSize(self, size):
        """Sets the font size of all axes ticks"""
        if isinstance(size, (int, float)) and size > 0:
            self.tickSize = size

    def setTickNumberX(self, number):
        """Sets the amount of ticks shown at x-axis """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberX = number

    def setTickNumberY(self, number):
        """Sets the amount of ticks shown at x-axis """
        if isinstance(number, (int, float)) and number >= 0:
            self.tickNumberY = number
      
    def setLabelSize(self, size):
        """Sets the font size of all axes labels"""
        if isinstance(size, (int, float)) and size > 0:
            self.LabelSize = size
        else:
            raise Exception("Please enter a valid number > 0 for the size of axes labels")
    
    def setLabelX(self, label):
        self.labelX = label
        
    def setXLim(self, lim1, lim2):
        """defines range of x-axis"""
        self.xLim = [lim1,lim2]
 
    def setYLim(self, lim1, lim2):
        """defines range of y-axis"""
        self.yLim = [lim1,lim2]        
        
        
        
    def setupParticleColorbars(self, particleDumps, axis):
        """analyses electron dumps that have colorcode for number of colormaps that need to be generated. also draws axes for each color map"""
        manager = self._M
        for dump in particleDumps:
                if dump.getColorCodeComp() is not "" and len(dump.getColorCodeVector()) > 0:
                    minVal = np.min(dump.getColorCodeVector())
                    maxVal = np.max(dump.getColorCodeVector())
                    if self.particleColorCodeList == []:
                         self.particleColorCodeList = ([[dump],[dump.getColorCodeComp()],[0],[(minVal, maxVal)],[0]])
                    else:
                        self.particleColorCodeList[0].append(dump) 
                        self.particleColorCodeList[1].append(dump.getColorCodeComp()) 
                        self.particleColorCodeList[2].append(0) 
                        self.particleColorCodeList[3].append((minVal, maxVal)) 
                        self.particleColorCodeList[4].append(0) 
            
        testListDict = {}
        colBarCounter = 0 
        if len(self.particleColorCodeList) == 0:
            return
        for i in range(len(self.particleColorCodeList[1])):
          item = self.particleColorCodeList[1][i]
          try:
            testListDict[item] += 1
            #adjust min/max
            for j in range(i):
                if self.particleColorCodeList[1][j] == item: 
                    minVal = np.min((self.particleColorCodeList[3][i][0], self.particleColorCodeList[3][j][0]))
                    maxVal = np.max((self.particleColorCodeList[3][i][1], self.particleColorCodeList[3][j][1]))
                    self.particleColorCodeList[3][i] = (minVal, maxVal)                        
                    self.particleColorCodeList[3][j] = (minVal, maxVal)                        
                    self.particleColorCodeList[0][i].setColorMap(self.particleColorCodeList[0][j].colorMap)
          except:
            testListDict[item] = 1
            if self.plotParticleColorbar == 1:
                self.particleColorCodeList[4][i] = 1
                transFigure = self.figure.transFigure.inverted()
                coord1 = transFigure.transform(axis.transData.transform([np.min(axis.get_xlim()), np.max(axis.get_ylim())*0.785]))
                coord2 = transFigure.transform(axis.transData.transform([np.max(axis.get_xlim()), np.max(axis.get_ylim())*0.785]))
                cbaxes = self.figure.add_axes([coord1[0], 0.95 + colBarCounter*0.15, coord2[0]-coord1[0], 0.08], transform =self.figure.transFigure)                             
                self.particleColorCodeList[2][i] = cbaxes
                colBarCounter = colBarCounter + 1
        
        
    def getColorVector(self, dumpObject, **kwargs):
        """return color and transparency vector as RGBa matrix"""
        minColorMap = 0
        maxColorMap = 0
        color       = 0
        for key in kwargs:
            if key == "minColorMap":
                minColorMap = kwargs[key]
            if key == "maxColorMap":
                maxColorMap = kwargs[key]
            if key == "color":
                color = kwargs[key]
        
        transVec = dumpObject.getTransparencyVector()
        colorCodeVec = dumpObject.getColorCodeVector()
        if dumpObject.colorBarNorm == 0:
            if np.min(transVec) < np.max(transVec):
                dumpObject.colorBarNorm = [np.min(transVec), np.max(transVec)]
            else:
                dumpObject.colorBarNorm = [0.,1.]
        
        
        if colorCodeVec is not 0:
            cmap = cm.get_cmap(dumpObject.colorMap, 100)         
            cmap_vals = cmap(np.arange(cmap.N)) 
        
        
        outputColor = 0
        
        for i in range(len(dumpObject.X)):
            
            if colorCodeVec is not 0 :
                currentColor = cmap_vals[int(cmap.N*(colorCodeVec[i] -  minColorMap)/ ( maxColorMap - minColorMap))-1]
            else:
                RGBA = matplotlib.colors.colorConverter.to_rgba(color)                
                currentColor = [RGBA[0], RGBA[1], RGBA[2], 1.]
                
            
            if transVec[i] < np.min(dumpObject.colorBarNorm):
                currentColor[3] = dumpObject.reverseTransparency - 0.
            elif transVec[i] > np.max(dumpObject.colorBarNorm):
                if dumpObject.reverseTransparency == 1:
                    currentColor[3] = 0.
                else:
                    currentColor[3] = 1.
            else:
                if dumpObject.reverseTransparency == 1:
                    currentColor[3] =  1 - (transVec[i] -  np.min(dumpObject.colorBarNorm))/ ( np.max(dumpObject.colorBarNorm) - np.min(dumpObject.colorBarNorm))
                elif dumpObject.reverseTransparency == 0:
                    currentColor[3] =  (transVec[i] -  np.min(dumpObject.colorBarNorm))/ ( np.max(dumpObject.colorBarNorm) - np.min(dumpObject.colorBarNorm))
            if i == 0:
                outputColor = currentColor
            else:
                outputColor = np.vstack((outputColor, currentColor))
        return outputColor
        
        
    def drawParticleColorMap(self, dump, colbarAxis,  colormap, colBarLimits):
            if dump.colorCodeComp == 1 or dump.colorCodeComp.lower() == "x" :
                self.labelParticleColorbar = "x [$\mu$m]"
            if dump.colorCodeComp == 2 or dump.colorCodeComp.lower() == "y" :
                self.labelParticleColorbar = "y [$\mu$m]"
            if dump.colorCodeComp == 3 or dump.colorCodeComp.lower() == "z" :
                self.labelParticleColorbar = "z [$\mu$m]"
            if dump.colorCodeComp == 4 or dump.colorCodeComp.lower() == "px" :
                self.labelParticleColorbar =  "P$_{\mathrm{x}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 5 or dump.colorCodeComp.lower() == "py" :
                self.labelParticleColorbar = "P$_{\mathrm{y}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 6 or dump.colorCodeComp.lower() == "pz" :
                self.labelParticleColorbar = "P$_{\mathrm{z}}$ m$_{\mathrm{e}}$$^{-1}$ [m s$^{\mathrm{-1}}$]"
            if dump.colorCodeComp == 7 or dump.colorCodeComp.lower() == "yp" :
                self.labelParticleColorbar = "YP [mrad]"
            if dump.colorCodeComp == 8 or dump.colorCodeComp.lower() == "zp" :
                self.labelParticleColorbar = "ZP [mrad]"
            if dump.colorCodeComp == 9 or dump.colorCodeComp.lower() == "e" :
                self.labelParticleColorbar = "E$_{\mathrm{e}}$ [MeV]"
            if dump.colorCodeComp == 10 or dump.colorCodeComp.lower() == "tag" :
                self.labelParticleColorbar = "Tag"
            if dump.colorCodeComp == 11 or dump.colorCodeComp.lower() == "weight" :
                self.labelParticleColorbar = "Weight"
            if dump.colorCodeComp == 12 or dump.colorCodeComp.lower() == "ex" :
                self.labelParticleColorbar = "E$_{\mathrm{e,x}}$ [MeV]"
            if dump.colorCodeComp == 13 or dump.colorCodeComp.lower() == "ey" :
                self.labelParticleColorbar = "E$_{\mathrm{e,y}}$ [MeV]"
            if dump.colorCodeComp == 14 or dump.colorCodeComp.lower() == "ez" :
                self.labelParticleColorbar = "E$_{\mathrm{e,z}}$ [MeV]"
            if dump.colorCodeComp == 15 or dump.colorCodeComp.lower() == "etrans" :
                self.labelParticleColorbar = "E$_{\mathrm{e,trans}}$ [MeV]"

            ticks = np.linspace(colBarLimits[0], colBarLimits[1]*0.9, 5)
            ticks = np.round(ticks, 2)
        
            if dump.transparencyVector == 0: # plot data with running transparency
                cbar1 = self.figure.colorbar(colormap,cax=colbarAxis, orientation='horizontal', ticks = ticks)      
            else:
                cmap = cm.get_cmap(dump.colorMap, 100)  
                norm = matplotlib.colors.Normalize(vmin=colBarLimits[0], vmax=colBarLimits[1])
                cbar1 = matplotlib.colorbar.ColorbarBase(colbarAxis, cmap=cmap, norm=norm, orientation='horizontal', ticks = ticks)
           
               
            colbarAxis.text(0.43, (np.max(colbarAxis.get_ylim())-np.min(colbarAxis.get_ylim()))/3.3, self.labelParticleColorbar,   color = self.colbarLabelColor, size=20)
            cbar1.solids.set(alpha=1)

            ticklabels = colbarAxis.get_xticklabels()
            

            minVal = colBarLimits[0]
            maxVal = colBarLimits[1]
            step = (maxVal-minVal)/len(ticklabels)
            if minVal < -999.9 or maxVal > 999.9:
                colbarAxis.set_xticklabels(["{0:.1e}".format(round(minVal + i*step,2)) for i in range(len(ticklabels))])

            for label in ticklabels:
                label.set_fontsize(self.tickSize)
       
        
        
    def plotSingle(self, bunch, comp):
        """ plots phase space of selected component (x,y,z or 0,1,2) of bunch (driver, or for witnesses (0-n, or witness name)
        Note: instead of a single bunch, you can also include a list e.g. ([HeElectrons, NeElectrons,...] or [0,2,5])"""
        particleDumps = []
        if isinstance(bunch, (list,tuple)):
            for i in range(len(bunch)):
                if self._M.getParticleDump(bunch[i]).loaded and len(self._M.getParticleDump(bunch[i]).X) > 0:
                    particleDumps.append(self._M.getParticleDump(bunch[i]))
        else:
            if self._M.getParticleDump(bunch).loaded and len(self._M.getParticleDump(bunch).X) > 0:
                particleDumps.append(self._M.getParticleDump(bunch))
        if len(particleDumps) == 0:
            return
        
        component = 0
        if isinstance(comp, (int, float)):
            if comp < 4:
                component = comp
            else:
                raise Exception("Component " + str(comp) + " is not available! Check input argument, try 0, 1, 2, or 3!")
        elif isinstance(comp, basestring):
            if comp.lower() == "x":
                component = 0
            elif comp.lower() == "y":
                component = 1
            elif comp.lower() == "z":
                component = 2
            elif comp.lower() == "t":
                component = 3
            else:
                raise Exception("Component " + comp + " is not available! Check input argument, try x, y, z, or t!")
        if len(self.figSize) == 0:
            self.figSize = [5,5]
        self.figure = plt.figure(figsize= self.figSize)
        gs    = gridspec.GridSpec(1, 1)
        axis   = self.figure.add_subplot(gs[0])      
        self.setupParticleColorbars(particleDumps, axis)
        self.plot(particleDumps, component, axis)

        
        
    def plot(self, elecBunchList, component, axis):

        
        axis.xaxis.set_major_locator(MaxNLocator(self.tickNumberX))
        axis.yaxis.set_major_locator(MaxNLocator(self.tickNumberY))


        ticklabels = axis.get_xticklabels()
        for label in ticklabels:
            label.set_fontsize(self.tickSize)
            
        ticklabels = axis.get_yticklabels()
        for label in ticklabels:
            label.set_fontsize(self.tickSize)
        
        dataGotPlottet = 0   
        for i in range(len(elecBunchList)):    
            if elecBunchList[i].loaded == False:
                print "ElecBunch " +str(elecBunchList[i].name) + " not loaded, skip phase space plotting"
                continue
            
            dataGotPlottet = 1
            xAxis = 0
            yAxis = 0
            xLabel = ""
            yLabel = ""
            if component == 0: #get longitudinal phase space
                xAxis     = elecBunchList[i].X
                yAxis     = elecBunchList[i].E
                xLabel    = '$\mathrm{\\xi \\ [\mu m]}$'
                yLabel    = '$\mathrm{E \\ [MeV]}$'
                compstr = "x"
                 
            if component == 1:
                xAxis = elecBunchList[i].Y
                yAxis = elecBunchList[i].YP
                xLabel    = '$\mathrm{y \\ [\mu m]}$'
                yLabel    = '$\mathrm{y\' \\ [mrad]}$'
                compstr = "y"
    
            if component == 2:
                xAxis = elecBunchList[i].Z
                yAxis = elecBunchList[i].ZP
                xLabel    = '$\mathrm{z \\ [\mu m]}$'
                yLabel    = '$\mathrm{z\' \\ [mrad]}$'
                compstr = "z"

            if component == 3:
                xAxis = elecBunchList[i].T -  np.max(elecBunchList[i].T)/2.0
                yAxis = elecBunchList[i].E
                xLabel    = '$\mathrm{t \\ [fs]}$'
                yLabel    = '$\mathrm{E \\ [MeV]}$'
                compstr = "t"

            if self.labelX is not 0:
                xLabel = self.labelX

            
            if self.export:
                a = np.column_stack((xAxis, yAxis))
                if component == 0:
                    comp = "x"
                elif component == 1:
                    comp = "y"
                elif component == 2:
                    comp = "z"
                elif component == 3:
                    comp = "t"
                    
                self.exportData(a, elecBunchList[i].name + "_PhaseSpace_" + comp)
                if self.dumpNote:
                    print "          Phase space coordinates for " + elecBunchList[i].name + " dumped"
                
            
            axis.set_xlabel(xLabel,fontsize=self.labelSize)
            axis.set_ylabel(yLabel,fontsize=self.labelSize)

            delStep = np.int((1/elecBunchList[i].plotParticleRatio))

            if isinstance(elecBunchList[i].getColorCodeVector(), (int,float)):
                marker = elecBunchList[i].plotMarker
    
                if len(marker) > 1:
                    color = marker[:-1]
#               
                if elecBunchList[i].transparencyVector is not 0:   
                    colorvector = self.getColorVector(elecBunchList[i], color = color)[0::delStep]
                    axis.scatter(xAxis[0::delStep], yAxis[0::delStep], c= colorvector,   s = elecBunchList[i].plotMarkerSize, edgecolor='')
                else:
                    axis.scatter(xAxis[0::delStep], yAxis[0::delStep], c= color,   s = elecBunchList[i].plotMarkerSize, alpha =elecBunchList[i].transparency, edgecolor='')
                   
                   
                    
                    
                    
                    
                    
                    
                    
                    
            else:
#                if len(self.particleColorCodeList) == 0:
#                    return 
                for j in range(len(self.particleColorCodeList[0])):
                    if self.particleColorCodeList[0][j].getName() == elecBunchList[i].getName():
                        
                        
#                        normalize = matplotlib.colors.Normalize(vmin= self.particleColorCodeList[3][j][0], vmax= self.particleColorCodeList[3][j][1])
                        
                        minVal = self.particleColorCodeList[3][j][0]
                        maxVal = self.particleColorCodeList[3][j][1]
                        xVec = xAxis[0::delStep]
                        yVec = yAxis[0::delStep]
                        colCodeVec = elecBunchList[i].getColorCodeVector()[0::delStep]

                        if elecBunchList[i].transparencyVector is not 0:   
                            colorvector = self.getColorVector(elecBunchList[i], minColorMap = minVal, maxColorMap = maxVal)[0::delStep]
                            colormap = axis.scatter(xVec, yVec, c = colorvector, s = elecBunchList[i].plotMarkerSize, edgecolor="")
                            
                            
                            
                        else:
                            colormap = axis.scatter(xVec, yVec, c= colCodeVec, cmap = elecBunchList[i].colorMap , vmin = minVal, vmax = maxVal, s = elecBunchList[i].plotMarkerSize, alpha = elecBunchList[i].transparency, edgecolor='')
                        
                        
                        
                        
                        if self.particleColorCodeList[4][j] == 1:
                            
                            self.drawParticleColorMap(self.particleColorCodeList[0][j], self.particleColorCodeList[2][j],  colormap, self.particleColorCodeList[3][j])
                            break
                        
                        
                
        if len(self.xLim) == 2:
            axis.set_xlim(self.xLim)
        if len(self.yLim) == 2:
            axis.set_ylim(self.yLim)
            
        if self.plotPropLen:
            runTime  = 0
            currentX = 0
            for i in range(len(elecBunchList)):
                if elecBunchList[i].loaded:
                    currentX = elecBunchList[i].getWindowEnd()/1000
                    runTime = elecBunchList[i].runTime*1e12   
                    break
            if self.plotPropLen == 1:
                axis.text((np.max(axis.get_xlim())- np.min(axis.get_xlim()))*self.propLenPosX + np.min(axis.get_xlim()), np.min(axis.get_ylim()) + np.abs((np.max(axis.get_ylim())-np.min(axis.get_ylim())))*self.propLenPosY, str("{0:.1f}".format(np.round(currentX,2))) + " mm", color= self.propLenColor, size=15)
            elif self.plotPropLen == 2:
                if runTime == 0:
                    runTime = currentX/3e8/1000*1e12
                axis.text((np.max(axis.get_xlim())- np.min(axis.get_xlim()))*self.propLenPosX + np.min(axis.get_xlim()), np.min(axis.get_ylim()) + np.abs((np.max(axis.get_ylim())-np.min(axis.get_ylim())))*self.propLenPosY, "t $_{\mathbf{sim}}$ = " + str("{0:.1f}".format(np.round(runTime,2))) + " ps", color= self.propLenColor, size=15)
            
            
        if dataGotPlottet == 1:    
            if self.saveFigures == 1:
                    outName = ""
                    for i in range(len(elecBunchList)):
                        if i == 0:
                            outName = elecBunchList[i].getName()
                        if i > 0:
                            outName = outName + "_" + elecBunchList[i].getName()

#                    dumpName = elecBunchList[0].getName()
                    if self.outFileFormat == "pdf":
                          plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_" + outName + "_" + str(compstr) +".pdf",format = "pdf", dpi = self.DPI, bbox_inches='tight')
                    elif self.outFileFormat == "png" or self.outFileFormat == "jpg":
                          plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_" + outName + "_" + str(compstr) + "." + self.outFileFormat, dpi=self.DPI, bbox_inches='tight')         
                    elif self.outFileFormat == "svg":
                          plt.savefig(self._M.outPath + self._M.prefix + "_" + str(self._M.dumpNumber) + "_"  + outName + "_" + +str(compstr) + ".svg", format = "svg", dpi=self.DPI, bbox_inches='tight')         
            plt.show() 
        self.figure.clf()
        self.particleColorCodeList = []

        
        
