# -*- coding: utf-8 -*-
"""
Created on Wed Feb 03 18:08:18 2016

@author: P. Scherkl, O. Karger
"""
import h5py
from elecDump import elecDump
from fieldDump import fieldDump
import numpy as np



class dataManager(object):
    """class loads several types of VSim .h5 dumps and saves relevant grid, field, and particle distribution parameters"""
    
    def __init__(self, particleDumpList, fieldDumpList, filePath, prefix):
        """initialization of object "dataManager". also summarizes all object variables"""
        self.beamElectrons              = elecDump("driver")      
        self.dummyParticles             = elecDump("dummy")      
        self.particleSpecies            = [] # saves bunch objects
        self.particleDumpList           = particleDumpList
        self.particleDumpListDummy      = []
        markers=['r.', 'b.', 'g.', 'k.', 'y.'] 
        for i in range(len(particleDumpList)):
            newWitnessSpecies = elecDump(particleDumpList[i]) 
            self.particleDumpListDummy.append(particleDumpList[i])
            try:
                newWitnessSpecies.setPlotMarker(markers[i])
            except:
                pass
            self.particleSpecies.append(newWitnessSpecies)
        self.beamElectrons.setPlotMarker("b.")


        self.fieldSpecies            = [] # saves bunch objects
        self.fieldDumpList           = fieldDumpList
        self.fieldDumpListDummy      = []
        self.dummyField              = fieldDump("dummy")
        for i in range(len(fieldDumpList)):
            newFieldDump = fieldDump(fieldDumpList[i]) 
            self.fieldDumpListDummy.append(fieldDumpList[i])
            self.fieldSpecies.append(newFieldDump)
            
        self.filePath                     = filePath
        self.prefix                       = prefix  
        self.dumpNumber                   = 0
        self.offsetX                      = 0


        self.outPath = ""
        
        
        
        
    def setOffsetX(self, offset):
        """moves xAxis of plots"""
        self.offsetX = offset
        

    def setOutpath(self, path):
        import os
        if not isinstance(path, basestring ):
            print ("   (!) Given variable " + str(path) +" is not a valid string for Outpath, command ignored")
            self.outPath = self.filePath
            return
        else:
            if not os.path.isdir(path):
                try:
                    os.makedirs(path)
#                    print ("   Output directory " + path + " created")
                except:
                    print ("   (!) Could no create directory " + path +" for Outpath, command ignored")
                    self.outPath = self.filePath
                    return
                    
            
        if path[-1] =="/" and path[-2] =="/":
            self.outPath = path
        else:
            self.outPath = path + "//"
                
        
    def loadFieldDump(self, numField):
        """loads field from given field.h5 file and also saves grid properties of the simulation box. 
        Note: grid units are [um] and transformed in co-moving coordinates!"""
        #dataSet = [pathToData, prefix, witnessSpecies, dumpNumbers]
        eFieldFile = self.filePath + self.prefix  + "_" + self.fieldDumpList[numField] + "_" + str(self.dumpNumber) + ".h5"
        fieldName = self.fieldDumpList[numField]
        if h5py.is_hdf5(eFieldFile) == False or eFieldFile.find(".h5") == -1:
            print ("   (!) No " + fieldName + "_" + str(self.dumpNumber) +".h5 file found, loading skipped")
            
        else:
            newFieldObject = self.fieldSpecies[numField]   
            f = h5py.File(eFieldFile)
            limits =  f[fieldName].attrs["vsLimits"]
            mesh = f[fieldName].attrs["vsMesh"]
            try:
                matrix = np.array(f[fieldName], dtype=np.float64) 
#                newFieldObject.fieldMatrix = np.delete(np.delete(matrix,(-1),axis=0),(0),axis=0)
                newFieldObject.fieldMatrix = matrix
            except:
                print "   (!) Corrupted file or unsufficient memory available for loading field. Please use python 64 bit or increase your memory!"
            if fieldName == "ElecMultiField" or fieldName =="lasersPlusPlasma" or fieldName =="lasers" or fieldName == "edgeE" or fieldName == "ElecFieldBeam" or fieldName == "ElecFieldPlasma":
                newFieldObject.fieldType = "electric"
                newFieldObject.fieldMatrix[:,:,:,0] = newFieldObject.fieldMatrix[:,:,:,0]/1e9
                newFieldObject.fieldMatrix[:,:,:,1] = newFieldObject.fieldMatrix[:,:,:,1]/1e9
                newFieldObject.fieldMatrix[:,:,:,2] = newFieldObject.fieldMatrix[:,:,:,2]/1e9
            newFieldObject.numCellX  = f[mesh].attrs["vsNumCells"][0] + 1
            newFieldObject.numCellY  = f[mesh].attrs["vsNumCells"][1] + 1
            newFieldObject.numCellZ  = f[mesh].attrs["vsNumCells"][2] + 1
            newFieldObject.windowStart = f[mesh].attrs["vsLowerBounds"][0]*1e6
            newFieldObject.windowEnd   = f[mesh].attrs["vsUpperBounds"][0]*1e6
            newFieldObject.runTime   = f["time"].attrs["vsTime"]
            newFieldObject.cellSizeX = (f[limits].attrs["vsUpperBounds"][0]-f[limits].attrs["vsLowerBounds"][0])/newFieldObject.numCellX*1e6
            newFieldObject.cellSizeY = (f[limits].attrs["vsUpperBounds"][1]-f[limits].attrs["vsLowerBounds"][1])/newFieldObject.numCellY*1e6
            newFieldObject.cellSizeZ = (f[limits].attrs["vsUpperBounds"][2]-f[limits].attrs["vsLowerBounds"][2])/newFieldObject.numCellZ*1e6
            
            newFieldObject.centerCellTransY = (newFieldObject.numCellY-1)/2 + 1
            newFieldObject.centerCellTransZ = (newFieldObject.numCellZ-1)/2 + 1
            newFieldObject.xAxis = np.linspace(0, (f[limits].attrs["vsUpperBounds"][0]-f[limits].attrs["vsLowerBounds"][0])*1e6, newFieldObject.numCellX, endpoint=True) + self.offsetX
            newFieldObject.yAxis = np.linspace(f[limits].attrs["vsLowerBounds"][1]*1e6, f[limits].attrs["vsUpperBounds"][1]*1e6, newFieldObject.numCellY, endpoint=True)
            newFieldObject.zAxis = np.linspace(f[limits].attrs["vsLowerBounds"][2]*1e6, f[limits].attrs["vsUpperBounds"][2]*1e6, newFieldObject.numCellZ, endpoint=True)
            
            f.close()  
            
            newFieldObject.loaded = 1
            
            multiCheck =  [i for i, name in enumerate(self.fieldDumpList) if name == newFieldObject.name]
            if len(multiCheck) > 1:
                newFieldObject.name = newFieldObject.name + str(numField)
                self.fieldDumpList[numField] = newFieldObject.name
           
            print "   Loading of "+ self.fieldDumpList[numField] + "_" + str(self.dumpNumber)+".h5 completed."     
        
    
       
    def loadBeamElectrons(self):
        """loads driver electron beam distribution. converts all spatial coordinates to [um] and transforms to co-moving frame"""
        beamElectronFile = self.filePath + self.prefix  +"_BeamElectrons_" + str(self.dumpNumber) +".h5"
        if h5py.is_hdf5(beamElectronFile) == False or beamElectronFile.find(".h5") == -1:
            print("   (!) No BeamElectrons.h5 file found, loading skipped")
        else:
            f = h5py.File(beamElectronFile)
            beamElectronData = np.array(f["BeamElectrons"], dtype=np.float32)  
#            limits =  "compGridGlobalLimits"
            limits =  f["BeamElectrons"].attrs["vsLimits"]
            self.beamElectrons.windowStart = f[limits].attrs["vsLowerBounds"][0]*1e6
            self.beamElectrons.windowEnd   = f[limits].attrs["vsUpperBounds"][0]*1e6
            self.beamElectrons.runTime   = f["time"].attrs["vsTime"]
            self.beamElectrons.numPtclsInMacro = int(round(f["BeamElectrons"].attrs["numPtclsInMacro"]))
            f.close()           
            self.beamElectrons.X      = (beamElectronData[:,0])*1e6 - self.beamElectrons.windowStart + self.offsetX
            if len(self.beamElectrons.X) < 10:
                self.beamElectrons.X = 0
                print("   (!) Less than 10 macroparticles in BeamElectrons, loading skipped")
            else: 
                self.beamElectrons.Y      = beamElectronData[:,1]*1e6
                self.beamElectrons.Z      = beamElectronData[:,2]*1e6
                self.beamElectrons.PX     = beamElectronData[:,3]
                self.beamElectrons.PY     = beamElectronData[:,4]
                self.beamElectrons.PZ     = beamElectronData[:,5]
                self.beamElectrons.YP     = self.beamElectrons.PY/self.beamElectrons.PX*1000
                self.beamElectrons.ZP     = self.beamElectrons.PZ/self.beamElectrons.PX*1000
                self.beamElectrons.T      = self.beamElectrons.X/299792458.0*1e9
                self.beamElectrons.T      = np.max(self.beamElectrons.T) - self.beamElectrons.T
                gammaCol                  = np.sqrt((self.beamElectrons.PX**2+self.beamElectrons.PY**2+self.beamElectrons.PZ**2)/(299792458.0**2))
                self.beamElectrons.E      = 0.511*(np.sqrt(1+gammaCol**2)-1)
                self.beamElectrons.EX = 0.511*(np.sqrt(1+(self.beamElectrons.PX/299792458.)**2-1))   
                self.beamElectrons.EY = 0.511*(np.sqrt(1+(self.beamElectrons.PZ/299792458.)**2-1))   
                self.beamElectrons.EZ = 0.511*(np.sqrt(1+(self.beamElectrons.PY/299792458.)**2-1))   
                self.beamElectrons.Etrans = np.sqrt(self.beamElectrons.EY**2+self.beamElectrons.EZ**2)
   
                self.beamElectrons.loaded = 1
                print "   Loading of BeamElectrons" + "_" + str(self.dumpNumber) + ".h5 completed."     
        
         
    def loadWitnessSpecies(self, numSpecies):
        """loads witness electron distribution(s). converts all spatial coordinates to [um] and transforms to co-moving frame"""
        witnessSpeciesFile = self.filePath + self.prefix  + "_" + self.particleDumpList[numSpecies] + "_" + str(self.dumpNumber) +".h5"
        if h5py.is_hdf5(witnessSpeciesFile) == False or witnessSpeciesFile.find(".h5") == -1:
                print("   (!) No "+ self.particleDumpList[numSpecies] +".h5 file found, loading skipped")
        else:
            f = h5py.File(witnessSpeciesFile)
            limits =  f[self.particleDumpList[numSpecies]].attrs["vsLimits"]
            witnessSpeciesData = np.array(f[self.particleDumpList[numSpecies]], dtype=np.float32)
            newWitnessSpecies = self.particleSpecies[numSpecies]   
            newWitnessSpecies.windowStart = f[limits].attrs["vsLowerBounds"][0]*1e6
            newWitnessSpecies.windowEnd   = f[limits].attrs["vsUpperBounds"][0]*1e6
            newWitnessSpecies.runTime   = f["time"].attrs["vsTime"]
            newWitnessSpecies.numPtclsInMacro = int(round(f[self.particleDumpList[numSpecies]].attrs["numPtclsInMacro"]))
            f.close() 
            newWitnessSpecies.X      = (witnessSpeciesData[:,0])*1e6 - newWitnessSpecies.windowStart + self.offsetX
            if len(newWitnessSpecies.X) < 10:
                newWitnessSpecies.X = 0
                print("   (!) Less than 10 macroparticles in "+ self.particleDumpList[numSpecies] +", loading skipped")
            else: 
                newWitnessSpecies.Y      = witnessSpeciesData[:,1]*1e6
                newWitnessSpecies.Z      = witnessSpeciesData[:,2]*1e6
                newWitnessSpecies.PX     = witnessSpeciesData[:,3]   
                newWitnessSpecies.PY     = witnessSpeciesData[:,4]
                newWitnessSpecies.PZ     = witnessSpeciesData[:,5]
                try: # in case that you want to load a driver beam as witness, the try avoids errors
                    newWitnessSpecies.Tag    = witnessSpeciesData[:,6]
                    newWitnessSpecies.Weight = witnessSpeciesData[:,7]
                except:
                    newWitnessSpecies.Tag    = 0      
                    newWitnessSpecies.Weight = 0
                try:
                    newWitnessSpecies.YP     = newWitnessSpecies.PY/newWitnessSpecies.PX*1000
                    newWitnessSpecies.ZP     = newWitnessSpecies.PZ/newWitnessSpecies.PX*1000
                except:
                    newWitnessSpecies.YP     = np.zeros(len(newWitnessSpecies.YP))
                    newWitnessSpecies.ZP     = np.zeros(len(newWitnessSpecies.ZP))
                    
                newWitnessSpecies.T      = newWitnessSpecies.X/299792458.0*1e9
                newWitnessSpecies.T      = np.max(newWitnessSpecies.T) - newWitnessSpecies.T
                gammaCol                 = np.sqrt((newWitnessSpecies.PX**2+newWitnessSpecies.PY**2+newWitnessSpecies.PZ**2)/(299792458.0**2))
                newWitnessSpecies.E      = 0.511*(np.sqrt(1+gammaCol**2)-1)
                newWitnessSpecies.loaded = 1
                newWitnessSpecies.EX = 0.511*(np.sqrt(1+(newWitnessSpecies.PX/299792458.)**2-1))   
                newWitnessSpecies.EY = 0.511*(np.sqrt(1+(newWitnessSpecies.PZ/299792458.)**2-1))   
                newWitnessSpecies.EZ = 0.511*(np.sqrt(1+(newWitnessSpecies.PY/299792458.)**2-1))   
                newWitnessSpecies.Etrans = np.sqrt(newWitnessSpecies.EY**2+newWitnessSpecies.EZ**2)
                
                if len(newWitnessSpecies.historyTags) > 0 and newWitnessSpecies.particleTrajectories == []:
                    newWitnessSpecies = self.loadHistoryFile(newWitnessSpecies)
                    
                
                multiCheck =  [i for i, name in enumerate(self.particleDumpList) if name == newWitnessSpecies.name]
                if len(multiCheck) > 1:
                    newWitnessSpecies.name = newWitnessSpecies.name + str(numSpecies)
                    self.particleDumpList[numSpecies] = newWitnessSpecies.name
                print "   Loading of "+ self.particleDumpList[numSpecies] + "_" + str(self.dumpNumber)+".h5 completed."     
                
                
                
                
    def loadHistoryFile(self, particleSpecies):
        histFile = self.filePath + self.prefix  + "_" + "History.h5"
        if isinstance(particleSpecies.Tag, (int, float)):
            print "   (!) Selected species " + particleSpecies.name + " does not have Tag-column. No history history/tracking possible"
            return particleSpecies 
            
        if h5py.is_hdf5(histFile) == False:
                print("   (!) No history.h5 file found, loading skipped")
        else:
                     
                f = h5py.File(histFile) 
                historyData = np.array(f[particleSpecies.historyName], dtype=np.float32)
                particleSpecies.timeStep = (f["timeSeries"].attrs["vsUpperBounds"] - f["timeSeries"].attrs["vsLowerBounds"] )/f["timeSeries"].attrs["vsNumCells"]
                f.close()
                particleSpecies.timeStep = particleSpecies.timeStep[0]
                
                #2BD: if appropriate, read in min and max tag from history file to make them accessible by user
                # make sure that individual particle tag corresponds to a specific entry in history file, particularly if just a few particles are tracked (e.g. tag = [1,5,10])
                for tag in particleSpecies.historyTags:
                    tag = int(tag)
                    currentCol = historyData[:,tag,:]*1e6
                    currentCol[:,0] =  currentCol[:,0]   + self.offsetX
                    if np.min(currentCol) == np.max(currentCol):
                        particleSpecies.particleTrajectories.append(0)                       
                    else:
                        particleSpecies.particleTrajectories.append(currentCol)
                        
                print "   Loading of history file completed"
        return particleSpecies
            
        
            
    def getParticleDump(self, bunch):
        """returns particleDump object for several types argument.Valid input is either an integer or the name of the dump (e.g. HeElectrons or driver)"""
        
        if isinstance(bunch, elecDump):
            return bunch
            
        elif isinstance(bunch, (int, float)) and  bunch < len(self.particleDumpList):
            if isinstance(self.particleSpecies[bunch] , elecDump):
                return self.particleSpecies[bunch]        
#            else:
#                print("   (!) ElectronDump object with the given argument " + str(bunch) +" not loaded.")

        elif isinstance(bunch, basestring):
            if bunch.lower().find("driver") > -1:
                if isinstance(self.beamElectrons, elecDump):
                    return self.beamElectrons
#                else:
#                    raise Exception("   (!) ElectronDump object with the given argument " + str(bunch) +" not loaded.")
            else:
                electronDump = next((x for x in self.particleSpecies if x.name.lower() == bunch.lower()), None)
                return electronDump

                   
        else:
#            print "   (!) ElectronDump object with the given argument " + str(bunch) +" not found. Please use a valid integer smaller than " +str(len(self.particleSpecies))+" or its name (e.g \"driver\" or \"HeElectrons\"). Dummy returned"
            return self.dummyParticles




    def getFieldDump(self, field):
        """returns fieldDump object for several types argument.Valid input is either an integer or the name of the dump (e.g. lasersPlusPlasma)"""
        if isinstance(field, (int, float)) and  field < len(self.fieldDumpList):
                return self.fieldSpecies[field]        
        elif isinstance(field, basestring):
                fieldDump = next((x for x in self.fieldSpecies if x.name == field), None)
                return fieldDump
        else:
#            print "   (!) FieldDump object with the given argument " + str(field) +" not found. Please use a valid integer smaller than " +str(len(self.fieldSpecies))+" or its name (e.g \"lasersPlusPlasma\" or \"ElecMultiField\"). Dummy returned"
            return self.dummyField
            
        
        
        

            
    def getElecMultiField(self):
        """returns elecMultiFiled object if loaded"""
        for i in range(len(self.fieldSpecies)):
            if self.fieldSpecies[i].name == "ElecMultiField":
                return self.fieldSpecies[i]
        return self.dummyField
                

            
    def getLasersPlusPlasma(self):
        """returns lasersPlusPlasma object if loaded"""
        for i in range(len(self.fieldSpecies)):
            if self.fieldSpecies[i].name == "lasersPlusPlasma":
                return self.fieldSpecies[i]
        return self.dummyField
            
    
    def getSumRhoJ(self):
        """returns sumRhoJ object if loaded"""
        for i in range(len(self.fieldSpecies)):
            if self.fieldSpecies[i].name == "SumRhoJ":
                return self.fieldSpecies[i]
        return self.dummyField
