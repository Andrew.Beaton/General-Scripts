


#Server Secure script Ubuntu 

echo Starting Secure Script: Updating 

sudo apt update

sudo apt upgrade

echo " To skip a setup step leave the input blank"

echo "Update Complete ; Starting Securing process "
echo "Please enter desired Username..."
read username

if [ -z "$username"]; then
	echo "No user created"
else
	adduser $username
	usermod -aG sudo $username 

	#echo Setting SSH key


fi

echo "Enter username for rsa creation"
read username2
if  [ -z "$username2"]; then
	echo "No rsa key created"
else

	ssh-keygen -t rsa C "$username2" -b 4096
	mkdir ~/.ssh; touch ~/.ssh/authorized_keys; chmod 700 ~/.ssh
	echo OK $username RSA key located in /home/your_username/.ssh by default
	echo It is recommended to edit /etc/ssh/sshd_config to remove root login and password authentication for SSH
fi

echo installing Fail2ban


sudo apt install fail2ban

sudo ufw allow ssh
sudo ufw enable

echo Creating fail2ban config copy : Please edit  /etc/fail2ban/fail2ban.local
sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
 
echo Basic security sorted 



echo "Do you want to install pywal ?(y/n)"
read pyoption
tt="y"
if [ $pyoption = $tt ];then


	echo Adding pywal stuff

	sudo apt install python3-pip

	sudo pip3 install pywal

	echo 'PATH="${PATH}:${HOME}/.local/bin/"' >> ~/.bashrc



	echo '# Import colorscheme from 'wal' asynchronously
	# &   # Run the process in the background.
	# ( ) # Hide shell job control messages.
	(cat ~/.cache/wal/sequences &)

	# Alternative (blocks terminal for 0-3ms)
	cat ~/.cache/wal/sequences

	# To add support for TTYs this line can be optionally added.
	source ~/.cache/wal/colors-tty.sh' >> ~/.bashrc



	echo '# Add this to your .xinitrc or whatever file starts programs on startup.
	# -R restores the last colorscheme that was in use.
	wal -R' >> ~/.profile

else
	echo 'Not installing pywal'
fi


echo 'Adding Standard Aliass'


echo "alias upd='sudo apt-get update && sudo apt-get upgrade -y '
alias edison='ssh abeaton@edison.nersc.gov'
alias ..='cd ..'
alias ...='cd ../../'
alias c='clear'
alias wget='wget -c'
alias hist='history | grep '
alias explore='nautilus .'
alias size='du -sh'

"

echo "alias upd='sudo apt update && sudo apt upgrade '
alias edison='ssh abeaton@edison.nersc.gov'
alias ..='cd ..'
alias ...='cd ../../'
alias c='clear'
alias wget='wget -c'
alias hist='history | grep '
alias explore='nautilus .'
alias size='du -sh' " >> ~/.bashrc

source ~/.bashrc
