


#Server Secure script Ubuntu 

echo Starting Secure Script: Updating 

sudo apt update && sudo apt upgrade -y


echo Update Complete : Starting Securing process 
echo Please enter desired Username...
read username

adduser $username
usermod -aG sudo $username 

#echo Setting SSH key

ssh-keygen -t rsa C "$username" -b 4096
mkdir ~/.ssh; touch ~/.ssh/authorized_keys; chmod 700 ~/.ssh
echo OK $username RSA key located in /home/your_username/.ssh by default
echo It is recommended to edit /etc/ssh/sshd_config to remove root login and password authentication for SSH


echo installing Fail2ban


sudo apt install fail2ban

sudo ufw allow ssh
sudo ufw enable

echo Creating fail2ban config copy : Please edit  /etc/fail2ban/fail2ban.local
 cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
 
echo Basic security sorted 


