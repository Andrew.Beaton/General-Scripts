#!/usr/bin/env bash

sudo apt update

sudo apt-get install cmake libx11-dev xorg-dev libglu1-mesa-dev freeglut3-dev libglew1.5 libglew1.5-dev libglu1-mesa libglu1-mesa-dev libgl1-mesa-glx libgl1-mesa-dev

cd ~/Documents

mkdir Geant4Install
cd Geant4Install

mkdir GeantBuild

wget http://cern.ch/geant4-data/releases/geant4.10.04.p02.tar.gz

tar xvzf geant4.10.04.p02.tar.gz

cd GeantBuild
cmake -DCMAKE_INSTALL_PREFIX=~/Documents/Geant4Install/ ~/Documents/Geant4Install/geant4.10.04.p02 -DGEANT4_INSTALL_DATA=ON -DGEANT4_USE_OPENGL_X11=ON

make -j 4
make install


cd ~/Documents/Geant4Install/GeantBuild



username="$(id -u -n)"




export G4NEUTRONHPDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4NDL4.5
export G4LEDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4EMLOW7.3
export G4LEVELGAMMADATA=/home/$username/Documents/Geant4Install/GeantBuild/data/PhotonEvaporation5.2
export G4RADIOACTIVEDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/RadioactiveDecay5.2
export G4NEUTRONXSDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4NEUTRONXS1.4
export G4PIIDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4PII1.3
export G4REALSURFACEDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/RealSurface2.1.1
export G4SAIDXSDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4SAIDDATA1.1
export G4ABLADATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4ABLA3.1
export G4ENSDFSTATEDATA=/home/$username/Documents/Geant4Install/GeantBuild/data/G4ENSDFSTATE2.2
