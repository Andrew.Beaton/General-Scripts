#!/bin/bash



echo "starting ..."

mkdir x
mkdir y
mkdir z
mkdir RealSpace


find . -name '*\_x.png' -exec mv {} x \;
find . -name '*\_y.png' -exec mv {} y \;
find . -name '*\_z.png' -exec mv {} z \;
find . -name '*\_plot2D.png' -exec mv {} RealSpace \;

echo "Finished !"

