#!/bin/bash -l
#SBATCH -N 2
#SBATCH -p debug
#SBATCH -C knl,quad,cache
#SBATCH -t 30:00
#SBATCH -L SCRATCH



# Add the following "sbcast" line here for jobs larger than 1500 MPI tasks:
# sbcast ./mycode.exe /tmp/mycode.exe

BIN=/project/projectdirs/vorpal/vp8users/cary/cori/internal-gcc-4.9/vorpal-r29554/bin
RUNDIRNAME=KNLVsim1

srun ${BIN}/txpp.py ${RUNDIRNAME}.pre
srun ${BIN}/vorpal -i ${RUNDIRNAME}.in -r 199
