#!/bin/bash
#SBATCH --nodes=64
#SBATCH --ntasks-per-node=64
#SBATCH --time=00:30:00
#SBATCH -J cori02
###SBATCH --partition=debug
#SBATCH --constraint=haswell 

###SBATCH --qos=premium

BIN=/project/projectdirs/vorpal/vp8users/cary/cori/internal-gcc-4.9/vorpal-r29554/bin
RUNDIRNAME=cori02

srun ${BIN}/txpp.py ${RUNDIRNAME}.pre
srun ${BIN}/vorpal -i ${RUNDIRNAME}.in
###srun ${BIN}/vorpal -i ${RUNDIRNAME}.in -r 32